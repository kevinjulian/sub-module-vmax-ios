//
//  SdkMediation.h
//  SeventynineAds
//
//  Created by sudha on 11/17/15.
//  Copyright © 2015 Seventynine. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SNADMeta;
@protocol MediationAdDelegate;

@interface SdkMediation : NSObject

@property (nonatomic, strong) id <MediationAdDelegate> mediationDelegate;
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, strong) NSString *adId;
@property (nonatomic, weak) UIView *fixedView;
@property (nonatomic, strong) NSString *zoneId;
@property (nonatomic) NSInteger height;

@property (nonatomic, strong) NSMutableArray *sdkMediationDataArray;
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) SNADMeta *meta;

@property (nonatomic, weak) id callBackDelegate;

@property (nonatomic, strong) NSArray *extraParameterArray;

@property (nonatomic, copy) void (^failureBlock) (void);
@property (nonatomic, copy) void (^seventynineSdkAdShow) (SNADMeta *meta);
@property (nonatomic, copy) void (^opportunityFireAndUserCounterUpdateBlock) (void);

@property(nonatomic, getter = isNativeViewDisposedBySDK) BOOL nativeViewDisposedBySDK;

//+ (SdkMediation*)sharedInstance;

- (void)showSdkMediationInViewControllerWithDictionary:(NSDictionary *)sdkMediationDict;
- (void)getSdkMediationArrayFromDBWithZoneId:(NSString *)zoneId;

- (void)fireTrackingForAdapters;

//Delegate Methods.
- (void)adRecieved:(NSString *)adapterType;
- (void)adWillPresent;
- (void)adDidPresent:(NSString *)adapterType;
- (void)adViewDidFailWithError:(NSString *)adapterType;
- (void)adViewWillRemove;
- (void)adWillClose;
- (void)adDIdClose;
- (void)adWillLeaveApplication;
- (void)adDidClick;
- (void)adWillPresentWithHeight:(NSInteger)height;

@end

@protocol MediationAdDelegate <NSObject>

@optional

//Adapter Delegates.
// Google Ad.
- (void)adViewDidShowAdMobInterstitialAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId;
- (void)adViewDidShowAdMobBannerAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId;

// Facebook Ad.
-(void)adViewDidShowFacebookInterstitialAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId;
-(void)adViewDidShowFacebookBannerAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId;
-(void)adViewDidShowFacebookNativeAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId;

// Chart Boost Ad.
- (void)adViewDidShowChartBoostInterstitialAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId cbSignature:(NSString *)cbSignature;
- (void)adViewDidShowChartBoostVideoAdWithSdkMediation:(SdkMediation *)sdkMediation placementId:(NSString *)placementId cbSignature:(NSString *)cbSignature;

//iAd.
- (void)adViewDidShowiAdBannerWithSdkMediation:(SdkMediation *)sdkMediation;
- (void)adViewDidShowiAdInterstitialWithSdkMediation:(SdkMediation *)sdkMediation;

@end



