//
//  SNADViewMAnager.h
//  libSeventynineAds
//
//  Created by Abhishek Sharma on 30/07/13.
//  Copyright (c) 2013 Seventynine. All rights reserved.
//

#import <Foundation/Foundation.h>

//extern static NSString *k
#define __SNADSK

/***************************************************************************************************************/
/***************************************************************************************************************/

/*********************************         Show Documentation On Page         *********************************/
/**********      http://appjacket-developer-wiki.seventynine.mobi/how-to/ios-manual-integration      **********/

/***************************************************************************************************************/
/***************************************************************************************************************/



//Runtime Parameters For Developer
extern NSString * const SNpublisherId;

extern NSString * const SNAD_TyrooHashKey;
extern NSString * const SNAD_TyrooWallAdEnable;

extern NSString * const SN_AdAutoRefreshTime; // Time in milliseconds (int)
extern NSString * const SN_InterstitialAdCloseAfterTime; // Time in milliseconds (int)
extern NSString * const SN_AdCloseButtonAppearTime; // Time in milliseconds (int)
extern NSString * const SN_AdClickDisabled; // Disable Click on Ad. [True/False]
extern NSString * const SN_AdContentType; // Select Ad Type: Video/Interstitial/VideoelseInterstitial
extern NSString * const SN_AdStartZone; // Make Video Ad visible/invisble by using 1 and 0. [On:1/Off:0]
extern NSString * const SN_AdHeaderZone; // Make Header Ad visible/invisble by using 1 and 0. [On:1/Off;0]
extern NSString * const SN_AdFooterZone; // Make Footer Ad visible/invisble by using 1 and 0. [On:1/Off:0]
extern NSString * const SN_AdAudioButton; // Enable/Disable sound of Ad. [Mute:0/Unmute:1]
extern NSString * const SN_AdLogoPosition; // Position of Logo when Ad is displayed.
extern NSString * const SN_AdSessionExcluded; //The Ad will not display on this session count (comma separated numerical values only).
extern NSString * const SN_AdSessionPattern; // Session Pattern For Ad Display.


/*  *****************************************************************************************  */
/*  ********* Below key string use for following methods of ADBaseViewDelegate to
 - (void)delegateFired:(NSString *)delegateString adid:(NSString *)adId adType:(SeventynineAdType)adType
 
 */
extern NSString * const kSNADNoShow;
extern NSString * const kSNADAdReady;
extern NSString * const kSNADAdStarted;
extern NSString * const kSNADVideoStart;
extern NSString * const kSNADVideoFirstQuartile;
extern NSString * const kSNADVideoSecondQuartile;
extern NSString * const kSNADVideoThirdQuartile;
extern NSString * const kSNADVideoCompleted;
extern NSString * const kSNADSkipEnabled;

extern NSString * const kSNADSmallBannerContentAvilable;
extern NSString * const kSNADSmallBannerContentUnavilable;

/*  *****************************************************************************************  */
/*  ************************** These key use to give data in method + (NSArray *)companionContent 
  return array contain dictionary objects **************************  */
extern NSString * const kSNADCompanionKey_width;
extern NSString * const kSNADCompanionKey_hieght;
extern NSString * const kSNADCompanionKey_contentType;
extern NSString * const kSNADCompanionKey_imagePath;
extern NSString * const kSNADcampaignKey_name;

typedef enum{
    NativeViewTypeGrid,
    NativeViewTypeDefault
} NativeViewType;

typedef enum{
	SmallViewTypeHeader,
	SmallViewTypeFooter
} SmallViewType;

typedef enum {
	kHeaderFooterContentTypeText,
	kHeaderFooterContentTypeWeb,
	kHeaderFooterContentTypeNone
	
}HeaderFooterContentType;

typedef enum {
	kSplashFullScreenContentTypeBoth,
	kSplashFullScreenContentTypeVideo,
	kSplashFullScreenContentTypeInterstitial,
	kSplashFullScreenContentTypeNone
	
}SplashFullScreenContentType;

typedef enum tagADViewState {
	kADViewStateDuplicate, //
	kADViewStateOldInstance = 0,
	kADViewStateNotConnectedToInternet,
	kADViewStateAppInBackground,
	kADViewStateNoAdToShow,
	kADViewStateUserActionClick,
	kADViewStateUserClosed,
	kADViewStateAdFinished,
	kADViewStateDeveloperClosed,
	kADViewStateNativeViewRemoved,
	kADViewStateUnKnown
} ADViewState;

typedef NS_ENUM(NSInteger, SeventynineAdTheme) {
	SeventynineAdThemeDefault,
	SeventynineAdThemeCustom
};

typedef NS_ENUM(NSInteger, SeventynineAdType) {
	SeventynineAdTypeSmallBanner,
	SeventynineAdTypeMainStream,
    SeventynineAdTypeTyroo
};

typedef NS_ENUM(NSInteger, SeventynineMainStreamAdLocation) {
	SeventynineMainStreamAdLocationMid,
	SeventynineMainStreamAdLocationPre
};

typedef NS_ENUM(NSInteger, SeventynineUserGender) {
    SeventynineUserGenderMale,
    SeventynineUserGenderFemale,
    SeventynineUserGenderUnknown
};

/*  *****************************************************************************************  */
/*  ************************** Ad UI and behavior decider variable **************************  */

extern NSString * const kSNADVariable_DelegateKey;// delgate object
extern NSString * const kSNADVariable_HandleAdClickKey;//Possible value @"YES", @"NO", default @"NO"
extern NSString * const kSNADVariable_ZoneIdKey;// zone id in string

// If you give background color black it prefer also pass this value @(SeventynineAdThemeCustom)
extern NSString * const kSNADVariable_BackgroundColorKey;// UIColor class object

extern NSString * const kSNADVariable_ThemeKey;//Possible value @(SeventynineAdThemeCustom), @(SeventynineAdThemeDefault), default @(SeventynineAdThemeDefault)

// These value used in video ad border design
extern NSString * const kSNADVariable_DrawBorderKey;//Possible value @"YES", @"NO", default @"YES"
extern NSString * const kSNADVariable_BorderAlphaKey;//NSNumber class object value between 0.0 to 1.0

// if yow want in fixed area ads will show instead of full screen pass this value @"NO" and you are responsible to handle frame of view and UI on orientation change of device
//extern NSString * const kSNADVariable_SdkHandleViewFrameKey;//Possible value @"YES", @"NO", default @"YES"
extern NSString * const kSNADVariable_AdIdKey;//any value in string, help to sort out for which Ad ADBaseViewDelegate method called
// It prefered give @(SeventynineMainStreamAdLocationPre) for ad come on first screen when launch app
extern NSString * const kSNADVariable_AdLocationKey;// Possible value @(SeventynineMainStreamAdLocationMid), @(SeventynineMainStreamAdLocationPre), default @(SeventynineMainStreamAdLocationMid)

extern NSString * const kSNADVariable_AdMobKey;
extern NSString * const kSNADVariable_FixedAdViewKey; // You have to give UIView in this key to call Ad in Fixed View.
extern NSString * const kSNADVariable_HeightKey; // Set Your view according ad height.

extern NSString * const kSNADVariable_CrossButtonKey; // Set Yes to display cross button.
extern NSString * const kSNADVariable_FixedViewDisposedBySDKKey;//Possible value @"YES", @"NO", default @"NO"
extern NSString * const kSNADVariable_VideoAdInitialAudioStateMuteKey;//Possible value @"YES", @"NO"

extern NSString * const kSNADVariable_FullScreenAdKey;// For future use

// Tyroo Ad variables.
extern NSString * const kSNADWallAdVariable_SizeKey; // Set how many ads you want see in tyroo list/grid.
extern NSString * const kSNADWallAdVariable_tyrooAdViewIdKey; // Set tyroo adWall id.

/*  *****************************************************************************************  */
/*  *****************************************************************************************  */



typedef void(^SNADAdReadyCompletion)(BOOL isReady);
typedef void (^SNADAdReadyBlock)(void);
//typedef void (^SNADAdTyrooAdBlock)(void);


@class SNADViewManager;
@protocol ADBaseViewDelegate <NSObject>

@optional

// Delegate Methods
- (void)adViewDidFailWithError:(NSError *)error;
- (void)adViewWillRemoveWithAdid:(NSString *)adId adState:(ADViewState)adstate adType:(SeventynineAdType)adType;
- (void)handleAdClicked:(NSString *)urlString adid:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)delegateFired:(NSString *)delegateString adid:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)adWillPresent:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adDidPresent:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)adWillClose:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adDidClose:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adWillLeaveApplication:(NSString *)adId adType:(SeventynineAdType)adType;

- (void)adIdUpdated:(NSString *)newAdId oldAdId:(NSString *)oldAdId;

// Delegate Method for SDK Mediation.
- (void)adDidClick:(NSString *)adId adType:(SeventynineAdType)adType;
- (void)adWillPresentWithHeight:(NSInteger)height adId:(NSString *)adId adType:(SeventynineAdType)adType;

@end

@interface SNADViewManager : NSObject
/**
 * Call this method to initialize seventynine sdk
 *
 * @param dict contain key-value pair for initialize sdk with option, for info about keys show SNADViewManager.h file
 */
+ (void)setRunTimeConfigration:(NSDictionary *)dict;

/**
 * Call this method to show small banner ad
 *
 * @param zoneId zone id for which want to check availability of ad
 * @param audioAd specify YES for you want specifically audio ad
 * @param developerHandleSize give YES if sdk does not handle ad view frame
 * @param completion completion is called on main thread as result calculated
 */
+ (void)isAdReadyWithZoneId:(NSString *)zoneId audioAd:(BOOL)audioAd developerHandleSize:(BOOL)developerHandleSize completion:(SNADAdReadyCompletion)completion;

/**
 * This method take a empty block which is excuted as ad ready to show.
 *
 * @param developerBlock emptyblock.
 */
+ (void)submitBlock:(SNADAdReadyBlock)developerBlock;

/**
 * Call this method to show small banner ad
 *
 * @param view View in which you want to show ad content
 * @param adType Type of small banner ad want to show
 * @param adId This adId use in ADBaseViewDelegate method to specify for which ad object corresponding delegate called
 * @param fromCustomEvent Provide vale NO for this parameter
 * @param delegate provide call back as banner ad finish
 **/
+ (void)createSmallBannerinView:(UIView *)view
                           type:(SmallViewType)adType
                           adId:(NSString *)adId
                fromCustomEvent:(BOOL)fromCustomEvent
                       delegate:(id)delegate;

/**
 * Call this method to show ad
 *
 * @param viewController ViewController in which you want to show ad content
 * @param options Options contain key-value pair for customise ad, for info about keys show SNADViewManager.h file
 */
+ (void)showAdInViewController:(UIViewController*)viewController options:(NSDictionary *)options;

/**
 * Call this method to play audio ad
 *
 * @param zoneId If want to play specific zone ad then provide zone id otherwise pass nil
 * @param adId This adId use in ADBaseViewDelegate method to specify for which ad object corresponding delegate called
 * @param delegate provide call back as audio ad finish
 */
+ (void)playAudioWithZoneId:(NSString *)zoneId adId:(NSString *)adId delegate:(id)delegate;

/**
 * Call this method to show tyroo ad
 * @param view View in which you want to show ad
 * @param size set the number of ads you want to show
 * @param adwallId set the adWallId
 * @param type set which type of ad you want to show (List or Grid)
 * @param adId This adId use in ADBaseViewDelegate method to specify for which ad object corresponding delegate called
 * @param delegate provide call back as banner ad finish
 **/
+ (void)showNativeAdInView:(UIView *)view size:(NSString *)size adWallId:(NSString *)adWallId type:(NativeViewType)type adId:(NSString *)adId delegate:(id)delegate;

/**
 * Call this method to dismiss native view (If you need to dealloc native view).
 * Implemented for memory management.
 **/
+ (void)viewControllerWillDismissed:(UIViewController *)viewController;

/**
 *
 * Start a new session for ads.
 *
 */
+ (void)changeSession;

/**
 *
 * Close all ad except small banner.
 *
 */
+ (void)closeAd;

/*
 *
 * Get User's Info
 *
 */
+ (void)setAge:(NSInteger)age;
+ (void)setDateOfBirth:(NSDate *)dob;
+ (void)setGender:(SeventynineUserGender)gender;
+ (void)setLanguage:(NSString *)language;
+ (void)setEmail:(NSString *)email;
+ (void)setCompilationId:(NSString *)compId;
+ (void)setContentLanguage:(NSString *)contentLang;

/*
 *
 * Custom Value
 *
 */
+ (void)customKey:(NSString *)customKey value:(NSString *)value;

/**
 * This method return companion data for ad.
 *
 * @return Array of companion data associated with mainstream ad.
 */
+ (NSArray *)companionContent;

// For app jacket
+ (void)appJacketModeWithViewController:(UIViewController *)controller;

// For Facebook info
+ (void)facebookAccessToken:(NSString *)token;

@end




@interface SNADNativeAdView : UIView

@end

