//
//  VMaxUnityAdRewardVideoAdapter.m
//  VMaxTableViewSample
//
//  Created by Rahul CK on 27/11/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import "VMaxUnityAdsRewardedVideoAdapter.h"
#import <UnityAds/UnityAds.h>

NSString* const kUnityAdGameId = @"gameid";

@interface VMaxUnityAdsRewardedVideoAdapter ()<UnityAdsDelegate>
@property (nonatomic,assign) NSInteger reward;
@property (nonatomic,assign) BOOL shouldShow;
@property (nonatomic,assign) BOOL skipped;
@end

@implementation VMaxUnityAdsRewardedVideoAdapter
-(id)init
{
    if (self = [super init]) {
        
        _reward = 0;
        _shouldShow = NO;
        
    }
    
    return self;
}

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    self.delegate = inDelegate;
    NSString *gameId = nil;
    if ([inParams objectForKey:kUnityAdGameId]) {
        
        gameId = [inParams objectForKey:kUnityAdGameId];
        [[UnityAds sharedInstance] startWithGameId:gameId];
        [[UnityAds sharedInstance] setDelegate:self];
        
        
        if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
            [[UnityAds sharedInstance] setTestMode:YES];
            NSLog(@"Unity Test Mode Activated");
        }
        
        
        if ([[UnityAds sharedInstance] canShow])
        {
            __weak   VMaxUnityAdsRewardedVideoAdapter *adapter = self;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if (adapter.delegate && [adapter.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
                    [adapter.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                                           withObject:adapter];
                }
                NSLog(@"Success Callback:DidReceiveAd");

            });
        }
    }
    else{
        
        __weak   VMaxUnityAdsRewardedVideoAdapter *adapter = self;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:-1 userInfo:nil];
            if (adapter.delegate && [adapter.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
                [adapter.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                                       withObject: adapter
                                       withObject: err];
                
            }
            NSLog(@"Failure Callback:DidReceiveAd");

        });
        
        
    }
    
    
}

-(void)showAd
{
    NSLog(@"Unity will try to show Ad");
    
    [[UnityAds sharedInstance]setViewController:self.parentViewController];
    
    if ([[UnityAds sharedInstance] canShow])
    {
        NSLog(@"Unity can show Ad");
        // If both are ready, show the ad.
        [[UnityAds sharedInstance] show];
    }
    else
    {
        NSLog(@"Unity failed to show Ad");
    }
    _shouldShow = YES;
}

-(void)invalidateAd
{
    //    [[UnityAds sharedInstance] hide];
    //    [[UnityAds sharedInstance] stopAll];
    //
    //
    //    [[UnityAds sharedInstance]setDelegate:nil];
    
    
}

-(void)dealloc
{
    
    [[UnityAds sharedInstance]setDelegate:nil];
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}

#pragma mark-UnityAdView Delegate

- (void)unityAdsVideoCompleted:(NSString *)rewardItemKey skipped:(BOOL)skipped
{
    self.skipped = skipped;
    if (skipped) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didInterruptRewardVideoWithUserResponseBlock:)]) {
            [self.delegate VMaxCustomAd:self didInterruptRewardVideoWithUserResponseBlock:^(BOOL cancelled) {
                if (cancelled) {
                    [[UnityAds sharedInstance]hide];
                    [[UnityAds sharedInstance] stopAll];
                    
                }
                else
                {
                    
                }
            }];
        }
        
    }
    else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]) {
            [self.delegate VMaxCustomAd:self didCompleteRewardVideo:self.reward];
        }
        
    }
}
- (void)unityAdsWillShow
{
    NSLog(@"unityAdsWillShow");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillPresentOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillPresentOverlay:) withObject:self];
    }
    
    
    
}
- (void)unityAdsDidShow
{
    NSLog(@"unityAdsDidShow");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                            withObject:self];
    }
    
    
    
}
- (void)unityAdsWillHide
{
    NSLog(@"unityAdsWillHide");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:) withObject:self];
    }
    
}
- (void)unityAdsDidHide
{
    NSLog(@"unityAdsDidHide");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
    
    
}
- (void)unityAdsWillLeaveApplication
{
    NSLog(@"unityAdsWillLeaveApplication");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
    
    
}
- (void)unityAdsVideoStarted
{
    
    NSLog(@"unityAdsVideoStarted");
    
}
- (void)unityAdsFetchCompleted;
{
    [[UnityAds sharedInstance]setViewController:self.parentViewController];
    NSLog(@"unityAdsFetchCompleted");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                            withObject:self];
        NSLog(@"Success Callback:DidReceiveAd");
        

    }
    
    
}
- (void)unityAdsFetchFailed
{
    NSLog(@"unityAdsFetchFailed");
    
    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:-1 userInfo:nil];
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: err];
        NSLog(@"Failure Callback:%@", err);

    }
    

    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailedToPlaybackRewardVideo:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailedToPlaybackRewardVideo:)
                            withObject: self
                            withObject: err];
    }
    
}
@end
