//
//  VMaxUnityAdRewardVideoAdapter.h
//  VMaxTableViewSample
//
//  Created by Rahul CK on 27/11/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxUnityAdsRewardedVideoAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

@end