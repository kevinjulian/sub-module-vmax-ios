
//
//  VMaxSuperSonicRewardedVideoAdapter.m
//  VMaxTableViewSample
//
//  Created by Rahul CK on 19/12/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import "VMaxSuperSonicRewardedVideoAdapter.h"
#import "Supersonic/Supersonic.h"

NSString* const kSuperSonicparameter_Apikey             = @"applicationkey";
NSString* const kSuperSonicparameter_PlaceName          = @"placement_name";

static BOOL isSDKInitilized = NO;
static BOOL isLoadAdCalled  = NO;

__weak static id<VMaxCustomAdListener> delegate;


@interface VMaxSuperSonicRewardedVideoAdapter ()<SupersonicLogDelegate,SupersonicRVDelegate>{
    NSString *placeMentName;

}

@end
@implementation VMaxSuperSonicRewardedVideoAdapter
-(id)init
{
    if (self = [super init]) {

    }
    
    return self;
}

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    
    delegate = inDelegate;
    isLoadAdCalled  = NO;
    
    NSString* apiKey = nil;
    placeMentName = nil;
    NSString *userIdfv = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    if ([inParams objectForKey: kSuperSonicparameter_Apikey]) {
        apiKey = [inParams objectForKey:kSuperSonicparameter_Apikey];
    }
    if ([inParams objectForKey: kSuperSonicparameter_PlaceName]) {
        apiKey = [inParams objectForKey:kSuperSonicparameter_PlaceName];
    }

 
    if (apiKey && userIdfv) {
        
        [[Supersonic sharedInstance] setLogDelegate:self];
        [[Supersonic sharedInstance] setRVDelegate:self];
        if (!isSDKInitilized) {
            [[Supersonic sharedInstance] initRVWithAppKey:apiKey withUserId:userIdfv];
            isSDKInitilized = YES;

        }
        if ([[Supersonic sharedInstance]isAdAvailable]) {
          __weak   VMaxSuperSonicRewardedVideoAdapter *adapter = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if ([delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)] && isLoadAdCalled  == NO) {
                    [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:adapter];
                    NSLog(@"Success Callback:DidReceiveAd");


                    isLoadAdCalled  = YES;
                }
                
            });
            
            
        }
        
    }

    
    
}

-(void)showAd
{
    
    if (placeMentName) {
        [[Supersonic sharedInstance] showRVWithPlacementName:placeMentName];

    }
    else{
        [[Supersonic sharedInstance] showRV];

    }

}

-(void)invalidateAd
{
//     delegate = nil;
//    [[Supersonic sharedInstance] setLogDelegate:nil];
//    [[Supersonic sharedInstance] setRVDelegate:nil];
    
}

-(void)dealloc
{

    NSLog(@"Deallocated %@",[self class]);

}
-(void) throughErrorCallback
{
    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:-1 userInfo:nil];
    
    if (delegate && [delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                       withObject: self
                       withObject: err];
    }
    if (delegate && [delegate respondsToSelector: @selector(VMaxCustomAd:didFailedToPlaybackRewardVideo:)]) {
        [delegate performSelector: @selector(VMaxCustomAd:didFailedToPlaybackRewardVideo:)
                       withObject: self
                       withObject: err];
    }

}
#pragma mark-SuperSonic Delegate

- (void)sendLog:(NSString *)log level:(LogLevel)level tag:(LogTag)tag
{
    
}
/*!
 * @discussion Invoked when initialization of RewardedVideo ad unit has finished successfully.
 */
- (void)supersonicRVInitSuccess
{

    NSLog(@"supersonicRVInitSuccess");
}

/*!
 * @discussion Invoked when RewardedVideo initialization process has failed.
 *
 *              NSError contains the reason for the failure.
 */
- (void)supersonicRVInitFailedWithError:(NSError *)error
{

    NSLog(@"supersonicRVInitFailedWithError");
    
    [self performSelectorOnMainThread:@selector(throughErrorCallback) withObject:nil waitUntilDone:YES];

    
    
    NSLog(@"Failure Callback:%@", error);

}

/*!
 * @discussion Invoked when there is a change in the ad availability status.
 *
 *              hasAvailableAds - value will change to YES when rewarded videos are available.
 *              You can then show the video by calling showRV(). Value will change to NO when no videos are available.
 */
- (void)supersonicRVAdAvailabilityChanged:(BOOL)hasAvailableAds
{

    NSLog(@"supersonicRVAdAvailabilityChanged");

    if (hasAvailableAds && isLoadAdCalled == NO) {
        if ([delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
            [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
            isLoadAdCalled =YES;
            
            NSLog(@"Success Callback:DidReceiveAd");
            

        }
    }
}

/*!
 * @discussion Invoked when the user completed the video and should be rewarded.
 *
 *              If using server-to-server callbacks you may ignore these events and wait for the callback from the Supersonic server.
 *              placementInfo - SupersonicPlacementInfo - an object contains the placement's reward name and amount
 */
- (void)supersonicRVAdRewarded:(SupersonicPlacementInfo*)placementInfo
{

    NSLog(@"supersonicRVAdRewarded");
   
    
    if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]) {
        [delegate VMaxCustomAd:self didCompleteRewardVideo:0];
    }
}

/*!
 * @discussion Invoked when an Ad failed to display.
 *
 *          error - NSError which contains the reason for the failure.
 *          The error contains error.code and error.message.
 */
- (void)supersonicRVAdFailedWithError:(NSError *)error
{

    [self performSelectorOnMainThread:@selector(throughErrorCallback) withObject:nil waitUntilDone:YES];

        NSLog(@"Failure Callback:%@", error);

}

/*!
 * @discussion Invoked when the RewardedVideo ad view has opened.
 *
 */
- (void)supersonicRVAdOpened
{
    if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                       withObject:self];
    }

    NSLog(@"supersonicRVAdOpened");

}

/*!
 * @discussion Invoked when the user is about to return to the application after closing the RewardedVideo ad.
 *
 */
- (void)supersonicRVAdClosed
{
    NSLog(@"supersonicRVAdClosed");
    if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                       withObject:self];
    }
    

}

/**
 * Note: the events below are not available for all supported Rewarded Video
 * Ad Networks.
 * Check which events are available per Ad Network you choose to include in
 * your build.
 * We recommend only using events which register to ALL Ad Networks you
 * include in your build.
 */


/*!
 * @discussion Invoked when the video ad starts playing.
 *
 *             Available for: AdColony, Vungle, AppLovin, UnityAds
 */
- (void)supersonicRVAdStarted
{

    NSLog(@"supersonicRVAdStarted");


}

/*!
 * @discussion Invoked when the video ad finishes playing.
 *
 *             Available for: AdColony, Flurry, Vungle, AppLovin, UnityAds.
 */
- (void)supersonicRVAdEnded
{

    NSLog(@"supersonicRVAdEnded");

}
@end
