//
//  VMaxSuperSonicRewardedVideoAdapter.h
//  VMaxTableViewSample
//
//  Created by Rahul CK on 19/12/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxSuperSonicRewardedVideoAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) UIViewController* parentViewController;

@end
