//
//  FlurryBannerAdapter.m
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import "VMaxFlurryBannerAdapter.h"
#import "FlurryAdBanner.h"
#import "Flurry.h"

#import <CoreLocation/CoreLocation.h>

NSString* const kVMaxCustomAdAdapter_FlurryBannerAppId = @"appid";
NSString* const kVMaxCustomAdAdapter_FlurryPlacement = @"adspace";


@interface VMaxFlurryBannerAdapter () <FlurryAdBannerDelegate>

@property (strong, nonatomic) FlurryAdBanner* flurryBannerAd;
@property (strong, nonatomic) UIView* adView;

@end

@implementation VMaxFlurryBannerAdapter

static BOOL isSessionInitilized = NO;

#pragma mark-VMaxCustomAd Delegate

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    
    _parentViewController=parentViewController;
    _delegate=inDelegate;
    NSString* appID = @"flurryTest";
    
    if ([inParams objectForKey:kVMaxCustomAdAdapter_FlurryBannerAppId]) {
        appID = [inParams objectForKey:kVMaxCustomAdAdapter_FlurryBannerAppId];
    }
    
    CGRect adViewSRect = CGRectZero;
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    BOOL isIpad = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    float bannerHeight = (isIpad)? 90 : 50;
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        adViewSRect = CGRectMake(0, 0, bannerHeight, screenSize.width);
    } else {
        adViewSRect = CGRectMake(0, 0, screenSize.width, bannerHeight);
    }

    if (!isSessionInitilized) {
        [Flurry startSession:appID];
        isSessionInitilized = YES;
    }
    
    NSString *adplacement = @"BANNER_BOTTOM";
    if ([inParams objectForKey:kVMaxCustomAdAdapter_FlurryPlacement]) {
        adplacement = [inParams objectForKey:kVMaxCustomAdAdapter_FlurryPlacement];
    }
    self.flurryBannerAd = [[FlurryAdBanner alloc] initWithSpace:adplacement];
    
    [self.flurryBannerAd setAdDelegate:self];

    NSNumber* latitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLatitude];
    NSNumber* longitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLongitude];
    
    NSLog(@"VMax USER PARAM: Lat: %@, Long: %@", latitude, longitude);

    FlurryAdTargeting* theTargetting = [FlurryAdTargeting targeting];

    if (latitude && longitude) {

        theTargetting.location = [[CLLocation alloc] initWithLatitude:latitude.doubleValue
                                                            longitude:longitude.doubleValue];
        
    }
    
    self.flurryBannerAd.targeting = theTargetting;

    self.adView = [[UIView alloc] initWithFrame:adViewSRect];
    [self.flurryBannerAd fetchAndDisplayAdInView: self.adView
                   viewControllerForPresentation: self.parentViewController];
    
    
}

-(void)showAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}
     
-(void)invalidateAd
{
    self.delegate = nil;
    
    if (self.adView.superview) {
        [self.adView removeFromSuperview];
        self.adView = nil;
        
        [self.flurryBannerAd setAdDelegate:nil];
        self.flurryBannerAd = nil;
    }
}

//-(void)dealloc
//{
//    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
//}

#pragma mark-FlurryAd Delegate

/*!
 *  @brief Invoked when an ad is received for the specified @c bannerAd object.
 *  @since 6.0.0
 *
 *  This method informs the app that an ad has been received and is available for display.
 *
 *  @see FlurryAdBanner#fetchAdForFrame: for details on the method that will invoke this delegate.
 *
 *  @param bannerAd The ad object that has successfully fetched an ad.
 */
- (void) adBannerDidFetchAd:(FlurryAdBanner*)bannerAd
{
    NSLog(@"Success Callback:adBannerDidFetchAd");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didLoadAdInView:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didLoadAdInView:) withObject:self withObject:self.adView];
    }
}

/*!
 *  @brief Invoked when the banner ad is rendered.
 *  @since 6.0.0
 *
 *  This method informs the user an ad was retrieved, and successful in displaying to the user.
 *
 *  @see \n
 *  FlurryAdBanner#displayAdInView:viewControllerForPresentation: for details on the method that will invoke this delegate. \n
 *  FlurryAdBanner#fetchAndDisplayAdInView:viewControllerForPresentation: for details on the method that will invoke this delegate.
 *
 *  @param bannerAd The ad object that rendered successfully.
 *
 */
- (void) adBannerDidRender:(FlurryAdBanner*)bannerAd
{
    NSLog(@"Success Callback:adBannerDidRender");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self withObject:self.adView];
    }
}

/*!
 *  @brief Invoked when the specified banner ad object is about to present a full screen.
 *  @since 6.0.0
 *
 *  @param bannerAd The banner ad object that is associated with the full screen that is about to present a fullscreen.
 *
 */
- (void) adBannerWillPresentFullscreen:(FlurryAdBanner*)bannerAd
{
}

/*!
 *  @brief Invoked when the ad has been selected that will take the user out of the app.
 *  @since 6.0.0
 *
 *  This method informs the app that an ad has been clicked and the user is about to be taken outside the app.
 *
 *  @param bannerAd The ad object that received the click.
 *
 */
- (void) adBannerWillLeaveApplication:(FlurryAdBanner*)bannerAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}

/*!
 *  @brief Invoked when a fullscreen associated with the specified ad will be removed.
 *  @since 6.0.0
 *
 *  @param bannerAd The banner ad object that is associated with the full screen that is about to be dismissed.
 *
 */
- (void) adBannerWillDismissFullscreen:(FlurryAdBanner*)bannerAd
{
    
}

/*!
 *  @brief Invoked when a fullscreen associated with the specified ad has been removed.
 *  @since 6.0.0
 *
 *  @param bannerAd The banner ad object that is associated with the full screen that that has been dismissed.
 *
 */
- (void) adBannerDidDismissFullscreen:(FlurryAdBanner*)bannerAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }
}

/*!
 *  @brief Informational callback invoked when an ad is clicked for the specified @c bannerAd object.
 *  @since 6.0.0
 *
 *  This method informs the app that an ad has been clicked. This should not be used to adjust state of an app. It is only intended for informational purposes.
 *
 *  @param bannerAd The ad object that received the click.
 *
 */
- (void) adBannerDidReceiveClick:(FlurryAdBanner*)bannerAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
}

/*!
 *  @brief Invoked when a video finishes playing
 *  @since 6.0.0
 *
 *  This method informs the app that a video associated with this ad has finished playing. Note the SDK
 *  will launch a full screen video player for banners that present a video when they receive a click.
 *
 *  @param bannerAd The banner ad object that played the video and finished playing the video.
 *
 */
- (void) adBannerVideoDidFinish:(FlurryAdBanner*)bannerAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }
}

/*!
 *  @brief Informational callback invoked when there is an ad error
 *  @since 6.0
 *
 *  @see FlurryAdError for the possible error reasons.
 *
 *  @param bannerAd The banner ad object associated with the error
 *  @param adError an enum that specifies the reason for the error.
 *  @param errorDescription An error object that gives additional information on the cause of the ad error.
 *
 */
- (void) adBanner:(FlurryAdBanner*) bannerAd adError:(FlurryAdError) adError errorDescription:(NSError*) errorDescription
{
    NSLog(@"Failure Callback:%@", errorDescription);


    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:errorDescription];
    }
}

@end
