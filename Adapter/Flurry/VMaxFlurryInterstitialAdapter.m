//
//  FlurryInterstitialAdapter.m
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import "VMaxFlurryInterstitialAdapter.h"

#import "FlurryAdInterstitial.h"
#import "Flurry.h"
#import <CoreLocation/CoreLocation.h>

NSString* const kVMaxCustomAdAdapter_FlurryInterstitialAppId = @"appid";

@interface VMaxFlurryInterstitialAdapter () <FlurryAdInterstitialDelegate>

@property (nonatomic, strong) FlurryAdInterstitial *adInterstitial;

@end


@implementation VMaxFlurryInterstitialAdapter

static BOOL isSessionInitilized = NO;

#pragma mark-VMaxCustomAd Delegate

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    
    _parentViewController = parentViewController;
    
    _delegate = inDelegate;
    
    NSString* appID = @"flurryTest";
    
    if ([inParams objectForKey:kVMaxCustomAdAdapter_FlurryInterstitialAppId]) {
        appID = [inParams objectForKey:kVMaxCustomAdAdapter_FlurryInterstitialAppId];
    }

    if (!isSessionInitilized) {
        [Flurry startSession:appID];
        isSessionInitilized = YES;
    }
    
    self.adInterstitial = [[FlurryAdInterstitial alloc] initWithSpace:@"INTERSTITIAL_MAIN_VC"];
    
    [self.adInterstitial setAdDelegate:self];
    
    self.adInterstitial.targeting = [FlurryAdTargeting targeting];

    [self.adInterstitial fetchAd];
}

-(void)showAd
{
    if (self.adInterstitial.ready) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
            [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
        }
    }else{
         [self.adInterstitial fetchAd];
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
}

//-(void)dealloc
//{
//    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
//}

#pragma mark-FlurryAd Delegate

- (void) spaceDidReceiveAd:(NSString*)adSpace
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate VMaxCustomAdDidLoadAd:self];
    }
}

- (void) spaceDidFailToReceiveAd:(NSString*)adSpace error:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [_delegate VMaxCustomAd:self didFailWithError:error];
    }
    
}

/** It is recommended to pause app activities when an interstitial is shown.* Listen to should display delegate.*/
-(BOOL) spaceShouldDisplay:(NSString*)adSpace interstitial:(BOOL)interstitial {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate VMaxCustomAdDidShowAd:self];
    }
    
    return YES;
}

/** Resume app state when the interstitial is dismissed.*/
-(void)spaceDidDismiss:(NSString *)adSpace interstitial:(BOOL)interstitial
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate VMaxCustomAdDidDismissAd:self];
    }
}

/*!
 *  @brief Invoked when an ad is received for the specified @c interstitialAd object.
 *  @since 6.0.0
 *
 *  This method informs the app that an ad has been received and is available for display.
 *
 *  @see FlurryAdInterstitial#fetchAd for details on the method that will invoke this delegate.
 *
 *  @param interstitialAd The ad object that has successfully fetched an ad.
 */
- (void) adInterstitialDidFetchAd:(FlurryAdInterstitial*)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
    }
}

/*!
 *  @brief Invoked when the interstitial ad is rendered.
 *  @since 6.0.0
 *
 *  This method informs the user an ad was retrieved, and successful in displaying to the user.
 *
 *  @see \n
 *  FlurryAdInterstitial#presentWithViewControler: for details on the method that will invoke this delegate. \n
 *
 *  @param interstitialAd The ad object that rendered successfully.
 *
 */
- (void) adInterstitialDidRender:(FlurryAdInterstitial*)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

/*!
 *  @brief Invoked when a fullscreen associated with the specified ad will present on the screen.
 *  @since 6.0.0
 *
 *  @param interstitialAd The interstitial ad object that is associated with the full screen that will present.
 *
 */
- (void) adInterstitialWillPresent:(FlurryAdInterstitial*)interstitialAd
{
}


/*!
 *  @brief Invoked when the ad has been selected that will take the user out of the app.
 *  @since 6.0.0
 *
 *  This method informs the app that an ad has been clicked and the user is about to be taken outside the app.
 *
 *  @param interstitialAd The ad object that received the click.
 *
 */
- (void) adInterstitialWillLeaveApplication:(FlurryAdInterstitial*)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}

/*!
 *  @brief Invoked when a fullscreen associated with the specified ad will be removed.
 *  @since 6.0.0
 *
 *  @param interstitialAd The interstitial ad object that is associated with the full screen that will be dismissed.
 *
 */
- (void) adInterstitialWillDismiss:(FlurryAdInterstitial*)interstitialAd
{
    
}

/*!
 *  @brief Invoked when a fullscreen associated with the specified ad has been removed.
 *  @since 6.0.0
 *
 *  @param interstitialAd The interstitial ad object that is associated with the full screen that has been dismissed.
 *
 */
- (void) adInterstitialDidDismiss:(FlurryAdInterstitial*)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }
}

/*!
 *  @brief Informational callback invoked when an ad is clicked for the specified @c interstitialAd object.
 *  @since 6.0.0
 *
 *  This method informs the app that an ad has been clicked. This should not be used to adjust state of an app. It is only intended for informational purposes.
 *
 *  @param interstitialAd The ad object that received the click.
 *
 */
- (void) adInterstitialDidReceiveClick:(FlurryAdInterstitial*)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
}

/*!
 *  @brief Invoked when a video finishes playing
 *  @since 6.0.0
 *
 *  This method informs the app that a video associated with this ad has finished playing.
 *
 *  @param interstitialAd The interstitial ad object that played the video and finished playing the video.
 *
 */
- (void) adInterstitialVideoDidFinish:(FlurryAdInterstitial*)interstitialAd
{
    
}

/*!
 *  @brief Informational callback invoked when there is an ad error
 *  @since 6.0
 *
 *  @see FlurryAdError for the possible error reasons.
 *
 *  @param interstitialAd The interstitial ad object associated with the error
 *  @param adError an enum that gives the reason for the error.
 *  @param errorDescription An error object that gives additional information on the cause of the ad error.
 *
 */
- (void) adInterstitial:(FlurryAdInterstitial*) interstitialAd adError:(FlurryAdError) adError errorDescription:(NSError*) errorDescription
{
    NSLog(@"Failure Callback:%@", errorDescription);

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self];
    }
}

@end

