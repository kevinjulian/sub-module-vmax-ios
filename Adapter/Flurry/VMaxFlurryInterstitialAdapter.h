//
//  FlurryInterstitialAdapter.h
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMaxCustomAd.h"


@interface VMaxFlurryInterstitialAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo;

-(void)showAd;

-(void)invalidateAd;
@end
