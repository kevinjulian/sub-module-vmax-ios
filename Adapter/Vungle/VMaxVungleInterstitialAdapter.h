//
//  VungleInterstetialAdapter.h
//  VingleSample
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxVungleInterstitialAdapter : NSObject <VMaxCustomAd>

@property (nonatomic, strong) UIViewController* parentViewController;

@end
