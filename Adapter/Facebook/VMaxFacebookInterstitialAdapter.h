//
//  VMaxFacebookAdInterface.h
//  VMaxClientSideMediationSample
//
//  Copyright (c) 2014 VMax.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAdListener.h"
#import "VMaxCustomAd.h"


@interface VMaxFacebookInterstitialAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

@end
