//
//  FacebookBannerAdapter.h
//  VMaxClientSideMediationSample
//
//  Created by RAHUL CK on 27/10/14.
//  Copyright (c) 2014 robosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxFacebookBannerAdapter :NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

@end
