//
//  VMaxFacebookNativeAdapter.h
//  VMaxTableViewSample
//
//  Created by Thejaswini K on 07/09/15.
//  Copyright © 2015 VMax.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxNativeAd.h"


@interface VMaxFacebookNativeAdapter : VMaxNativeAd <VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

@end
