//
//  VMaxFacebookNativeAdapter.m
//  VMaxTableViewSample
//
//  Created by Thejaswini K on 07/09/15.
//  Copyright © 2015 VMax.com. All rights reserved.
//

#import "VMaxFacebookNativeAdapter.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>

static NSString* const kVMaxCustomAdAdapter_FacebookBannerPlacementId = @"placementid";
static NSString* const kVMaxCustomAdAdapter_FacebookNaiveImpression = @"impression";


@interface VMaxFacebookNativeAdapter () <FBNativeAdDelegate,FBMediaViewDelegate> {
    
    FBNativeAd* nativeAd;
    NSArray *impressionUrls;
    FBAdChoicesView  *adChoicesView;
    FBMediaView *mediaView;
    
}

@end

@implementation VMaxFacebookNativeAdapter


#pragma mark -CustomAd Delegate

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo {
    
    self.delegate = inDelegate;
    self.parentViewController = parentViewController;
    impressionUrls = [inParams objectForKey:kVMaxCustomAdAdapter_FacebookNaiveImpression];
    
    NSString* placementId = @"facebookTest";
    
    if ([inParams objectForKey: kVMaxCustomAdAdapter_FacebookBannerPlacementId]) {
        placementId = [inParams objectForKey: kVMaxCustomAdAdapter_FacebookBannerPlacementId];
    }
    nativeAd = [[FBNativeAd alloc] initWithPlacementID:placementId];
    
    nativeAd.delegate = self;
    if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
        
        NSArray *testDevices = [inExtraInfo objectForKey:kVMaxCustomAdExtras_TestDevices];
        if(testDevices && testDevices.count){
            [FBAdSettings addTestDevices:testDevices];
            NSLog(@"Added FB test Devices:%@",testDevices);
        }
        
        
    }
    
    [nativeAd loadAd];
}
#pragma mark - FBNativeAdDeleagte methods
- (void)nativeAdDidLoad:(nonnull FBNativeAd *)fbNativeAd
{
    nativeAd = fbNativeAd;
    if(adChoicesView == nil) {
        adChoicesView = [[FBAdChoicesView alloc] initWithNativeAd:nativeAd expandable:NO];
    }
   [self createDictionaryFromFbNativeAd:nativeAd];
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
        NSLog(@"Success Callback:DidReceiveAd");
        
    }
    @try {
        
        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setValue:fbNativeAd.title forKey:@"title"];
        [response setValue:fbNativeAd.subtitle forKey:@"subtitle"];
        [response setValue:fbNativeAd.socialContext forKey:@"socialContext"];
        [response setValue:fbNativeAd.callToAction forKey:@"callToAction"];
        [response setValue:fbNativeAd.icon.url forKey:@"iconUrl"];
        [response setValue:fbNativeAd.coverImage.url forKey:@"coverImageUrl"];
        [response setValue:fbNativeAd.body forKey:@"body"];
        
        NSLog(@"Facebook Native Ad Response: %@", response);
    }
    @catch (NSException *exception) {
        
    }
}

- (void)nativeAdWillLogImpression:(nonnull FBNativeAd *)nativeAd
{
}

- (void)nativeAd:(nonnull FBNativeAd *)nativeAd didFailWithError:(nonnull NSError *)error {
    NSLog(@"%@", error);
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
        NSLog(@"Failure Callback:%@", error);
        
    }
}

- (void)nativeAdDidClick:(nonnull FBNativeAd *)nativeAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
    if (self.nativeAdDelegate && [self.nativeAdDelegate respondsToSelector:@selector(nativeAdwillPresentOverlay:)]) {
        [self.nativeAdDelegate performSelector:@selector(nativeAdwillPresentOverlay:) withObject:self];
    }
    
}

- (void)nativeAdDidFinishHandlingClick:(nonnull FBNativeAd *)nativeAd
{
    NSLog(@"The user finished to interact with the ad.");
}
-(void)dealloc
{
    [nativeAd unregisterView];
    nativeAd.delegate = nil;
    nativeAd = nil;
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    @catch (NSException *exception) {
        
    }
}

-(void) createDictionaryFromFbNativeAd:(FBNativeAd*)fbNativeAd
{
    
    self.imageIcon = [[VMaxAdImage alloc]initWithURL:fbNativeAd.icon.url width:fbNativeAd.icon.width
                                              height:fbNativeAd.icon.height];
    
    self.imageMain = [[VMaxAdImage alloc]initWithURL:fbNativeAd.coverImage.url
                                               width:fbNativeAd.coverImage.width height:fbNativeAd.coverImage.height];
    [self.imageMain loadImageAsyncWithBlock:nil];
    
    self.ctaText = fbNativeAd.callToAction;
    self.title = fbNativeAd.title;
    self.desc = fbNativeAd.body;
    
    mediaView = [[FBMediaView alloc] initWithNativeAd:fbNativeAd];
    mediaView.delegate = self;
    [mediaView setNativeAd:fbNativeAd];
    [mediaView setAutoplayEnabled:YES];
    self.view = mediaView;
    self.nativeAdPartner = @"Facebook";
    // Rating.
    double rating = (fbNativeAd.starRating.value / ((double)fbNativeAd.starRating.scale + 0.000000000001)) * 5.0;
    int wholeNum = (int)rating;
    int fraction = (rating - wholeNum) * 10;
    self.rating = [NSString stringWithFormat:@"%d.%d", wholeNum, fraction];
    [self setImpTrackers:impressionUrls];
    
}

-(void)showAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

-(void)invalidateAd
{
    
}

-(void)registerViewForInteraction:(VMaxAdView*)adView view:(UIView*)view listOfViews:(NSArray*)listOfViews
{
    [super registerViewForInteraction:adView view:view listOfViews:nil];// passing nil as list of views since fb ad is handling click ,
    
    UIView *contentView = ([view viewWithTag:501])?[view viewWithTag:501]:view;
    
    
    if(contentView!=nil) {
        if(adChoicesView == nil) {
            adChoicesView = [[FBAdChoicesView alloc] initWithNativeAd:nativeAd expandable:NO];
        }
        
        if(![adChoicesView superview]) // fix: navigating back from the ad choices view triggers an update here, readding a new ad choices view
            [contentView addSubview:adChoicesView];
        
        [adChoicesView updateFrameFromSuperview];
    }
    
    [contentView bringSubviewToFront:adChoicesView];
    
    NSLog(@"%@ %@", adChoicesView.superview.description, NSStringFromCGRect(adChoicesView.frame));
    
    // register as observer for orientation changes to position adChoicesView properly
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orientationChanged)
                                                     name:UIDeviceOrientationDidChangeNotification
                                                   object:nil];
    }
    
    if (nativeAd && listOfViews!=nil && view!=nil) {
        [nativeAd registerViewForInteraction:view
                          withViewController:self.parentViewController
                          withClickableViews:listOfViews];
    }
}
- (void)orientationChanged {
    if(adChoicesView) {
        [adChoicesView setHidden:YES];
        [adChoicesView updateFrameFromSuperview];
        [adChoicesView setHidden:NO];
    }
}

#pragma mark-Util
-(NSString*) convertoJsonString:(NSDictionary*) inDic
{
    NSMutableDictionary *nativeAdDic = [NSMutableDictionary dictionaryWithDictionary:inDic];
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:nativeAdDic
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}
@end
