//
//  VMaxInmobiBannerAdapter.m
//  VMaxSample
//
//  Created by Anup Dsouza on 01/06/16.
//  Copyright © 2016 VMax. All rights reserved.
//

#import "VMaxInmobiBannerAdapter.h"
#import "IMBannerDelegate.h"
#import "IMBanner.h"
#import "IMCommonConstants.h"
#import "IMRequestStatus.h"
#import "IMSdk.h"

#define kVMaxSDKVersion                            @"A-IO-3.5.7"

static NSString* const kVMaxCustomAdAdapter_InMobiBannerPlacementId = @"placementid";
static NSString* const kVMaxCustomAdAdapter_InMobiBannerAccountId   = @"accountid";

@interface VMaxInmobiBannerAdapter () <IMBannerDelegate>
@property (nonatomic, strong) IMBanner *bannerAd;
@property (nonatomic, strong) NSArray *impressionUrls;
@end

@implementation VMaxInmobiBannerAdapter

#pragma mark VMaxCustomAd -
- (void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo {
    
    self.delegate = inDelegate;
    self.parentViewController = parentViewController;
    
    NSString *placementId = inParams[kVMaxCustomAdAdapter_InMobiBannerPlacementId];
    NSString *accountId = inParams[kVMaxCustomAdAdapter_InMobiBannerAccountId];
    
    if (placementId && accountId) {
        
        // location info
        NSNumber *lat = inExtraInfo[kVMaxCustomAdExtras_LocationLatitude];
        NSNumber *lon = inExtraInfo[kVMaxCustomAdExtras_LocationLongitude];
        
        if (lat && lon) {
            CLLocation *location = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lon.doubleValue];
            [IMSdk setLocation:location];
        }
        
        NSString *gender = inExtraInfo[kVMaxCustomAdExtras_Gender]; // gender
        if (gender) {
            IMSDKGender genderValue = ([gender isEqualToString:kVMaxCustomAdExtras_GenderMale]) ? kIMSDKGenderMale:kIMSDKGenderFemale;
            [IMSdk setGender:genderValue];
        }
        
        NSString *age = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Age]; // age
        if (age) {
            [IMSdk setAge:age.intValue];
        }
        
        [IMSdk initWithAccountID:accountId];
        [IMSdk setLogLevel:kIMSDKLogLevelDebug];
        
        // SBD determination
        NSInteger adScale = 0;
        NSString *adSizeStr = nil;
        CGSize bannerSize = CGSizeZero;
        
        if (inExtraInfo[kVMaxCustomAdExtras_AdSettings]) {
            adSizeStr = inExtraInfo[kVMaxCustomAdExtras_AdSettings][kVMaxCustomAdExtras_AdSize];
        }
        
        if (inExtraInfo[kVMaxCustomAdExtras_AdSettings]) {
            adScale = [inExtraInfo[kVMaxCustomAdExtras_AdSettings][kVMaxCustomAdExtras_Scale] integerValue];
        }
        
        if (adSizeStr) {
            
            bannerSize = [self bannerSizeFromSizeString:adSizeStr];
            
            // SBD scaling
            if (adScale > 0 && adScale < 200) {
                
                CGFloat width = (bannerSize.width * adScale)/100;
                CGFloat height = (bannerSize.height * adScale)/100;
                bannerSize = CGSizeMake(width, height);
            }
        } else {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                bannerSize = CGSizeMake(320, 50);
            } else {
                bannerSize = CGSizeMake(728, 90);
            }
        }
        
        // banner init
        self.bannerAd = [[IMBanner alloc] initWithFrame:CGRectMake(0, 0, bannerSize.width, bannerSize.height)
                                            placementId:placementId.longLongValue
                                               delegate:self];
        
        NSString *keywords = inExtraInfo[kVMaxCustomAdExtras_Keyword];
        if (keywords) {
            self.bannerAd.keywords = keywords;
        }
        
        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
        [paramsDict setObject:@"c_vmax" forKey:@"tp"];
        [paramsDict setObject:kVMaxSDKVersion forKey:@"tp-ver"];
        self.bannerAd.extras = paramsDict;
        
        [self.bannerAd load];
    }
}

- (CGSize)bannerSizeFromSizeString:(NSString*)sizeString {
    sizeString = [sizeString stringByReplacingOccurrencesOfString:@"kVMaxAdSize_" withString:@""];
    NSArray *sizeArray = [sizeString componentsSeparatedByString:@"x"];
    if([sizeArray count] == 2) {
        return CGSizeMake([sizeArray[0] integerValue], [sizeArray[1] integerValue]);
    }
    
    return CGSizeZero;
}

- (void)showAd {
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

- (void)invalidateAd {
    
    self.delegate = nil;
    
    if (self.bannerAd.superview) {
        [self.bannerAd removeFromSuperview];
    }
    
    self.bannerAd.delegate = nil; self.bannerAd = nil;
}

#pragma mark -
- (void)dealloc {
    
    [self invalidateAd];
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}

#pragma mark IMBannerDelegate -
/**
 * Notifies the delegate that the banner has finished loading
 */
-(void)bannerDidFinishLoading:(IMBanner*)banner {
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didLoadAdInView:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didLoadAdInView:) withObject:self withObject:self.bannerAd];
    }
}

/**
 * Notifies the delegate that the banner has failed to load with some error.
 */
-(void)banner:(IMBanner*)banner didFailToLoadWithError:(IMRequestStatus*)error {
    NSLog(@"%@", error);
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
    }
}

/**
 * Notifies the delegate that the banner was interacted with.
 */
-(void)banner:(IMBanner*)banner didInteractWithParams:(NSDictionary*)params {
    NSLog(@"adViewDidClick");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidInteractAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidInteractAd:) withObject:self];
    }
}

/**
 * Notifies the delegate that the user would be taken out of the application context.
 */
-(void)userWillLeaveApplicationFromBanner:(IMBanner*)banner {
    NSLog(@"User will leave app from Inmobi banner ad");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }

}

/**
 * Notifies the delegate that the banner would be presenting a full screen content.
 */
-(void)bannerWillPresentScreen:(IMBanner*)banner {
    NSLog(@"Inmobi banner ad will present screen");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillPresentOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillPresentOverlay:) withObject:self];
    }
}

/**
 * Notifies the delegate that the banner has finished presenting screen.
 */
-(void)bannerDidPresentScreen:(IMBanner*)banner {
    NSLog(@"Inmobi banner ad did present screen");
}

/**
 * Notifies the delegate that the banner will start dismissing the presented screen.
 */
-(void)bannerWillDismissScreen:(IMBanner*)banner {
    NSLog(@"Inmobi banner ad will dismiss screen");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:) withObject:self];
    }
}

/**
 * Notifies the delegate that the banner has dismissed the presented screen.
 */
-(void)bannerDidDismissScreen:(IMBanner*)banner {
    NSLog(@"Inmobi banner ad did dismiss screen");
}

/**
 * Notifies the delegate that the user has completed the action to be incentivised with.
 */
-(void)banner:(IMBanner*)banner rewardActionCompletedWithRewards:(NSDictionary*)rewards {
    NSLog(@"Inmobi banner ad did complete reward action");
}

@end