//
//  VMaxInmobiNativeAdapter.m
//  VMaxTableViewSample
//
//  Created by Rahul CK on 23/09/15.
//  Copyright © 2015 VMax.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VMaxInmobiNativeAdapter.h"
#import "IMNativeDelegate.h"
#import "IMNative.h"
#import "IMRequestStatus.h"
#import "IMSdk.h"
#import "IMCommonConstants.h"

#define kVMaxSDKVersion                            @"A-IO-3.5.7"

static NSString* const kVMaxCustomAdAdapter_InmobiNativePlacementId = @"placementid";
static NSString* const kVMaxCustomAdAdapter_InmobiNativeAccountId   = @"accountid";
static NSString* const kVMaxCustomAdAdapter_InmobiNativeImpression = @"impression";

@interface VMaxInmobiNativeAdapter ()<IMNativeDelegate>
@property (nonatomic, strong) IMNative* nativeAd;
@property (nonatomic, strong) NSString* nativeContent;
@property (nonatomic, strong) NSArray* impressionUrls;
@property (nonatomic, strong) UIView* impressionRegisteredView;
@end


@implementation VMaxInmobiNativeAdapter


#pragma mark-VMax CustomAd
-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.delegate = inDelegate;
    self.parentViewController = parentViewController;
    
    NSString* placementId = [inParams objectForKey: kVMaxCustomAdAdapter_InmobiNativePlacementId];
    NSString* accountId = [inParams objectForKey: kVMaxCustomAdAdapter_InmobiNativeAccountId];
    
    
    self.impressionUrls = [inParams objectForKey:kVMaxCustomAdAdapter_InmobiNativeImpression];
    
    NSNumber* latitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLatitude];
    NSNumber* longitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLongitude];
    
    if (latitude && longitude) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude.doubleValue
                                                          longitude:longitude.doubleValue];
        [IMSdk setLocation:location];
    }
    
    //Gender
    NSString *gender = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Gender];
    if (gender) {
        
        IMSDKGender genderValue = ([gender isEqualToString:kVMaxCustomAdExtras_GenderMale])?kIMSDKGenderMale:kIMSDKGenderFemale;
        [IMSdk setGender:genderValue];
    }
    
    
    //Age
    NSString *age = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Age];
    if (age) {
        [IMSdk setAge:age.intValue];
    }
    
    
    if (placementId&&accountId) {
        [IMSdk initWithAccountID:accountId];
        [IMSdk setLogLevel:kIMSDKLogLevelDebug];
        self.nativeAd = [[IMNative alloc] initWithPlacementId:placementId.longLongValue];
        
        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
        [paramsDict setObject:@"c_vmax" forKey:@"tp"];
        [paramsDict setObject:kVMaxSDKVersion forKey:@"tp-ver"];
        self.nativeAd.extras = paramsDict;
        
        self.nativeAd.delegate = self;
        [self.nativeAd load];
        
        
        
    }
}

-(void)handleActionForClickOnView
{
    NSString *link = [self getLink];

    if (link && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:link]] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
        
    }
    [self.nativeAd reportAdClick:nil];
    
    if (self.nativeAdDelegate && [self.nativeAdDelegate respondsToSelector:@selector(nativeAdwillPresentOverlay:)]) {
        [self.nativeAdDelegate nativeAdwillPresentOverlay:self];
    }
    if (self.nativeAdDelegate && [self.nativeAdDelegate respondsToSelector:@selector(nativeAdwillLeaveApp:)]) {
        [self.nativeAdDelegate nativeAdwillLeaveApp:self];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
   

}


-(void)registerViewForInteraction:(VMaxAdView*)adView view:(UIView*)view listOfViews:(NSArray*)listOfViews
{
    [ super registerViewForInteraction:adView view:view listOfViews:nil]; // passing nill since click is handled in the adapter its self
    self.impressionRegisteredView = view;
     if(view && listOfViews)
      [IMNative bindNative:self.nativeAd toView:view];
    
    for (UIView* view in listOfViews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button =(UIButton*)view;
            [button addTarget:self action:@selector(handleActionForClickOnView) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else{
            
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                        action:@selector(handleActionForClickOnView)];
            [view addGestureRecognizer:tapGesture];
        }
    }
    
}

-(void)showAd{
    
}

-(void)invalidateAd
{
   
}
- (void)dealloc
{
    self.nativeAd.delegate = nil;
    self.nativeAd = nil;
    [IMNative unBindView:self.impressionRegisteredView];
}

-(void) extractNativeAdFieldsFromInMobiNativeAd:(IMNative*)imNativeAd
{
    
    if (imNativeAd.adContent)
    {
        NSData* data = [imNativeAd.adContent dataUsingEncoding:NSUTF8StringEncoding];
        NSError* error = nil;
        NSDictionary* jsonDict = nil;
       
        if(data)
          jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSMutableDictionary* nativeJsonDict = [NSMutableDictionary dictionaryWithDictionary:jsonDict];
        
        NSLog(@"InMobi Response Json: %@",nativeJsonDict);
        
        if (error == nil && nativeJsonDict != nil)
        {
            [nativeJsonDict setValue:[NSNumber numberWithBool:YES] forKey:@"isAd"]; // To differentiate the native JSON dictionary from other data.
            NSDictionary* imageIConDict = [nativeJsonDict valueForKey:@"imageIcon"];
            NSDictionary* imageMediumDict = [nativeJsonDict valueForKey:@"imageMedium"];
            NSDictionary* imageMainDict = [nativeJsonDict valueForKey:@"imageMain"];
            
            if (imageMainDict){
                self.imageMain = [[VMaxAdImage alloc]initWithURL:[NSURL URLWithString:[imageMainDict valueForKey:@"url"]]
                                                             width:[[imageMainDict valueForKey:@"width"]integerValue] height:
                                    [[imageMainDict valueForKey:@"height"]integerValue]];
                [self.imageMain loadImageAsyncWithBlock:nil];
            }
            
            if (imageMediumDict){
                self.imageMedium = [[VMaxAdImage alloc]initWithURL:[NSURL URLWithString:[imageMediumDict valueForKey:@"url"]]
                                                             width:[[imageMediumDict valueForKey:@"width"]integerValue] height:
                                    [[imageMediumDict valueForKey:@"height"]integerValue]];
                [self.imageMedium loadImageAsyncWithBlock:nil];

            }
            
            
            
            
            if (imageIConDict)
                self.imageIcon = [[VMaxAdImage alloc]initWithURL:[NSURL URLWithString:[imageIConDict valueForKey:@"url"]]
                                                           width:[[imageIConDict valueForKey:@"width"]integerValue] height:
                                  [[imageIConDict valueForKey:@"width"]integerValue]];
            
            self.rating  = [nativeJsonDict valueForKey:@"rating"];
            self.ctaText = [nativeJsonDict valueForKey:@"ctaText"];
            self.title = [nativeJsonDict valueForKey:@"title"];
            self.desc = [nativeJsonDict valueForKey:@"desc"];
            
            NSString *linkUrl = [[nativeJsonDict valueForKey:@"linkUrl"]
                            stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self setLinkUrl:linkUrl];
            
            NSString *linkFallBackUrl = [[nativeJsonDict valueForKey:@"linkFallback"]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [self setLinkFallback:linkFallBackUrl];
            
            [self setImpTrackers:self.impressionUrls];
            self.nativeAdPartner = @"InMobi";
     
            
        }
    }
    
    
    
}

#pragma mark-IMNativeDelegate

/*The native ad notifies its delegate that it is ready.Fetching publisher-specific ad asset content from native.adContent. The publisher will specify the format. If the publisher does not provide a format, no ad will be loaded.*/

-(void)nativeDidFinishLoading:(IMNative*)native
{
    
    [self extractNativeAdFieldsFromInMobiNativeAd:native];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
    }
}

/*The native ad notifies its delegate that an error has been encountered while trying to load the ad.Check IMRequestStatus.h for all possible errors.Try loading the ad again, later.*/

-(void)native:(IMNative*)native didFailToLoadWithError:(IMRequestStatus*)error
{
    NSLog(@"%@", error);
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
    }
}

/* Indicates that the native ad is going to present a screen. */
-(void)nativeWillPresentScreen:(IMNative*)native{
    NSLog(@"Native Ad will present screen");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillPresentOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillPresentOverlay:) withObject:self];
    }
}

/* Indicates that the native ad has presented a screen. */
-(void)nativeDidPresentScreen:(IMNative*)native
{
    NSLog(@"Native Ad did present screen");
}

/* Indicates that the native ad is going to dismiss the presented screen. */
-(void)nativeWillDismissScreen:(IMNative*)native
{
    NSLog(@"Native Ad will dismiss screen");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:) withObject:self];
    }
}

/* Indicates that the native ad has dismissed the presented screen. */
-(void)nativeDidDismissScreen:(IMNative*)native
{
    NSLog(@"Native Ad did dismiss screen");
}

/* Indicates that the user will leave the app. */
-(void)userWillLeaveApplicationFromNative:(IMNative*)native
{
    NSLog(@"User leave");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}

@end
