//
//  VMaxInmobiInterstitialAdapter.m
//  VMaxSample
//
//  Created by Anup Dsouza on 03/06/16.
//  Copyright © 2016 VMax. All rights reserved.
//

#import "VMaxInmobiInterstitialAdapter.h"
#import "IMInterstitialDelegate.h"
#import "IMInterstitial.h"
#import "IMCommonConstants.h"
#import "IMRequestStatus.h"
#import "IMSdk.h"

#define kVMaxSDKVersion                            @"A-IO-3.5.7"

static NSString* const kVMaxCustomAdAdapter_InMobiInterstitialPlacementId = @"placementid";
static NSString* const kVMaxCustomAdAdapter_InMobiInterstitialAccountId   = @"accountid";

@interface VMaxInmobiInterstitialAdapter () <IMInterstitialDelegate>
@property (nonatomic, strong) IMInterstitial *interstitialAd;
@property (nonatomic, strong) NSArray *impressionUrls;
@end

@implementation VMaxInmobiInterstitialAdapter
#pragma mark VMaxCustomAd -
- (void)loadCustomAd:(NSDictionary*)inParams
        withDelegate:(id<VMaxCustomAdListener>)inDelegate
      viewController:(UIViewController*)parentViewController
          withExtras:(NSDictionary*)inExtraInfo {
    
    self.delegate = inDelegate;
    self.parentViewController = parentViewController;
    
    NSString *placementId = inParams[kVMaxCustomAdAdapter_InMobiInterstitialPlacementId];
    NSString *accountId = inParams[kVMaxCustomAdAdapter_InMobiInterstitialAccountId];
    
    if (placementId && accountId) {
        
        // location info
        NSNumber *lat = inExtraInfo[kVMaxCustomAdExtras_LocationLatitude];
        NSNumber *lon = inExtraInfo[kVMaxCustomAdExtras_LocationLongitude];
        
        if (lat && lon) {
            CLLocation *location = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lon.doubleValue];
            [IMSdk setLocation:location];
        }
        
        NSString *gender = inExtraInfo[kVMaxCustomAdExtras_Gender]; // gender
        if (gender) {
            IMSDKGender genderValue = ([gender isEqualToString:kVMaxCustomAdExtras_GenderMale]) ? kIMSDKGenderMale:kIMSDKGenderFemale;
            [IMSdk setGender:genderValue];
        }
        
        NSString *age = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Age]; // age
        if (age) {
            [IMSdk setAge:age.intValue];
        }
        
        [IMSdk initWithAccountID:accountId];
        [IMSdk setLogLevel:kIMSDKLogLevelDebug];
        
        // interstitial init
        self.interstitialAd = [[IMInterstitial alloc] initWithPlacementId:placementId.longLongValue
                                                                 delegate:self];
        
        NSString *keywords = inExtraInfo[kVMaxCustomAdExtras_Keyword];
        if (keywords) {
            self.interstitialAd.keywords = keywords;
        }
        
        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
        [paramsDict setObject:@"c_vmax" forKey:@"tp"];
        [paramsDict setObject:kVMaxSDKVersion forKey:@"tp-ver"];
        self.interstitialAd.extras = paramsDict;
        
        [self.interstitialAd load];
    }
}

- (void)showAd {
    
    if (self.interstitialAd && self.interstitialAd.isReady) {
        [self.interstitialAd showFromViewController:self.parentViewController];
    }
}

- (void)invalidateAd {
    
    self.delegate = nil;
    
    if (self.interstitialAd && self.parentViewController.presentedViewController) {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
    self.interstitialAd.delegate = nil; self.interstitialAd = nil;
}

#pragma mark -
- (void)dealloc {
    
    [self invalidateAd];
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}

#pragma mark IMInterstitialDelegate -
/**
 * Notifies the delegate that the interstitial has finished loading
 */
-(void)interstitialDidFinishLoading:(IMInterstitial*)interstitial {
    
    NSLog(@"Success Callback:adViewDidLoad");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
        NSLog(@"Success Callback:DidReceiveAd");
        
    }
}
/**
 * Notifies the delegate that the interstitial has failed to load with some error.
 */
-(void)interstitial:(IMInterstitial*)interstitial didFailToLoadWithError:(IMRequestStatus*)error {
    NSLog(@"Interstitial failed to load ad");
    NSLog(@"Error : %@",error.description);
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
    }
}
/**
 * Notifies the delegate that the interstitial would be presented.
 */
-(void)interstitialWillPresent:(IMInterstitial*)interstitial {
    NSLog(@"interstitialWillPresent");
}
/**
 * Notifies the delegate that the interstitial has been presented.
 */
-(void)interstitialDidPresent:(IMInterstitial *)interstitial{
    NSLog(@"interstitialDidPresent");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}
/**
 * Notifies the delegate that the interstitial has failed to present with some error.
 */
-(void)interstitial:(IMInterstitial*)interstitial didFailToPresentWithError:(IMRequestStatus*)error{
    NSLog(@"Interstitial didFailToPresentWithError");
    NSLog(@"Error : %@",error.description);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
        
        NSLog(@"Failure Callback:%@", error);
    }
}
/**
 * Notifies the delegate that the interstitial will be dismissed.
 */
-(void)interstitialWillDismiss:(IMInterstitial*)interstitial{
    NSLog(@"interstitialWillDismiss");
}
/**
 * Notifies the delegate that the interstitial has been dismissed.
 */
-(void)interstitialDidDismiss:(IMInterstitial*)interstitial{
    NSLog(@"interstitialDidDismiss");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }
}
/**
 * Notifies the delegate that the interstitial has been interacted with.
 */
-(void)interstitial:(IMInterstitial*)interstitial didInteractWithParams:(NSDictionary*)params {
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
}
/**
 * Notifies the delegate that the user has performed the action to be incentivised with.
 */
-(void)interstitial:(IMInterstitial*)interstitial rewardActionCompletedWithRewards:(NSDictionary*)rewards {
    NSLog(@"rewardActionCompletedWithRewards");
}
/**
 * Notifies the delegate that the user will leave application context.
 */
-(void)userWillLeaveApplicationFromInterstitial:(IMInterstitial*)interstitial {
    NSLog(@"userWillLeaveApplicationFromInterstitial");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}

@end