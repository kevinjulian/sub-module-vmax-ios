//
//  VMaxInmobiNativeAdapter.h
//  VMaxTableViewSample
//
//  Created by Rahul CK on 23/09/15.
//  Copyright © 2015 VMax.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxNativeAd.h"

@interface VMaxInmobiNativeAdapter :VMaxNativeAd<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;
@property (nonatomic, strong) UIViewController* parentViewController;

@end
