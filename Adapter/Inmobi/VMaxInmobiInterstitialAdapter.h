//
//  VMaxInmobiInterstitialAdapter.h
//  VMaxSample
//
//  Created by Anup Dsouza on 03/06/16.
//  Copyright © 2016 VMax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxInmobiInterstitialAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id <VMaxCustomAdListener> delegate;
@property (nonatomic, strong) UIViewController* parentViewController;

@end