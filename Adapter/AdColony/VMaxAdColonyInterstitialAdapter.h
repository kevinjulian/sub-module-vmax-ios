//
//  AdColonyInterstetialAdapter.h
//  VMaxAdapterTest
//
//  Created by Tejus Adiga on 05/11/14.
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxAdColonyInterstitialAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, strong) UIViewController* parentViewController;


@end
