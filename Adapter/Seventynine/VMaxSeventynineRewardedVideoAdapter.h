//
//  VMaxSeventynineRewardedVideoAdapter.h
//  VMaxTableViewSample
//
//  Created by Rahul CK on 29/12/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxSeventynineRewardedVideoAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;
@property (nonatomic, strong) UIViewController *parentViewController;
@end
