//
//  VMaxSeventynineRewardedVideoAdapter.m
//  VMaxTableViewSample
//
//  Created by Rahul CK on 29/12/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import "VMaxSeventynineRewardedVideoAdapter.h"
#import "SNADViewManager.h"
NSString* const ksevntyNineParameter_PublishreId             = @"publisherid";
NSString* const ksevntyNineParameter_ZoneId                  = @"zoneid";
NSString* const ksevntyNine_Progress_VideoComplete           = @"com.seventynine.VideoCompleted";



@interface VMaxSeventynineRewardedVideoAdapter ()

@property (strong,nonatomic) NSString *zoneId;
@property (strong,nonatomic) NSString *videoProgress;

@end
@implementation VMaxSeventynineRewardedVideoAdapter
-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)inParentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    
    self.parentViewController = inParentViewController;
    self.delegate = inDelegate;
    NSString *publisherid= [inParams objectForKey:ksevntyNineParameter_PublishreId];
    
    [SNADViewManager changeSession];
    if ([inParams objectForKey:ksevntyNineParameter_ZoneId]) {
        self.zoneId = [inParams objectForKey:ksevntyNineParameter_ZoneId];
        
    }
    
    if (publisherid) {
        [SNADViewManager setRunTimeConfigration:@{SNpublisherId:publisherid,kSNADVariable_ZoneIdKey:self.zoneId
                                                  }];
        
    }
    
    
    __weak   VMaxSeventynineRewardedVideoAdapter *adapter = (VMaxSeventynineRewardedVideoAdapter*)self;
    
    [SNADViewManager isAdReadyWithZoneId:self.zoneId audioAd:NO developerHandleSize:NO completion:^(BOOL isReady) {
        
        if (isReady) {
            if (adapter.delegate && [adapter.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
                [adapter.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                                       withObject:adapter];
                NSLog(@"Success Callback:SeventyNine");
            }
        }
        else
        {
            NSError *error = [[NSError alloc]initWithDomain:NSOSStatusErrorDomain code:-1 userInfo:@{@"Error:":@"Failed to load seventynine ad."}];
            if (adapter.delegate && [adapter.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
                [adapter.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                                       withObject: adapter
                                       withObject: error];
            }
            
        }
    }];
    
    
    
    
}

-(void)showAd
{
    NSMutableDictionary *options = [ NSMutableDictionary new];
    
    if (self.zoneId) {
        
        [options setObject:self.zoneId forKey:kSNADVariable_ZoneIdKey];
        [options setObject:self forKey:kSNADVariable_DelegateKey];
        [options setObject:[UIColor blackColor] forKey:kSNADVariable_BackgroundColorKey];
        [options setObject:@(SeventynineAdThemeCustom) forKey:kSNADVariable_ThemeKey];
        [SNADViewManager showAdInViewController:self.parentViewController options:options];
        
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
}

#pragma mark- SeventyNine Delegates
- (void)adViewDidFailWithError:(NSError *)error
{
    NSLog(@"Failure Callback:adViewDidFailWithError");
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: error];
    }
    
    
}
- (void)adViewWillRemoveWithAdid:(NSString *)adId adState:(ADViewState)adstate adType:(SeventynineAdType)adType
{
    NSLog(@"adViewWillRemoveWithAdid");
    if ([self.videoProgress isEqualToString:ksevntyNine_Progress_VideoComplete]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]) {
            [self.delegate VMaxCustomAd:self didCompleteRewardVideo:0];
        }
        
    }
    
    
}
- (void)handleAdClicked:(NSString *)urlString adid:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"handleAdClicked");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }
    
    
    
}

- (void)delegateFired:(NSString *)delegateString adid:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"delegateFired:%@",delegateString);
    
    self.videoProgress = delegateString;
    
    
    
}

- (void)adWillPresent:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"adWillPresent");
}
- (void)adDidPresent:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"adDidPresent");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                            withObject:self];
    }
    
}

- (void)adWillClose:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"adWillClose");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:)
                            withObject:self];
    }
    
}
- (void)adDidClose:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"adDidClose");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
    
}
- (void)adWillLeaveApplication:(NSString *)adId adType:(SeventynineAdType)adType
{
    NSLog(@"adWillLeaveApplication");
    
    
    
}


@end
