//
//  GoogleAdMobInterstetialAdapter.h
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMaxCustomAd.h"

@interface VMaxGoogleMobileAdsInterstitialAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

@end
