//
//  VMaxGoogleMobileAdsNativeAdapter.m
//  VMaxTableViewSample
//
//  Created by Rahul CK on 14/03/16.
//  Copyright © 2016 Vserv.mobi. All rights reserved.
//

#import "VMaxGoogleMobileAdsNativeAdapter.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <CoreLocation/CoreLocation.h>


NSString* const kVMaxAdapterGoogleAdMobNativeUnitId = @"adunitid";
static NSString* const kVMaxCustomAdAdapter_GoogleAdMobNaiveImpression = @"impression";



@interface VMaxGoogleMobileAdsNativeAdapter ()<GADNativeContentAdLoaderDelegate,GADNativeAppInstallAdLoaderDelegate,GADNativeAdDelegate>

@property (nonatomic,strong) GADAdLoader *gadAdLoader;
@property (nonatomic,strong) GADNativeAd *gadNativeAd;
@property (nonatomic,strong) NSArray *impressionUrls;


@end

@implementation VMaxGoogleMobileAdsNativeAdapter

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.delegate = inDelegate;
    self.parentViewController = parentViewController;
    
    self.impressionUrls = [inParams objectForKey:kVMaxCustomAdAdapter_GoogleAdMobNaiveImpression];
    
    NSString* unitId = [inParams objectForKey:kVMaxAdapterGoogleAdMobNativeUnitId];
    GADRequest* nativeAdRequest = [GADRequest request];
    
    
    NSString *keyword = nil;
    
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_Keyword]) {
        keyword = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Keyword];
    }
    
    
    if (keyword) {
        nativeAdRequest.keywords = @[keyword];
    }
    
    if (inExtraInfo) {
        
        NSString* inGender = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Gender];
        nativeAdRequest.gender = kGADGenderUnknown;
        
        if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderMale]) {
            nativeAdRequest.gender = kGADGenderMale;
        } else if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderFemale]) {
            nativeAdRequest.gender = kGADGenderFemale;
        }
        NSNumber* latitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLatitude];
        NSNumber* longitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLongitude];
        if (latitude && longitude) {
            [nativeAdRequest setLocationWithLatitude: latitude.floatValue
                                           longitude: latitude.floatValue
                                            accuracy: kCLLocationAccuracyKilometer];
        }
        if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
            
            NSArray *testDevices = [inExtraInfo objectForKey:kVMaxCustomAdExtras_TestDevices];
            if (testDevices.count) {
                nativeAdRequest.testDevices = testDevices;
                NSLog(@"Added AdMob Test Devices:%@",testDevices);
            }
            else{
                nativeAdRequest.testDevices = @[kGADSimulatorID];
            }
        }
    }
    
    self.gadAdLoader = [[GADAdLoader alloc]
                        initWithAdUnitID:unitId
                        rootViewController:self.parentViewController
                        adTypes:@[kGADAdLoaderAdTypeNativeAppInstall,kGADAdLoaderAdTypeNativeContent]
                        options:nil];
    
    self.gadAdLoader.delegate = self;
    [self.gadAdLoader loadRequest:nativeAdRequest];
    
    
    
    
}

-(void)showAd
{
    
}

-(void)invalidateAd
{
    
}
-(void)registerViewForInteraction:(VMaxAdView*)adView view:(UIView*)view listOfViews:(NSArray*)listOfViews
{
    [ super registerViewForInteraction:adView view:view listOfViews:nil]; // passing nill since click is handled in the adapter its self
    
    UIView *contentView = ([view viewWithTag:501])?[view viewWithTag:501]:view;
    
    if ([contentView isKindOfClass:[GADNativeAppInstallAdView class]]) {
        
        GADNativeAppInstallAdView *appInstallAdView =[view viewWithTag:501];
        if (appInstallAdView && [appInstallAdView isKindOfClass:[GADNativeAppInstallAdView class]]) {
            appInstallAdView.nativeAppInstallAd =(GADNativeAppInstallAd*) self.gadNativeAd;
            appInstallAdView.callToActionView.userInteractionEnabled = NO;
        }
    }
    else if ([contentView isKindOfClass:[GADNativeContentAdView class]])
    {
        GADNativeContentAdView *appInstallAdView =[view viewWithTag:501];
        if (appInstallAdView && [appInstallAdView isKindOfClass:[GADNativeContentAdView class]]) {
            appInstallAdView.nativeContentAd = (GADNativeContentAd*) self.gadNativeAd;
            appInstallAdView.callToActionView.userInteractionEnabled = NO;
        }
    }
    
}



#pragma mark-GoogleAd Delegates
- (void)adLoader:(GADAdLoader *)adLoader didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"%@",[error localizedDescription]);
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)] ) {
        NSLog(@"Failure Callback:%@", error);
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
    }
    
    
}
- (void)adLoader:(GADAdLoader *)adLoader didReceiveNativeContentAd:(GADNativeContentAd *)nativeContentAd
{
    self.gadNativeAd = nativeContentAd;
    self.title = nativeContentAd.headline;
    self.ctaText = nativeContentAd.callToAction;
    self.desc = nativeContentAd.body;
    
    nativeContentAd.delegate = self;
    
    if (nativeContentAd.logo) {
        self.imageIcon = [[VMaxAdImage alloc]initWithURL:nativeContentAd.logo.imageURL
                                                   width:nativeContentAd.logo.scale height:nativeContentAd.logo.scale];
        self.imageIcon.image = nativeContentAd.logo.image;
    }
   
    
    GADNativeAdImage *gadMainImage = nativeContentAd.images.firstObject;
    
    self.imageMain  = [[VMaxAdImage alloc]initWithURL:gadMainImage.imageURL
                                                width:gadMainImage.scale height:gadMainImage.scale];
    self.imageMain.image = gadMainImage.image;
    
    
    GADNativeAdImage *gadImageMedium = [nativeContentAd.images lastObject];
    
    self.imageMedium = [[VMaxAdImage alloc]initWithURL:gadImageMedium.imageURL
                                                 width:gadImageMedium.scale height:gadImageMedium.scale];
    self.imageMedium.image = gadImageMedium.image;
    
    [self setNativeAdPartner:@"Admob"];
    [self setNativeAdType:kVMaxNativeAdType_ContentAd];
    [self setObjective:@"Content_Ad"];
    [self setImpTrackers:self.impressionUrls];
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
    }
    
    @try {
        
        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setValue:nativeContentAd.headline forKey:@"headline"];
        [response setValue:nativeContentAd.body forKey:@"body"];
        [response setValue:nativeContentAd.callToAction forKey:@"callToAction"];
        [response setValue:nativeContentAd.advertiser forKey:@"advertiser"];
        [response setValue:nativeContentAd.logo.imageURL forKey:@"logo"];
        [response setValue:nativeContentAd.images forKey:@"images"];
        [response setValue:@"GADNativeContentAd" forKey:@"responseType"];
        
        NSLog(@"Admob Native Ad Response: %@", response);
    }
    @catch (NSException *exception) {
        
    }
    
    
}
- (void)adLoader:(GADAdLoader *)adLoader didReceiveNativeAppInstallAd:(GADNativeAppInstallAd *)nativeAppInstallAd
{
    
    self.gadNativeAd = nativeAppInstallAd;
    self.title = nativeAppInstallAd.headline;
    self.ctaText = nativeAppInstallAd.callToAction;
    self.desc = nativeAppInstallAd.body;
    self.price = nativeAppInstallAd.price;
    nativeAppInstallAd.delegate = self;
    [self setObjective:@"App_Install_Ad"];
    
    self.rating = [NSString stringWithFormat:@"%ld",(long)nativeAppInstallAd.starRating.integerValue];
    
    self.imageIcon = [[VMaxAdImage alloc]initWithURL:nativeAppInstallAd.icon.imageURL
                                               width:nativeAppInstallAd.icon.scale height:nativeAppInstallAd.icon.scale];
    
  
    self.imageIcon = [[VMaxAdImage alloc]initWithURL:nativeAppInstallAd.icon.imageURL
                                               width:nativeAppInstallAd.icon.scale height:nativeAppInstallAd.icon.scale];
    
    self.imageIcon.image = nativeAppInstallAd.icon.image;
    
    GADNativeAdImage *gadMainImage = nativeAppInstallAd.images.firstObject;
    
    self.imageMain  = [[VMaxAdImage alloc]initWithURL:gadMainImage.imageURL
                                                width:gadMainImage.scale height:gadMainImage.scale];
    self.imageMain.image = gadMainImage.image;

   
    GADNativeAdImage *gadImageMedium = [nativeAppInstallAd.images lastObject];
    
    self.imageMedium = [[VMaxAdImage alloc]initWithURL:gadImageMedium.imageURL
                                                 width:gadImageMedium.scale height:gadImageMedium.scale];
    self.imageMedium.image = gadImageMedium.image;

    [self setNativeAdPartner:@"Admob"];
    [self setNativeAdType:kVMaxNativeAdType_AppInstall];
    [self setImpTrackers:self.impressionUrls];

    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
    }
    
    @try {
        
        NSMutableDictionary *response = [NSMutableDictionary dictionary];
        [response setValue:nativeAppInstallAd.headline forKey:@"headline"];
        [response setValue:nativeAppInstallAd.store forKey:@"store"];
        [response setValue:nativeAppInstallAd.price forKey:@"price"];
        [response setValue:nativeAppInstallAd.callToAction forKey:@"callToAction"];
        [response setValue:nativeAppInstallAd.icon.imageURL forKey:@"iconUrl"];
        [response setValue:nativeAppInstallAd.store forKey:@"store"];
        [response setValue:nativeAppInstallAd.body forKey:@"body"];
        [response setValue:nativeAppInstallAd.images forKey:@"images"];
        [response setValue:nativeAppInstallAd.starRating forKey:@"starRating"];
        [response setValue:@"GADNativeAppInstallAd" forKey:@"responseType"];
        
        NSLog(@"Admob Native Ad Response: %@", response);
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)nativeAdWillPresentScreen:(GADNativeAd *)nativeAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillPresentOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillPresentOverlay:) withObject:self];
    }
   
    if (self.nativeAdDelegate && [self.nativeAdDelegate respondsToSelector:@selector(nativeAdwillPresentOverlay:)]) {
        [self.nativeAdDelegate performSelector:@selector(nativeAdwillPresentOverlay:) withObject:self];
    }
   
}

- (void)nativeAdWillDismissScreen:(GADNativeAd *)nativeAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:) withObject:self];
    }
    
    if (self.nativeAdDelegate && [self.nativeAdDelegate respondsToSelector:@selector(nativeAdwillDismissOverlay:)]) {
        [self.nativeAdDelegate performSelector:@selector(nativeAdwillDismissOverlay:) withObject:self];
    }

}

- (void)nativeAdDidDismissScreen:(GADNativeAd *)nativeAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}

- (void)nativeAdWillLeaveApplication:(GADNativeAd *)nativeAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
    
    if (self.nativeAdDelegate && [self.nativeAdDelegate respondsToSelector:@selector(nativeAdwillLeaveApp:)]) {
        [self.nativeAdDelegate performSelector:@selector(nativeAdwillLeaveApp:) withObject:self];
    }
}

@end
