//
//  GogleAdMobbannerAdapter.m
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import "VMaxGoogleMobileAdsBannerAdapter.h"

#import <CoreLocation/CoreLocation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NSString* const kVMaxAdapterGoogleAdMobBannerUnitId = @"adunitid";

@interface VMaxGoogleMobileAdsBannerAdapter () <GADBannerViewDelegate>

@property (strong, nonatomic) GADBannerView* bannerView;

@end

@implementation VMaxGoogleMobileAdsBannerAdapter


-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    self.delegate = inDelegate;
    
    NSString* unitId = [inParams objectForKey:kVMaxAdapterGoogleAdMobBannerUnitId];
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    
    NSString *adSize = nil;
    NSInteger adSizeScale = 0;
    
    
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]) {
        adSize = [[inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]objectForKey:kVMaxCustomAdExtras_AdSize];
    }
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]) {
        adSizeScale = [[[inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]objectForKey:kVMaxCustomAdExtras_Scale]integerValue];
    }
    NSString *keyword = nil;
    
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_Keyword]) {
        keyword = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Keyword];
    }
    
    GADAdSize bannerSize = kGADAdSizeBanner;
    
    if (adSize) {
        if ([adSize isEqualToString:kVMaxCustomAdAdSize_320x50]){bannerSize = kGADAdSizeBanner;}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_728x90]){bannerSize = kGADAdSizeLeaderboard;}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_300x250]){bannerSize = kGADAdSizeMediumRectangle;}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_480x320]){bannerSize = GADAdSizeFromCGSize(CGSizeMake(480, 320));}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_320x480]){bannerSize = GADAdSizeFromCGSize(CGSizeMake(320, 480));}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_480x800]){bannerSize = GADAdSizeFromCGSize(CGSizeMake(480, 800));}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_1024x600]){bannerSize = GADAdSizeFromCGSize(CGSizeMake(1024, 600));}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_800x480]){bannerSize = GADAdSizeFromCGSize(CGSizeMake(800, 480));}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_600x1024]){bannerSize = GADAdSizeFromCGSize(CGSizeMake(600, 1024));}
        
        else
        {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                bannerSize = kGADAdSizeSmartBannerPortrait;
            } else {
                bannerSize = kGADAdSizeSmartBannerLandscape;
            }
            
        }
        //For Handling Scaling
        if (adSizeScale>0 && adSizeScale<200) {
            CGFloat width =  (bannerSize.size.width*adSizeScale)/100; ;
            CGFloat height =  (bannerSize.size.height*adSizeScale)/100; ;
            bannerSize = GADAdSizeFromCGSize(CGSizeMake(width, height));
            
        }
    }
    else
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            bannerSize = kGADAdSizeLeaderboard;
        }
        else
        {
            bannerSize= kGADAdSizeBanner;
        }
    }
    if (unitId) {
        
        self.bannerView = [[GADBannerView alloc] initWithAdSize: bannerSize];
        self.bannerView.rootViewController = parentViewController;
        self.bannerView.adUnitID = unitId;
        self.bannerView.delegate = self;
        
        GADRequest* bannerRequest = [GADRequest request];
        
        if (keyword) {
            bannerRequest.keywords = @[keyword];
        }
        
        if (inExtraInfo) {
            NSString* inGender = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Gender];
            
            bannerRequest.gender = kGADGenderUnknown;
            
            if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderMale]) {
                bannerRequest.gender = kGADGenderMale;
            } else if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderFemale]) {
                bannerRequest.gender = kGADGenderFemale;
            }
            NSNumber* latitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLatitude];
            NSNumber* longitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLongitude];
            
            NSLog(@"VMax USER PARAM: Gender: %@, Lat: %@, Long: %@", inGender, latitude, longitude);
            
            if (latitude && longitude) {
                [bannerRequest setLocationWithLatitude: latitude.floatValue
                                             longitude: latitude.floatValue
                                              accuracy: kCLLocationAccuracyKilometer];
            }
        }
        if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
            
            NSArray *testDevices = [inExtraInfo objectForKey:kVMaxCustomAdExtras_TestDevices];
            if (testDevices.count) {
                bannerRequest.testDevices = testDevices;
                NSLog(@"Added AdMob Test Devices:%@",testDevices);
            }
            else{
                bannerRequest.testDevices = @[kGADSimulatorID];
            }
            
        }
        [self.bannerView loadRequest:bannerRequest];
    }
}

-(void)showAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

-(void)invalidateAd
{
    
    if (self.bannerView.superview) {
        [self.bannerView removeFromSuperview];
        [self.bannerView loadRequest:nil];
        self.bannerView = nil;
    }
}

-(void)dealloc
{
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}

#pragma mark GADBannerViewDelegate Ad Request Lifecycle Notifications

/// Called when an ad request loaded an ad. This is a good opportunity to add this view to the
/// hierarchy if it has not been added yet. If the ad was received as a part of the server-side auto
/// refreshing, you can examine the hasAutoRefreshed property of the view.
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Success Callback:DidReceiveAd");
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didLoadAdInView:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didLoadAdInView:)
                            withObject: self
                            withObject: view];
    }
    
    
}
/// Called when an ad request failed. Normally this is because no network connection was available
/// or no ads were available (i.e. no fill). If the error was received as a part of the server-side
/// auto refreshing, you can examine the hasAutoRefreshed property of the view.
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Failure Callback:%@", error);
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: error];
    }
}

#pragma mark GADBannerViewDelegate Click-Time Lifecycle Notifications

/// Called just before presenting the user a full screen view, such as a browser, in response to
/// clicking on an ad. Use this opportunity to stop animations, time sensitive interactions, etc.
///
/// Normally the user looks at the ad, dismisses it, and control returns to your application by
/// calling adViewDidDismissScreen:. However if the user hits the Home button or clicks on an App
/// Store link your application will end. On iOS 4.0+ the next method called will be
/// applicationWillResignActive: of your UIViewController
/// (UIApplicationWillResignActiveNotification). Immediately after that adViewWillLeaveApplication:
/// is called.
- (void)adViewWillPresentScreen:(GADBannerView *)adView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
}

/// Called just before dismissing a full screen view.
- (void)adViewWillDismissScreen:(GADBannerView *)adView
{
    
}

/// Called just after dismissing a full screen view. Use this opportunity to restart anything you
/// may have stopped as part of adViewWillPresentScreen:.
- (void)adViewDidDismissScreen:(GADBannerView *)adView
{
    
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store). The normal
/// UIApplicationDelegate methods, like applicationDidEnterBackground:, will be called immediately
/// before this.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}

@end

