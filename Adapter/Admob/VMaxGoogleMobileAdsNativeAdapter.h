//
//  VMaxGoogleMobileAdsNativeAdapter.h
//  VMaxTableViewSample
//
//  Created by Rahul CK on 14/03/16.
//  Copyright © 2016 Vserv.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxNativeAd.h"

@interface VMaxGoogleMobileAdsNativeAdapter :VMaxNativeAd<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;
@property (nonatomic, strong) UIViewController* parentViewController;

@end
