//
//  GoogleAdMobInterstetialAdapter.m
//
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import "VMaxGoogleMobileAdsInterstitialAdapter.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NSString* const kVMaxAdapterGoogleAdMobInterstetialUnitId = @"adunitid";

@interface VMaxGoogleMobileAdsInterstitialAdapter () <GADInterstitialDelegate>

@property (strong, nonatomic) GADInterstitial* interstetialAd;

@end

@implementation VMaxGoogleMobileAdsInterstitialAdapter

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    
    self.delegate = inDelegate;
    
    NSString* unitId = [inParams objectForKey:kVMaxAdapterGoogleAdMobInterstetialUnitId];
    NSString *keyword = nil;
    
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_Keyword]) {
        keyword = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Keyword];
    }

    
    if (unitId) {
        
        self.interstetialAd = [[GADInterstitial alloc] initWithAdUnitID:unitId];
        self.interstetialAd.delegate = self;
        
        GADRequest* interstetialRequest = [GADRequest request];
                
        if (keyword) {
            interstetialRequest.keywords = @[keyword];
        }
        
        if (inExtraInfo) {
            NSString* inGender = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Gender];
            
            interstetialRequest.gender = kGADGenderUnknown;
            
            if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderMale]) {
                interstetialRequest.gender = kGADGenderMale;
            } else if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderFemale]) {
                interstetialRequest.gender = kGADGenderFemale;
            }
            
            NSNumber* latitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLatitude];
            NSNumber* longitude = [inExtraInfo objectForKey: kVMaxCustomAdExtras_LocationLongitude];
            
            if (latitude && longitude) {
                [interstetialRequest setLocationWithLatitude: latitude.floatValue
                                                   longitude: latitude.floatValue
                                                    accuracy: kCLLocationAccuracyKilometer];
            }
        }
        if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
            
            NSArray *testDevices = [inExtraInfo objectForKey:kVMaxCustomAdExtras_TestDevices];
            if (testDevices.count) {
                interstetialRequest.testDevices = testDevices;
                NSLog(@"Added AdMob Test Devices:%@",testDevices);
            }
            
        }

        [self.interstetialAd loadRequest:interstetialRequest];
    }
}

-(void)showAd
{
    if (self.interstetialAd) {
        
        while (self.parentViewController.isBeingPresented || self.parentViewController.isBeingDismissed) {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.3]];
        }
        
        [self.interstetialAd presentFromRootViewController: self.parentViewController];
    }
}

-(void)invalidateAd
{
    self.interstetialAd = nil;
    self.delegate = nil;
}

//-(void)dealloc
//{
//    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
//}

#pragma mark GADInterstitialDelegate Ad Request Lifecycle Notifications

/// Called when an interstitial ad request succeeded. Show it at the next transition point in your
/// application such as when transitioning between view controllers.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    NSLog(@"Success Callback:DidReceiveAd");

    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAdDidLoadAd:)
                            withObject: self];
    }
    
}

/// Called when an interstitial ad request completed without an interstitial to
/// show. This is common since interstitials are shown sparingly to users.
- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Failure Callback:%@", error);
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: error];
    }
}

#pragma mark GADInterstitialDelegate Display-Time Lifecycle Notifications

/// Called just before presenting an interstitial. After this method finishes the interstitial will
/// animate onto the screen. Use this opportunity to stop animations and save the state of your
/// application in case the user leaves while the interstitial is on screen (e.g. to visit the App
/// Store from a link on the interstitial).
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad
{
    
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store). The normal
/// UIApplicationDelegate methods, like applicationDidEnterBackground:, will be called immediately
/// before this.
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:) withObject:self];
    }
}
@end