//
//  ChartboostInterstetialAdapter.h
//  VMaxChartboostSample
//
//  Created by RAHUL CK on 03/11/14.
//  Copyright (c) 2014 robosoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"


@interface VMaxChartBoostInterstitialAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController *parentViewController;

@end
