//
//  ChartboostInterstetialAdapter.m
//  VMaxChartboostSample
//
//  Created by RAHUL CK on 03/11/14.
//  Copyright (c) 2014 robosoft. All rights reserved.
//

#import "VMaxChartBoostInterstitialAdapter.h"

#import <Chartboost/Chartboost.h>
#import <Chartboost/CBInPlay.h>
#import <CommonCrypto/CommonDigest.h>
#import <AdSupport/AdSupport.h>

NSString* const kchartboostParameter_AppId              = @"appid";
NSString* const kchartboostParameter_Signatire          = @"appsignature";
NSString* const kchartboostParameter_AdType             = @"adtype";
NSString* const kchartboostParameter_Location           = @"location";


NSString* const kchartboostParameter_AdTypeInterstitial = @"interstitial";
NSString* const kchartboostParameter_AdTypeInPlay       = @"inplay";
NSString* const kchartboostParameter_AdTypeMoreApps     = @"more apps";
NSString* const kChartboostParameter_AdTypeRewarded     = @"Rewarded video";

@interface VMaxChartBoostInterstitialAdapter () <ChartboostDelegate>

@property (nonatomic, strong) CBInPlay* inPlayAd;

@property (nonatomic, strong) NSString* adType;
@property (nonatomic, strong) NSString* location;

@property (nonatomic, assign) BOOL isCachingFirstTime;

@end

@implementation VMaxChartBoostInterstitialAdapter

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)inParentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
   
    
    self.delegate = inDelegate;
    self.isCachingFirstTime = YES;
    
    self.parentViewController = inParentViewController;
    
    NSString* appId     = nil;
    NSString* signature = nil;
    
    self.location  = CBLocationDefault;

    if ([inParams objectForKey: kchartboostParameter_AppId]) {
        appId = [inParams objectForKey: kchartboostParameter_AppId];
    }
    
    if ([inParams objectForKey: kchartboostParameter_Signatire]) {
        signature = [inParams objectForKey: kchartboostParameter_Signatire];
    }
    
    if ([inParams objectForKey: kchartboostParameter_AdType]) {
        self.adType = [inParams objectForKey: kchartboostParameter_AdType];
    }
    
    if ([inParams objectForKey: kchartboostParameter_Location]) {
        self.location = [inParams objectForKey: kchartboostParameter_Location];
    }
    
    if (appId && signature && self.adType) {
        [Chartboost startWithAppId: appId
                      appSignature: signature
                          delegate: self];
        
        if ([self.adType caseInsensitiveCompare: kchartboostParameter_AdTypeMoreApps] == NSOrderedSame){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [Chartboost cacheMoreApps:self.location];
                
            });
            
            
        } else if ([self.adType caseInsensitiveCompare:kchartboostParameter_AdTypeInPlay]==NSOrderedSame){

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [Chartboost cacheInPlay: self.location];
                
            });
            
        } else if ([self.adType caseInsensitiveCompare:kchartboostParameter_AdTypeInterstitial]==NSOrderedSame){
            
           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [Chartboost cacheInterstitial: self.location];

           });

        } else if ([self.adType caseInsensitiveCompare:kChartboostParameter_AdTypeRewarded]==NSOrderedSame) {

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [Chartboost cacheRewardedVideo:self.location];
                
            });
        }
    else {
        
        NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:-1 userInfo:nil];
        
        if ([self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
            [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                                withObject: self
                                withObject: err];
            }
        NSLog(@"Failure Callback:didFailToLoadMoreApps");
        
        }
    }
}

-(void)showAd
{
    if ([self.adType caseInsensitiveCompare: kchartboostParameter_AdTypeMoreApps] == NSOrderedSame) {
        
            [Chartboost showMoreApps:self.parentViewController location:self.location];
        
    } else if ([self.adType caseInsensitiveCompare: kchartboostParameter_AdTypeInPlay] == NSOrderedSame) {
        
            self.inPlayAd = [Chartboost getInPlay: self.location];
        
    } else if ([self.adType caseInsensitiveCompare: kChartboostParameter_AdTypeRewarded] ==NSOrderedSame){
        
        [Chartboost showRewardedVideo:self.location];
        
    } else {
            [Chartboost showInterstitial: self.location];
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
}

//-(void)dealloc
//{
//    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
//}

#pragma mark-Chartboost Delegate

// Called before requesting an interstitial via the Chartboost API server.
- (BOOL)shouldRequestInterstitial:(CBLocation)location
{
    return YES;
}

// Called before an interstitial will be displayed on the screen.
- (BOOL)shouldDisplayInterstitial:(CBLocation)location
{
    return YES;

}

// Called after an interstitial has been displayed on the screen.
- (void)didDisplayInterstitial:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                            withObject:self];
    }
}

// Called after an interstitial has been loaded from the Chartboost API
// servers and cached locally.
- (void)didCacheInterstitial:(CBLocation)location
{
    if (self && self.isCachingFirstTime) {
        self.isCachingFirstTime = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
            [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                                withObject:self];
            NSLog(@"Success Callback:didCache Chartboost Ad");

        }
    }
}

// Called after an interstitial has attempted to load from the Chartboost API
// servers but failed.
- (void)didFailToLoadInterstitial:(CBLocation)location
                        withError:(CBLoadError)error
{
    NSLog(@"Failure Callback:Chartboost Error in fetching ad with Location %@: %ld", location,(unsigned long)error);
    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:error userInfo:nil];
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: err];
    }
}

// Called after a click is registered, but the user is not forwarded to the App Store.
- (void)didFailToRecordClick:(CBLocation)location
                   withError:(CBClickError)error
{

}

// Called after an interstitial has been dismissed.
- (void)didDismissInterstitial:(CBLocation)location
{
    NSLog(@"didDismissInterstitial");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
}

// Called after an interstitial has been closed.
- (void)didCloseInterstitial:(CBLocation)location
{
    NSLog(@"didCloseInterstitial");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
}

// Called after an interstitial has been clicked.
- (void)didClickInterstitial:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }
}

#pragma mark-Chartboost Delegate

// Called before a MoreApps page will be displayed on the screen.
- (BOOL)shouldDisplayMoreApps:(CBLocation)location
{
    return YES;
}

// Called after a MoreApps page has been displayed on the screen.
- (void)didDisplayMoreApps:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                            withObject:self];
    }
}

// Called after a MoreApps page has been loaded from the Chartboost API
// servers and cached locally.
- (void)didCacheMoreApps:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                            withObject:self];
        NSLog(@"Success Callback:didCache Chartboost Ad");

    }
}

// Called after a MoreApps page has been dismissed.
- (void)didDismissMoreApps:(CBLocation)location
{
    NSLog(@"didDismissMoreApps");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
}

// Called after a MoreApps page has been closed.
- (void)didCloseMoreApps:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
}

// Called after a MoreApps page has been clicked.
- (void)didClickMoreApps:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }
}

// Called after a MoreApps page attempted to load from the Chartboost API
// servers but failed.
- (void)didFailToLoadMoreApps:(CBLocation)location
                    withError:(CBLoadError)error
{
    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:error userInfo:nil];
    NSLog(@"Failure Callback:didFailToLoadMoreApps");
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: err];
    }
}

#pragma mark - InPlay Delegate

/*!
 @abstract
 Called after an InPlay object has been loaded from the Chartboost API
 servers and cached locally.
 
 @param location The location for the Chartboost impression type.
 
 @discussion Implement to be notified of when an InPlay object has been loaded from the Chartboost API
 servers and cached locally for a given CBLocation.
 */
- (void)didCacheInPlay:(CBLocation)location
{
    NSLog(@"Success Callback:didCache Chartboost Ad");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                            withObject:self];
    }
}

/*!
 @abstract
 Called after a InPlay has attempted to load from the Chartboost API
 servers but failed.
 
 @param location The location for the Chartboost impression type.
 
 @param error The reason for the error defined via a CBLoadError.
 
 @discussion Implement to be notified of when an InPlay has attempted to load from the Chartboost API
 servers but failed for a given CBLocation.
 */
- (void)didFailToLoadInPlay:(CBLocation)location
                  withError:(CBLoadError)error
{
    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:error userInfo:nil];
    NSLog(@"Failure Callback:%@",[err localizedDescription]);
    
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: err];
    }
}


#pragma mark - Rewarded Video ads delegate methods

/*
 Called before a rewarded video will be displayed on the screen.
 This is evaluated if the showRewardedVideo:(CBLocation)
 is called.  If YES is returned the operation will proceed, if NO, then the
 operation is treated as a no-op and nothing is displayed*/

- (BOOL)shouldDisplayRewardedVideo:(CBLocation)location {
    return YES;
}


 //Called after a rewarded video has been displayed on the screen
- (void)didDisplayRewardedVideo:(CBLocation)location{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                            withObject:self];
    }
}

/*!
  Called after a rewarded video has been loaded from the Chartboost API
 servers and cached locally.
 */
- (void)didCacheRewardedVideo:(CBLocation)location
{
    NSLog(@"Success Callback:didCache Chartboost Ad");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                            withObject:self];

    }
}


/*!
 Called after a rewarded video has attempted to load from the Chartboost API
 servers but failed.
 */
- (void)didFailToLoadRewardedVideo:(CBLocation)location
                         withError:(CBLoadError)error{

    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:error userInfo:nil];
    NSLog(@"Failure Callback:%@",[err localizedDescription]);

    
    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: err];
    }
}

/*!
 Called after a rewarded video has been dismissed.
 To be notified of when a rewarded video has been dismissed for a given CBLocation.
 */
- (void)didDismissRewardedVideo:(CBLocation)location{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
}

/*!
 Called after a rewarded video has been closed.
 To be notified of when a rewarded video has been closed for a given CBLocation.
 */
- (void)didCloseRewardedVideo:(CBLocation)location {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }
}

/*!
 Called after a rewarded video has been clicked.
 To be notified of when a rewarded video has been click for a given CBLocation.
 */
- (void)didClickRewardedVideo:(CBLocation)location
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }
}

/*! 
 Called after a rewarded video has been viewed completely and user is eligible for reward.
 To be notified of when a rewarded video has been viewed completely and user is eligible for reward.
 */
- (void)didCompleteRewardedVideo:(CBLocation)location
                      withReward:(int)reward
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]) {
        [self.delegate VMaxCustomAd:self didCompleteRewardVideo:reward];
    }
}

@end
