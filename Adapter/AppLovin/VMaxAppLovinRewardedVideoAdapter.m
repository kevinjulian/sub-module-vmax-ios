//
//  VMaxAppLovinRewardedVideoAdapter.m
//  VMaxTableViewSample
//
//  Created by Rahul CK on 22/01/16.
//  Copyright © 2016 VMax.com. All rights reserved.
//

#import "VMaxAppLovinRewardedVideoAdapter.h"
#import "ALSdk.h"
#import  "ALInterstitialAd.h"

@interface VMaxAppLovinRewardedVideoAdapter ()<ALAdDisplayDelegate,ALAdLoadDelegate,ALAdVideoPlaybackDelegate>
@property BOOL isLoadAdCalled;
@end

@implementation VMaxAppLovinRewardedVideoAdapter
-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)inParentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    
    self.parentViewController = inParentViewController;
    self.delegate = inDelegate;
    self.isLoadAdCalled = NO;
    [ALSdk initializeSdk];
    [ALInterstitialAd shared].adDisplayDelegate = self;
    [ALInterstitialAd shared].adLoadDelegate = self;
    [ALInterstitialAd shared].adVideoPlaybackDelegate = self;

    if ([ALInterstitialAd isReadyForDisplay]) {
        __weak   VMaxAppLovinRewardedVideoAdapter *adapter = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([adapter.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)] && !adapter.isLoadAdCalled) {
                [adapter.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:adapter];
                adapter.isLoadAdCalled = YES;
            NSLog(@"Success Callback:Applovin:adService:didLoadAd");

            }
            
        });

    }
    
}

-(void)showAd
{
    if([ALInterstitialAd isReadyForDisplay]){
        [ALInterstitialAd show];
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
}

#pragma mark-ALLoad Delegate

- (void) adService: (alnonnull ALAdService *) adService didLoadAd: (alnonnull ALAd *) ad
{
    NSLog(@"Success Callback:Applovin:adService:didLoadAd");

    if ([self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)] && !self.isLoadAdCalled) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
        self.isLoadAdCalled = YES;
    }
}

- (void) adService: (alnonnull ALAdService *) adService didFailToLoadAdWithError: (int) code
{
    NSLog(@"Failure Callback:adService:didFailToLoadAdWithError");

    NSError* err = [NSError errorWithDomain:NSOSStatusErrorDomain code:-1 userInfo:nil];

    if (self.delegate && [self.delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                            withObject: self
                            withObject: err];
    }
    

}

#pragma mark-ALDisplay Delegate

- (void) ad: (alnonnull ALAd *) ad wasDisplayedIn: (alnonnull UIView *) view
{
    NSLog(@"ad:wasDisplayedIn");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:)
                            withObject:self];
    }
}

- (void) ad: (alnonnull ALAd *) ad wasHiddenIn: (alnonnull UIView *) view
{
    NSLog(@"ad:wasHiddenIn");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                            withObject:self];
    }

}

- (void) ad: (alnonnull ALAd *) ad wasClickedIn: (alnonnull UIView *) view
{
    NSLog(@"ad:wasClickedIn");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }

}
#pragma mark-ALVideo Playback Delegate

- (void) videoPlaybackBeganInAd: (alnonnull ALAd *) ad
{
    NSLog(@"videoPlaybackBeganInAd");

}

- (void) videoPlaybackEndedInAd: (alnonnull ALAd *) ad atPlaybackPercent: (alnonnull NSNumber *) percentPlayed fullyWatched: (BOOL) wasFullyWatched
{
    NSLog(@"videoPlaybackEndedInAd");
    if (wasFullyWatched) {
        
            if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]) {
                [self.delegate VMaxCustomAd:self didCompleteRewardVideo:0];
            }
            
    }
}



@end
