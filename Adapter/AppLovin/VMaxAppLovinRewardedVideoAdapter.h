//
//  VMaxAppLovinRewardedVideoAdapter.h
//  VMaxTableViewSample
//
//  Created by Rahul CK on 22/01/16.
//  Copyright © 2016 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxAppLovinRewardedVideoAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;
@property (nonatomic, strong) UIViewController *parentViewController;

@end
