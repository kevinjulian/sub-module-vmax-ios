//
//  VMaxMMediaBannerAdapter.m
//  VMaxTableViewSample
//
//  Created by Tejus Adiga on 25/11/14.
//  Copyright (c) 2014 VMax.com. All rights reserved.
//

#import "VMaxMMediaBannerAdapter.h"

#import "VMaxCustomAd.h"

#import <CoreLocation/CoreLocation.h>
#import <MMAdSDK/MMAdSDK.h>

NSString* const kVMaxCustomAdAdapter_MMediaBannerAppId = @"appid";
NSString* const kVMaxCustomAdAdapter_MMediaBannerAdType = @"adsize";
NSString* const kVMaxBannerAdTypeRectangle = @"rectangle";
NSString* const kVMaxBannerAdTypeBanner = @"bannner";


@interface VMaxMMediaBannerAdapter () <MMInlineDelegate>

@property (strong, nonatomic) MMInlineAd* adView;

@end

@implementation VMaxMMediaBannerAdapter

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    
    self.delegate = inDelegate;
    
    MMInlineAdSize adSize = MMInlineAdSizeBanner;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        adSize = MMInlineAdSizeLeaderboard;
    }
    
    // checking ad type for Rectangle.
    if ([[inParams objectForKey:kVMaxCustomAdAdapter_MMediaBannerAdType]
         isEqualToString:kVMaxBannerAdTypeRectangle]) {
        
        adSize = MMInlineAdSizeMediumRectangle;
    }
    
    NSString *adSizeString = nil;
    
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]) {
        adSizeString = [[inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]objectForKey:kVMaxCustomAdExtras_AdSize];
    }
    
    if (adSizeString) {
        if ([adSizeString isEqualToString:kVMaxCustomAdAdSize_320x50]){adSize = MMInlineAdSizeBanner;}
        
        else if ([adSizeString isEqualToString:kVMaxCustomAdSize_728x90]){adSize = MMInlineAdSizeLeaderboard;}
        
        else if ([adSizeString isEqualToString:kVMaxCustomAdSize_300x250]){adSize = MMInlineAdSizeMediumRectangle;}
        
        
    }
    
    NSString* appId = [inParams objectForKey:kVMaxCustomAdAdapter_MMediaBannerAppId];
    
    if (appId) {
        
        if (!self.adView) {
            
            MMUserSettings* userSettings = [[MMUserSettings alloc] init];
            
            NSString* inGender = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Gender];
            NSString* age = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Age];
            
            userSettings.gender = MMGenderOther;
            
            if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderMale]) {
                userSettings.gender = MMGenderMale;
                
            } else if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderFemale]) {
                userSettings.gender = MMGenderFemale;
            }
            
            userSettings.country = nil;
            userSettings.age = nil;
            
            NSLog(@"VMax USER PARAM: Gender: %@, Age: %@", inGender, age);
            
            MMSDK* sdk = [MMSDK sharedInstance];
            
            sdk.sendLocationIfAvailable = YES;
            
            [sdk initializeWithSettings:nil withUserSettings:userSettings];
            
            
            self.adView = [[MMInlineAd alloc] initWithPlacementId:appId
                                                           adSize:adSize];
            self.adView.delegate = self;
            
            [self.adView request:nil];
        }
    }
}

-(void)showAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
    
    if (self.adView) {
        if (self.adView.view.superview) {
            [self.adView.view removeFromSuperview];
        }
        
        self.adView = nil;
    }
}

-(void)dealloc
{
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}


#pragma mark - MMAdDelegte

/**
 * The view controller over which modal content will be displayed.
 *
 * @return A view controller that is used for presenting modal content.
 */
- (UIViewController *)viewControllerForPresentingModalView
{
    return self.parentViewController;
}


/**
 * Callback indicating that an ad request has succeeded.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement which was successfully requested.
 */
-(void)inlineAdRequestDidSucceed:(MMInlineAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didLoadAdInView:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didLoadAdInView:)
                            withObject:self
                            withObject:ad.view];
        
    }
}

/**
 * Callback indicating that ad content failed to load or render.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement for which the request failed.
 * @param error The error indicating the failure.
 */
-(void)inlineAd:(MMInlineAd*)ad requestDidFailWithError:(NSError*)error
{
    NSLog(@"%@", error);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:)
                            withObject:self
                            withObject:error];
    }
}

/**
 *  Callback indicating that the user has interacted with ad content.
 *
 * This callback should not be used to adjust the contents of your application -- it should
 * be used only for the purposes of reporting.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement which was tapped.
 */
-(void)inlineAdContentTapped:(MMInlineAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }
}

/**
 * Callback indicating that the ad is preparing to be resized.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 * @param frame The size and location of the ad placement.
 * @param isClosingResize This flag indicates the resize close button was tapped, causing a resize to the default/original size.
 */
-(void)inlineAd:(MMInlineAd*)ad willResizeTo:(CGRect)frame isClosing:(BOOL)isClosingResize
{
    
}

/**
 * Callback indicating the ad has finished resizing.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 * @param frame The size and location of the ad placement.
 * @param isClosingResize This flag indicates the resize close button was tapped, causing a resize to the default/original size.
 */
-(void)inlineAd:(MMInlineAd*)ad didResizeTo:(CGRect)frame isClosing:(BOOL)isClosingResize
{
    
}

/**
 * Callback indicating that the ad is preparing to present a modal view.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 */
-(void)inlineAdWillPresentModal:(MMInlineAd *)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillPresentOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillPresentOverlay:)
                            withObject:self];
    }
}

/**
 * Callback indicating that the ad has presented a modal view.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 */
-(void)inlineAdDidPresentModal:(MMInlineAd *)ad
{
    
}

/**
 * Callback indicating that the ad is preparing to dismiss a modal view.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 */
-(void)inlineAdWillCloseModal:(MMInlineAd *)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:)
                            withObject:self];
    }
}

/**
 * Callback indicating that the ad has dismissed a modal view.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 */
-(void)inlineAdDidCloseModal:(MMInlineAd *)ad
{
    
}

/**
 * Callback invoked prior to the application going into the background due to a user interaction with an ad.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 */
-(void)inlineAdWillLeaveApplication:(MMInlineAd *)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:)
                            withObject:self];
    }
}

@end

