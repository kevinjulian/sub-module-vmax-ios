//
//  VMaxMMediaBannerAdapter.h
//  VMaxTableViewSample
//
//  Created by Tejus Adiga on 25/11/14.
//  Copyright (c) 2014 VMax.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMaxCustomAd.h"

@interface VMaxMMediaBannerAdapter : NSObject <VMaxCustomAd>

@property (nonatomic, weak) id<VMaxCustomAdListener> delegate;

@property (nonatomic, strong) UIViewController* parentViewController;

@end

