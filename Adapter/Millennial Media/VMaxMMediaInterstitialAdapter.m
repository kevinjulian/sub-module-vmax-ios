//
//  VMaxMMediaInterstitialAdapter.m
//  VMaxTableViewSample
//
//  Created by Tejus Adiga on 25/11/14.
//  Copyright (c) 2014 VMax.com. All rights reserved.
//

#import "VMaxMMediaInterstitialAdapter.h"

#import "VMaxCustomAd.h"

#import <CoreLocation/CoreLocation.h>
#import <MMAdSDK/MMAdSDK.h>

NSString* const kVMaxCustomAdAdapter_MMediaInterstitialAppId = @"appid";

@interface VMaxMMediaInterstitialAdapter () <MMInterstitialDelegate>
{
    BOOL isAdShown;
}

@property (nonatomic, strong) MMInterstitialAd* theAd;
@end

@implementation VMaxMMediaInterstitialAdapter

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    
    self.delegate = inDelegate;
    
    NSString* appId = [inParams objectForKey:kVMaxCustomAdAdapter_MMediaInterstitialAppId];
    
    isAdShown = NO;
    
    if (appId) {
        
        MMUserSettings* userSettings = [[MMUserSettings alloc] init];
        
        NSString* inGender = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Gender];
        NSString* age = [inExtraInfo objectForKey:kVMaxCustomAdExtras_Age];
        
        userSettings.gender = MMGenderOther;
        
        if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderMale]) {
            userSettings.gender = MMGenderMale;
            
        } else if ([inGender isEqualToString: kVMaxCustomAdExtras_GenderFemale]) {
            userSettings.gender = MMGenderFemale;
        }
        
        userSettings.country = nil;
        userSettings.age = nil;
        
        NSLog(@"VMax USER PARAM: Gender: %@, Age: %@", inGender, age);
        
        //Initialising SDK with user settings
        MMSDK* sdk = [MMSDK sharedInstance];
        sdk.sendLocationIfAvailable = YES;
        [sdk initializeWithSettings:nil withUserSettings:userSettings];
        
        self.theAd = [[MMInterstitialAd alloc] initWithPlacementId:appId];
        self.theAd.delegate = self;
        
        if (!self.theAd.ready || self.theAd.expired) {
            [self.theAd load:nil];
            
        } else {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
                [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                                    withObject:self];
            }
        }
    }
}

-(void)showAd
{
    if (self.theAd.ready && !self.theAd.expired) {
        isAdShown = YES;
        [self.theAd showFromViewController:self.parentViewController];
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
    
    self.theAd.delegate = nil;
    
    self.theAd = nil;
}

//-(void)dealloc
//{
//    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
//}

/**
 * Callback fired when an ad load (request and content processing) succeeds.
 *
 * This method is always invoked on the main thread.
 *
 * @param ad The ad placement which was successfully requested.
 */
-(void)interstitialAdLoadDidSucceed:(MMInterstitialAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:)
                            withObject:self];
    }
}

/**
 * Callback fired when an ad load fails. The failure can be caused by failure to either retrieve or parse
 * ad content.
 *
 * This method is always invoked on the main thread.
 *
 * @param ad The ad placement for which the request failed.
 * @param error The error indicating the failure.
 */
-(void)interstitialAd:(MMInterstitialAd*)ad loadDidFailWithError:(NSError*)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:)
                            withObject:self
                            withObject:error];
    }
}

/**
 *  Callback fired when an interstitial will be displayed, but before the display action begins.
 *  Note that the ad could still fail to display at this point.
 *
 * This method is always called on the main thread.
 *
 *  @param ad The interstitial which will display.
 */
-(void)interstitialAdWillDisplay:(MMInterstitialAd*)ad
{
}

/**
 * Callback fired when the interstitial is displayed.
 *
 * This method is always called on the main thread.
 *
 * @param ad The interstitial which is displayed.
 */
-(void)interstitialAdDidDisplay:(MMInterstitialAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

/**
 * Callback fired when an attempt to show the interstitial fails.
 *
 * This method is always called on the main thread.
 *
 * @param ad The interstitial which failed to show.
 * @param error The error indicating the failure.
 */
-(void)interstitialAd:(MMInterstitialAd*)ad showDidFailWithError:(NSError*)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:)
                            withObject:self
                            withObject:error];
    }
}

/**
 * Callback fired when an interstitial will be dismissed, but before the dismiss action begins.
 *
 * This method is always called on the main thread.
 *
 *  @param ad The interstitial which will be dismissed.
 */
-(void)interstitialAdWillDismiss:(MMInterstitialAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillDismissOverlay:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillDismissOverlay:)
                            withObject:self];
    }
}

/**
 * Callback fired when the interstitial is dismissed.
 *
 * This method is always called on the main thread.
 *
 * @param ad The interstitial which was dismissed.
 */
-(void)interstitialAdDidDismiss:(MMInterstitialAd*)ad
{
    
}

/**
 * Callback fired when the ad expires.
 *
 * After receiving this message, your app should call -load before attempting to display the interstitial.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement which expired.
 */
-(void)interstitialAdDidExpire:(MMInterstitialAd*)ad
{
    if (!isAdShown) {
        [ad load:nil];
    }
}

/**
 * Callback fired when the ad is tapped.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement which was tapped.
 */
-(void)interstitialAdTapped:(MMInterstitialAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:)
                            withObject:self];
    }
}

/**
 * Callback invoked prior to the application going into the background due to a user interaction with an ad.
 *
 * This method is always called on the main thread.
 *
 * @param ad The ad placement.
 */
-(void)interstitialAdWillLeaveApplication:(MMInterstitialAd*)ad
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdWillLeaveApplication:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdWillLeaveApplication:)
                            withObject:self];
    }
}

@end
