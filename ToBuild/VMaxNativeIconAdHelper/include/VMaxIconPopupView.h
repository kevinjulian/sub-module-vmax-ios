//
//  VMaxIconPopupView.h
//  VMaxNativeAdHelperForIcon
//
//  Created by Anup Dsouza on 13/04/16.
//  Copyright © 2016 VMax.Com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VMaxIconPopupViewDelegate <NSObject>
@required
- (void)popupDidShow;  // notify delegate for impression
- (void)popupDidClose; // notify delegate for removal
- (void)popupWillEnterFullscreen; // notify delegate for VAST fullscreen ON
- (void)popupWillExitFullscreen; // notify delegate for VAST fullscreen OFF
@end

@class VMaxNativeAd;

@interface VMaxIconPopupView : UIViewController

@property (nonatomic, weak) VMaxNativeAd *nativeAd;
@property (nonatomic, weak) id<VMaxIconPopupViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UILabel *titleTextView;
@property (nonatomic, weak) IBOutlet UIImageView *coverImageView;
@property (nonatomic, weak) IBOutlet UIView *otherMediaView;
@property (nonatomic, weak) IBOutlet UIButton *actionButton;
@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UILabel *adIndicatorLabel;
@property (nonatomic, weak) IBOutlet UIButton *adCloseButton;
@property (nonatomic, strong) IBOutlet UIView *adView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adViewWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adIndicatorLabelTrailingConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adIndicatorLabelTopConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adCoverImageViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adCoverImageViewWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adCoverImageViewPadTopConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adCoverImageViewPadLeftConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adOtherMediaViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adOtherMediaViewWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adOtherMediaViewPadTopConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *adOtherMediaViewPadLeftConstraint;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *iconImageViewPadLeftConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *iconImageViewPadTopConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleTextViewPadLeftConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleTextViewPadTopConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleTextViewWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleTextViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *actionButtonWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *actionButtonHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *actionButtonPadLeftConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *closeButtonHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *closeButtonWidthConstraint;

// Customisable
@property (nonatomic, strong) UIColor *adViewBGColor;
@property (nonatomic, strong) UIColor *ctaBtnBGColor;

- (instancetype)initWithFrame:(CGRect)frame withNativeAd:(VMaxNativeAd*)aNativeAd;
- (void)resumeMediaViewPlaybackIfNeeded;
@end