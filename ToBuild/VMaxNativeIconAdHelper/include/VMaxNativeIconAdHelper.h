//
//  VMaxNativeIconAdHelper.h
//  VMaxNativeAdHelperForIcon
//
//  Created by Anup Dsouza on 13/04/16.
//  Copyright © 2016 VMax.Com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxNativeIconAdView.h"

@interface VMaxNativeIconAdHelper : NSObject

/*!
 @function  sharedNativeAdHelper
 @abstract  This method will return object of VMaxNativeIconAdHelper
 */
+ (instancetype)sharedNativeAdHelper;

/*!
 @function  createNativeAdView:forViewController
 @abstract  This method will create nativeAdView with nativead object
 @param     nativeAd, NativeAd Object
 @param     adViewController, ViewController Object
 */
- (VMaxNativeIconAdView*)createNativeAdView:(VMaxNativeAd*)nativeAd forViewController:(UIViewController*)adViewController;

@end
