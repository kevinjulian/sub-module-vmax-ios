//
//  VMaxNativeIconAdView.h
//  VMaxNativeAdHelperForIcon
//
//  Created by Anup Dsouza on 13/04/16.
//  Copyright © 2016 VMax.Com. All rights reserved.
//

#import "VMaxNativeAd.h"
#import <UIKit/UIKit.h>

@class VMaxNativeIconAdView;
@protocol VMaxNativeIconAdViewDelegate <NSObject>

/*!
 @function  didRenderNativeIconAd
 @abstract  This method will be called by SDK when the icon is rendered
 */
- (void)didRenderNativeIconAd;
/*!
 @function  didFailToRenderNativeIconAdWithError
 @abstract  This method will be called by SDK when failed to load with error
 */
- (void)didFailToRenderNativeIconAdWithError:(NSError*)error;
/*!
 @function  adViewDidExpand
 @abstract  This method will be called by SDK when the popup is displayed
 */
- (void)adViewDidExpand:(VMaxNativeIconAdView *)theAdView;
/*!
 @function  adViewDidCollapse
 @abstract  This method will be called by SDK when the popup is dismissed
 */
- (void)adViewDidCollapse:(VMaxNativeIconAdView *)theAdView;
@end

typedef NS_ENUM (NSInteger, IconLayoutType) {
    kIconLayoutTypeIcon80x80
};

@interface VMaxNativeIconAdView : UIView

@property (nonatomic, strong) VMaxNativeAd *nativeAd;
@property (nonatomic, weak) UIViewController *nativeAdViewController;
@property (nonatomic, weak) id<VMaxNativeIconAdViewDelegate> delegate;
@property (nonatomic, strong) UIColor *adViewBGColor;
@property (nonatomic, strong) UIColor *ctaBtnBGColor;

/*!
 @function  iconViewWithNativeAd
 @abstract  This method create object of VMaxNativeIconAdView
 */
+ (VMaxNativeIconAdView*)iconViewWithNativeAd:(VMaxNativeAd*)nativeAd;

@end