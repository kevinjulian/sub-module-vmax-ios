//
//  VMaxAdDelegate.h
//  VMaxAdSDK
//
//  Copyright (c) 2014 VMax.com. All rights reserved.
//
#import "VMaxWalletElement.h"

/*!
 @enum      VMaxAdRequestError
 @abstract  Error codes definition. This is sent by SDK in adView:didFailedToLoadAd: callback in [NSError code] field.
 @field     kVMaxAdRequestError_ConnectionFailed, Connection to the Ad Server failed.
 @field     kVMaxAdRequestError_NoFill, There is no ad content received in the responce from the ad server.
 @field     kVMaxAdRequestError_LoadingFailed, Could not load the ad received from ad server.
 @field     kVMaxAdError_InvalidResponce, Requested Ad Type and Received Ad Type does not match.
 */
typedef NS_ENUM(NSInteger, VMaxAdError) {
    kVMaxAdError_ConnectionFailed = -100,
    kVMaxAdError_NoFill,
    kVMaxAdError_LoadingFailed,
    kVMaxAdError_InvalidResponce
};

@class VMaxAdView;

/*!
 @protocol      VMaxAdDelegate
 @abstract      This interface declares the delegate methods that will be called by VMaxAdView class on events.
 */
@protocol VMaxAdDelegate <VMaxWalletElementDelegate>
@optional

/*!
 @function  adViewDidCacheAd:
 @abstract  This method will be called by SDK when the ad is cached and cacheAdWithorientation: in the background. The ad is not displayed yet. To display the ad call showAd method.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)adViewDidCacheAd:(VMaxAdView *)theAdView;

/*!
 @function  adViewDidLoadAd:
 @abstract  This method will be called by SDK when the ad is loaded to the ad spot and is visible to the user.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)adViewDidLoadAd:(VMaxAdView *)theAdView;

/*!
 @function  adView: adViewDidFailedToLoadAd:
 @abstract  This method will be called by SDK when the adview failed to load the ad.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 @param     error, NSError. Description of error occured.
 */
- (void)adView:(VMaxAdView *)theAdView didFailedToLoadAd:(NSError *)error;
/*!
 @function  didInteractWithAd:
 @abstract  This method will be called when used interacts with the ad.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)didInteractWithAd:(VMaxAdView *)theAdView;

/*!
 @function  willPresentOverlay:
 @abstract  This method will be called when the SDK is about to present the overlay on top of the user view.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)willPresentAd:(VMaxAdView *)theAdView;

/*!
 @function  willDismissOverlay:
 @abstract  This method will be called when the SDK is about to dismis the Overlay put by the SDK.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)willDismissAd:(VMaxAdView *)theAdView;

/*!
 @function  willLeaveApp:
 @abstract  This method will be called when the SDK is taking the app to background due to action of the ad. For instance when clicking on ad will launch Safari which caused this app to go to background and safari to come front.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)willLeaveApp:(VMaxAdView *)theAdView;
/*!
 @function  adViewDidExpand:
 @abstract  This method will be called when the adView expanded.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
-(void)adViewDidExpand:(VMaxAdView *)theAdView;
/*!
 @function  willDismissOverlay:
 @abstract  This method will be called when the adview is collapsed
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
-(void)adViewDidCollapse:(VMaxAdView *)theAdView;

@end