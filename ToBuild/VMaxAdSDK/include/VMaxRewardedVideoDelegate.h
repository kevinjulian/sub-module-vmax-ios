//
//  VMaxRewardedVideoDelegate.h
//  VMaxAdSDK
//
//  Created by Rahul CK on 08/01/16.
//  Copyright © 2016 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>

@class VMaxAdView;

@protocol VMaxRewardedVideoDelegate <NSObject>

/*!
 @function  adView:didInterruptRewardVideo:
 @abstract  This method will be called when RewardedVideo is interrupted by the user
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 @param     message, Message to user
 
 */
- (void)adView:(VMaxAdView *)theAdView didInterruptRewardVideo:(NSString *)message;
/*!
 @function  adView:didFailedToPlaybackRewardVideo:
 @abstract  This method will be called when RewardVideo Failed to Playback with Error
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 @param     error, Error
 
 */
- (void)adView:(VMaxAdView *)theAdView didFailedToPlaybackRewardVideo:(NSError*)error;
/*!
 @function  theAdView:didCompleteRewardVideo:
 @abstract  This method will be called when Reward Video completed play back
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 @param     rewardAmount ,The Reward Amonut
 */
- (void)adView:(VMaxAdView *)theAdView didCompleteRewardVideo:(NSInteger)rewardAmount;

/*!
 @function  willShowRewardedVideoPrepopUp
 @abstract  This method will be called when before showing Prepopup
 */
-(void) willShowRewardedVideoPrepopUp;
/*!
 @function  willShowRewardedVideoPostpopUp
 @abstract  This method will be called when before showing Postpopup
 */
-(void) willShowRewardedVideoPostpopUp;
/*!
 @function  willShowRewardedVideoNoFillpopUp
 @abstract  This method will be called when before showing FillpopUp
 */
-(void) willShowRewardedVideoNoFillpopUp;
/*!
 @function  willShowRewardedVideoImpressionCappopUpUp
 @abstract  This method will be called when before showing ImpressionCappopUp
 */
-(void) willShowRewardedVideoImpressionCappopUp;
/*!
 @function  willShowRewardedVideoAdInterruptedpopUp
 @abstract  This method will be called when before showing InterruptedpopUp
 */
-(void) willShowRewardedVideoAdInterruptedpopUp;

@end
