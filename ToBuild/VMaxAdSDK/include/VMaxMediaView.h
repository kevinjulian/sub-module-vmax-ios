//
//  VMaxMediaView.h
//  VMaxAdSDK
//
//  Created by Rahul CK on 24/05/16.
//  Copyright © 2016 Vserv.mobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMaxMediaView;
@class VMaxNativeAd;

@protocol VMaxMediaViewDelegate <NSObject>

/*!
 @function  mediaViewDidLoad:
 @abstract  This method will be called when the mediaview loaded successfully.
 @param     mediaView, VMaxMediaView. The mediaview view which sent this event.
 */
- (void)mediaViewDidLoad:(VMaxMediaView *)mediaView;
/*!
 @function  mediaViewFailedToLoad:
 @abstract  This method will be called when the mediaview is failed to load.
 @param     mediaView, VMaxMediaView. The mediaview view which sent this event.
 */
- (void)mediaViewFailedToLoad:(VMaxMediaView *)mediaView;
/*!
 @function  mediaViewDidClick:
 @abstract  This method will be called when the mediaview is clicked.
 @param     mediaView, VMaxMediaView. The mediaview view which sent this event.
 */
- (void)mediaViewDidClick:(VMaxMediaView *)mediaView;


/*!
 @function  mediaViewDidExpand:
 @abstract  This method will be called when the mediaview expanded.
 @param     mediaView, VMaxMediaView. The mediaview view which sent this event.
 */
-(void)mediaViewDidExpand:(VMaxMediaView *)mediaView;
/*!
 @function  mediaViewDidCollapse:
 @abstract  This method will be called when the mediaview is collapsed
 @param     mediaView, VMaxMediaView. The mediaview view which sent this event.
 */
-(void)mediaViewDidCollapse:(VMaxMediaView *)mediaView;

@end

@interface VMaxMediaView : UIView
/*!
 @property delegate
 @abstract delegate for mediaview
 */
@property (nonatomic,weak) id<VMaxMediaViewDelegate> delegate;

/*!
 @property nativeAd
 @abstract native ad object
 */
@property (strong,nonatomic) VMaxNativeAd *nativeAd;
/*!
 @property controller
 @abstract controller for presenting the mediaview in  full screen
 */
@property (weak,nonatomic) UIViewController *controller;
/*!
 @function  initWithVMaxNativeAd:
 @abstract  initialise with native ad.
 @param     nativeAd, VMaxNativeAd.The native ad object.
 */
- (instancetype)initWithVMaxNativeAd:(VMaxNativeAd *)nativeAd;
@end
