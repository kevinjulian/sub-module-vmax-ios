//
//  VMaxRewardVideo.h
//  VMaxAdSDK
//
//  Created by Rahul CK on 24/11/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxRewardedVideoDelegate.h"


@class VMaxWalletElement,UIImage;

@interface VMaxRewardedVideo : NSObject

@property (nonatomic,weak) id<VMaxRewardedVideoDelegate> delegate;
@property (nonatomic,assign) BOOL shouldShowMaxImpressionCapPopup;
@property (nonatomic,assign) BOOL shouldShowPrePopup;
@property (nonatomic,assign) BOOL shouldShowPostPopup;
@property (nonatomic,assign) BOOL shouldShowInterruptPopup;
@property (nonatomic,assign) BOOL shouldShowNoFillPopup;

@property (strong,nonatomic,readonly) NSString *maxImpressionCapPopUpTitle;
@property (strong,nonatomic,readonly) NSString *maxImpressionCapPopUpMessage;

@property (strong,nonatomic,readonly) NSString *prePopUpTitle;
@property (strong,nonatomic,readonly) NSString *prePopUpMessage;

@property (strong,nonatomic,readonly) NSString *postPopUpTitle;
@property (strong,nonatomic,readonly) NSString *postPopUpMessage;

@property (strong,nonatomic,readonly) NSString *interruptPopUpTitle;
@property (strong,nonatomic,readonly) NSString *interruptPopUpMessage;

@property (strong,nonatomic,readonly) NSString *noFillPopUpTitle;
@property (strong,nonatomic,readonly) NSString *noFillPopUpMessage;

@property (strong,nonatomic,readonly) NSString *currency;
@property (strong,nonatomic,readonly) UIImage *thumbnail;



-(instancetype)initWithWalletElement:(VMaxWalletElement*) walletElement;

-(void) setMaxImpressionCapPopUpWithTitle:(NSString*)title message:(NSString*)message;

-(void) setPrePopUpWithTitle:(NSString*)title message:(NSString*)message;

-(void) setPostPopUpWithTitle:(NSString*)title message:(NSString*)message;

-(void) setInterruptPopUpWithTitle:(NSString*)title message:(NSString*)message;

-(void) setNoFillPopUpWithTitle:(NSString*)title message:(NSString*)message;





@end
