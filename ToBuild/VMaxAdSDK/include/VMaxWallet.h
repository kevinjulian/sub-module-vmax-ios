//
//  VMaxWallet.h
//  VMaxAdSDK
//
//  Created by Rahul CK on 24/11/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxWalletElement.h"

@interface VMaxWallet : NSObject<NSCoding>

@property (strong,nonatomic,readonly) NSMutableArray *walletElements;

+(instancetype) sharedVMaxWallet;

-(VMaxWalletElement*) walletElementWithCurrency:(NSString*) currency;

-(NSArray*) allWalletElemets;
-(void) synchronizeWallet;

@end
