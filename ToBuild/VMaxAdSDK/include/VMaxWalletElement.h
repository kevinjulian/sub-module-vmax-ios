//
//  VMaxWalletElement.h
//  VMaxAdSDK
//
//  Created by Rahul CK on 24/11/15.
//  Copyright © 2015 VMax.com  All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMaxWalletElement;

typedef void(^VMaxWalletUpdateCompletion)(BOOL isSuccess ,NSInteger currentBalance);

@protocol VMaxWalletElementDelegate <NSObject>

@optional

-(void) didUpdateWalletElement:(VMaxWalletElement*)walletElement withCurrencyBalance:(NSInteger)currency;

-(void) didFailtoUpdateWalletElement:(VMaxWalletElement*)walletElement withError:(NSError*)error;

@end

@interface VMaxWalletElement : NSObject<NSCoding>

@property (weak,nonatomic) id<VMaxWalletElementDelegate> delegate;

@property (strong,nonatomic) NSString* currency;

@property (strong,nonatomic) UIImage* currencyImage;

-(instancetype) initWithCurrency:(NSString*)currency;

-(NSInteger) getTotalCurrency;

-(void) spendVirtualCurrency:(NSInteger)currencyValue;
-(void) spendVirtualCurrency:(NSInteger)currencyValue withCompletionBlock:(VMaxWalletUpdateCompletion)completionBlock;

-(void) awardVirtualCurrency:(NSInteger)currencyValue;
-(void) awardVirtualCurrency:(NSInteger)currencyValue withCompletionBlock:(VMaxWalletUpdateCompletion)completionBlock;



@end
