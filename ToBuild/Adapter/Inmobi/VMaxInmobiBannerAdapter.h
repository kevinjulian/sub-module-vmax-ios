//
//  VMaxInmobiBannerAdapter.h
//  VMaxSample
//
//  Created by Anup Dsouza on 01/06/16.
//  Copyright © 2016 VMax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaxCustomAd.h"
#import "VMaxCustomAdListener.h"

@interface VMaxInmobiBannerAdapter : NSObject<VMaxCustomAd>

@property (nonatomic, weak) id <VMaxCustomAdListener> delegate;
@property (nonatomic, strong) UIViewController* parentViewController;

@end