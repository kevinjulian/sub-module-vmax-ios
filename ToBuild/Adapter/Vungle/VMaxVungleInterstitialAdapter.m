//
//  VungleInterstetialAdapter.m
//  VingleSample
//
//  Created by Tejus Adiga on 27/10/14.
//  Copyright (c) 2014 Robosoft. All rights reserved.
//

#import "VMaxVungleInterstitialAdapter.h"
#import <VungleSDK/VungleSDK.h>

const NSString* kVungleParameter_AppID = @"appid";
static BOOL isLoadAdCalled;

__weak static id<VMaxCustomAdListener> delegate;

@interface VMaxVungleInterstitialAdapter () <VungleSDKDelegate>
{
    BOOL  didShowAdCalled;
    
    BOOL isAwarded;
}

@property (weak, nonatomic) VungleSDK* sdk;

@property (strong, nonatomic) NSString* appId;

@end


@implementation VMaxVungleInterstitialAdapter

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    delegate = inDelegate;

    isLoadAdCalled=NO;
    isAwarded = NO;
    didShowAdCalled = NO;
    
    NSString* appID = nil;
    
    if ([inParams objectForKey: kVungleParameter_AppID]) {
        appID = [inParams objectForKey: kVungleParameter_AppID];
    }
    
    if (appID) {
        
        self.sdk = [VungleSDK sharedSDK];
        
        self.sdk.delegate = self;
        
        [self.sdk startWithAppId: appID];
        
        [self.sdk setLoggingEnabled:YES];
        
        if ([[VungleSDK sharedSDK] isAdPlayable]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
                [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
            }
                isLoadAdCalled = YES;
            });
        }
    }
}

-(void)showAd
{
    NSError *error;
    if ([[VungleSDK sharedSDK] isAdPlayable]) {
        [[VungleSDK sharedSDK] playAd:self.parentViewController error:&error];
    }
    if (error) {
        
    }
}

-(void)invalidateAd
{
    
    [[VungleSDK sharedSDK] setLoggingEnabled:NO];
}

-(void)dealloc
{
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}

#pragma mark - VingleSDKDelegate methods

/**
 * if implemented, this will get called when the SDK is about to show an ad. This point
 * might be a good time to pause your game, and turn off any sound you might be playing.
 */
- (void)vungleSDKwillShowAd
{
    NSLog(@"VungleSDK Will Show Ad");
    if (!isLoadAdCalled) {
        if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
            [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
            isLoadAdCalled = YES;
            
        }
    }
    if (!didShowAdCalled) {
        
        
        if ([delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
            [delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
        }
        didShowAdCalled = YES;
    }
    
}

/**
 * if implemented, this will get called when the SDK closes the ad view, but there might be
 * a product sheet that will be presented. This point might be a good place to resume your game
 * if there's no product sheet being presented. The viewInfo dictionary will contain the
 * following keys:
 * - "completedView": NSNumber representing a BOOL whether or not the video can be considered a
 *               full view.
 * - "playTime": NSNumber representing the time in seconds that the user watched the video.
 * - "didDownlaod": NSNumber representing a BOOL whether or not the user clicked the download
 *                  button.
 */
- (void)vungleSDKwillCloseAdWithViewInfo:(NSDictionary*)viewInfo willPresentProductSheet:(BOOL)willPresentProductSheet
{
    if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }

    NSLog(@"VungleSDK vungleSDKwillCloseAdWithViewInfo: willPresentProductSheet: %@", viewInfo);
    double playTime,videoLength;
    playTime = [[viewInfo objectForKey:@"playTime"]doubleValue];
    videoLength = [[viewInfo objectForKey:@"videoLength"]doubleValue];
    if (playTime ==videoLength) {
        if (!isAwarded) {
            
            if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]) {
                [delegate VMaxCustomAd:self didCompleteRewardVideo:0];
            }
            isAwarded = YES;
        }
    }
}

/**
 * if implemented, this will get called when the product sheet is about to be closed.
 */
- (void)vungleSDKwillCloseProductSheet:(id)productSheet
{
    NSLog(@"VungleSDK Will close ProductSheet");
}

/**
 * if implemented, this will get called when there is an ad cached and ready to be shown.
 */
- (void)vungleSDKhasCachedAdAvailable
{
}
- (void)vungleSDKAdPlayableChanged:(BOOL)isAdPlayable
{
    if (isAdPlayable) {
        NSLog(@"vungleSDKAdPlayableChanged");
        if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]&&!isLoadAdCalled) {
            [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
            isLoadAdCalled=YES;
            
        }
    }
    else
    {
        if (!isLoadAdCalled) {
            
        NSError *error = [[NSError alloc]initWithDomain:NSOSStatusErrorDomain code:-1 userInfo:@{@"Error:":@"Vungle Ad not available"}];
        
        NSLog(@"vungleSDKAdPlayableAd not avalible");
        if (delegate && [delegate respondsToSelector: @selector(VMaxCustomAd:didFailWithError:)]) {
            [delegate performSelector: @selector(VMaxCustomAd:didFailWithError:)
                                withObject: self
                                withObject: error];
        }
        }
        
    }
}


@end
