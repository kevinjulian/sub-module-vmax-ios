//
//  AdColonyInterstetialAdapter.m
//  VMaxAdapterTest
//
//  Created by Tejus Adiga on 05/11/14.
//  Copyright (c) 2014 VMax. All rights reserved.
//

#import "VMaxAdColonyInterstitialAdapter.h"
#import <AdColony/AdColony.h>

NSString* const kAdColonyAdTypeIterstitial = @"interstitial";

NSString* const kAdColonyAdTypeV4VC        = @"v4vc";
static BOOL shouldRewarded;
__weak static id<VMaxCustomAdListener> delegate;



@interface VMaxAdColonyInterstitialAdapter () <AdColonyAdDelegate, AdColonyDelegate>
{
    BOOL shouldShowAd;
    NSString* zoneId;
     BOOL didCalledFailedCallback;

    
}
@property(strong,nonatomic)NSString* adType;
@end

@implementation VMaxAdColonyInterstitialAdapter

-(id)init
{
    if (self = [super init]) {
        shouldShowAd = YES;
    }
    return self;
}

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
   
    delegate = inDelegate;
    shouldRewarded = YES;
    didCalledFailedCallback = NO;
    
    NSString* adColonyAppId = nil;
    id adColonyZoneID = nil;

    if ([inParams objectForKey: @"appid"]) {
        adColonyAppId = [inParams objectForKey: @"appid"];
    }
    
    if ([inParams objectForKey: @"zoneid"]) {
        adColonyZoneID = [inParams objectForKey: @"zoneid"];
    }
    
    self.adType = kAdColonyAdTypeIterstitial;
    
    if ([inParams objectForKey:@"adtype"]) {
        self.adType = [inParams objectForKey:@"adtype"];
    }
    
    if (![adColonyZoneID isKindOfClass:[NSArray class]]) {
        zoneId = adColonyZoneID;
        
    }
    if ([AdColony zoneStatusForZone:zoneId] == ADCOLONY_ZONE_STATUS_ACTIVE) {
      __weak  VMaxAdColonyInterstitialAdapter *adapter = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if ([delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
                [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:adapter];
            }
            NSLog(@"Success Callback:AdColony");

            
        });
        NSLog(@"Ad Colony CacheAd Available");
    } else {
    
        [AdColony configureWithAppID: adColonyAppId
                             zoneIDs: @[zoneId]
                            delegate: self
                             logging: YES];
    }
}

-(void)showAd
{
    // Interstitial Ad.
    if ([self.adType isEqualToString:kAdColonyAdTypeIterstitial]) {

        [AdColony playVideoAdForZone:zoneId
                        withDelegate:self];
    } else {
        
        // V4VC Ad.
        [AdColony playVideoAdForZone:zoneId
                        withDelegate:self
                    withV4VCPrePopup:NO
                    andV4VCPostPopup:NO];
    }

}

-(void)invalidateAd
{

    shouldShowAd = NO;
}

-(void)dealloc
{
    [AdColony cancelAd];
}

#pragma mark - AdColonyDelegate methods

/**
 * Provides your app with real-time updates about ad availability changes.
 * This method is called when a zone's ad availability state changes (when ads become available, or become unavailable).
 * Listening to these callbacks allows your app to update its user interface immediately.
 * For example, when ads become available in a zone you could immediately show an ad for that zone.
 * @param available Whether ads became available or unavailable
 * @param zoneID The affected zone
 */
- ( void )onAdColonyAdAvailabilityChange:(BOOL)available inZone:(NSString*) zoneID
{
    NSLog(@"onAdColonyAdAvailabilityChange:%@",(available)?@"YES":@"NO");

    // Ad awailable.
    if (available) {
        if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
            [delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
        }
        NSLog(@"Success Callback:onAdColonyAdAvailabilityChange");

        
        
    // Ad Not Awailable. Report Error.
    } else {
        if (!didCalledFailedCallback) {
            if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
                [delegate performSelector:@selector(VMaxCustomAd:didFailWithError:)
                                    withObject:self withObject:[NSError errorWithDomain:NSOSStatusErrorDomain
                                                                                   code:-1
                                                                               userInfo:nil]];
                didCalledFailedCallback = YES;

            }
            
        }
        
    }
}


/** @name Virtual Currency Rewards (V4VC) */

/**
 * Notifies your app when a virtual currency transaction has completed as a result of displaying an ad.
 * In your implementation, check for success and implement any app-specific code that should be run when
 * AdColony has successfully rewarded. Client-side V4VC implementations should increment the user's currency
 * balance in this method. Server-side V4VC implementations should contact the game server to determine
 * the current total balance for the virtual currency.
 * @param success Whether the transaction succeeded or failed
 * @param currencyName The name of currency to reward
 * @param amount The amount of currency to reward
 * @param zoneID The affected zone
 */
- ( void ) onAdColonyV4VCReward:(BOOL)success currencyName:(NSString*)currencyName currencyAmount:(int)amount inZone:(NSString*)zoneID
{
    NSLog(@"onAdColonyV4VCReward");

    // Implement your Reward logic here.
    if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]&&shouldRewarded) {
        [delegate VMaxCustomAd:self didCompleteRewardVideo:amount];
        shouldRewarded = NO;
    }


}

#pragma mark - AdColonyAdDelegate methods

/**
 * Notifies your app that an ad will actually play in response to the app's request to play an ad.
 * This method is called when AdColony has taken control of the device screen and is about to begin
 * showing an ad. Apps should implement app-specific code such as pausing a game and turning off app music.
 * @param zoneID The affected zone
 */
- ( void ) onAdColonyAdStartedInZone:( NSString * )zoneID
{
    NSLog(@"onAdColonyAdStartedInZone");


    if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

/**
 * Notifies your app that an ad completed playing (or never played) and control has been returned to the app.
 * This method is called when AdColony has finished trying to show an ad, either successfully or unsuccessfully.
 * If an ad was shown, apps should implement app-specific code such as unpausing a game and restarting app music.
 * @param shown Whether an ad was actually shown
 * @param zoneID The affected zone
 */
- ( void ) onAdColonyAdAttemptFinished:(BOOL)shown inZone:( NSString * )zoneID
{
    NSLog(@"onAdColonyAdAttemptFinished:%@",(shown)?@"YES":@"NO");
    if (shown) {
        if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
            [delegate performSelector:@selector(VMaxCustomAdDidDismissAd:)
                                withObject:self];
        }
        if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAd:didCompleteRewardVideo:)]&&shouldRewarded) {
            [delegate VMaxCustomAd:self didCompleteRewardVideo:0];
            shouldRewarded = NO;
        }
    // Ad Not Awailable. Report Error.
    } else {
        if (!didCalledFailedCallback) {
            if (delegate && [delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
                [delegate performSelector:@selector(VMaxCustomAd:didFailWithError:)
                                    withObject:self withObject:[NSError errorWithDomain:NSOSStatusErrorDomain
                                                                                   code:-1
                                                                               userInfo:nil]];
            }
            didCalledFailedCallback = YES;
        }
        
        if (delegate && [delegate respondsToSelector: @selector(VMaxCustomAd:didFailedToPlaybackRewardVideo:)]) {
            [delegate performSelector: @selector(VMaxCustomAd:didFailedToPlaybackRewardVideo:)
                                withObject: self
                                withObject: [NSError errorWithDomain:NSOSStatusErrorDomain
                                                                code:-1
                                                            userInfo:nil]];
        }
    }
}

@end


