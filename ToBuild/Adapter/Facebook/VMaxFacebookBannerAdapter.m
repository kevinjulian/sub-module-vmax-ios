//
//  FacebookBannerAdapter.m
//  VMaxClientSideMediationSample
//
//  Copyright (c) 2014 VMax.com All rights reserved.
//

#import <FBAudienceNetwork/FBAudienceNetwork.h>

#import "VMaxFacebookBannerAdapter.h"

NSString* const kVMaxCustomAdAdapter_FacebookBannerPlacementId = @"placementid";

@interface VMaxFacebookBannerAdapter () <FBAdViewDelegate>

@end

@implementation VMaxFacebookBannerAdapter
{
    BOOL shouldDisplayAd;
    
    FBAdView *adView;
    NSString *selectedAdSize;
}

#pragma mark-VMaxCustomAd Delegate

-(void)loadCustomAd: (NSDictionary*)inParams
       withDelegate: (id<VMaxCustomAdListener>)inDelegate
     viewController: (UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    
    self.delegate = inDelegate;
    
    NSString* placementId = @"facebookTest";
    
    if ([inParams objectForKey: kVMaxCustomAdAdapter_FacebookBannerPlacementId]) {
        placementId = [inParams objectForKey: kVMaxCustomAdAdapter_FacebookBannerPlacementId];

    }
    
    FBAdSize bannerSize = kFBAdSize320x50;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        bannerSize = kFBAdSizeHeight90Banner;
    }
    
    NSString *adSize = nil;
    
    if ([inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]) {
        adSize = [[inExtraInfo objectForKey:kVMaxCustomAdExtras_AdSettings]objectForKey:kVMaxCustomAdExtras_AdSize];
        selectedAdSize = adSize;
    }
    
    if (adSize) {
        if ([adSize isEqualToString:kVMaxCustomAdAdSize_320x50]){bannerSize = kFBAdSize320x50;}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_728x90]){bannerSize = kFBAdSizeHeight90Banner;}
        
        else if ([adSize isEqualToString:kVMaxCustomAdSize_300x250]){bannerSize = kFBAdSizeHeight250Rectangle;}
        
        
    }

    
    adView = [[FBAdView alloc] initWithPlacementID: placementId
                                            adSize: bannerSize
                                rootViewController: self.parentViewController];

    if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
        
        NSArray *testDevices = [inExtraInfo objectForKey:kVMaxCustomAdExtras_TestDevices];
        if(testDevices && testDevices.count){
            [FBAdSettings addTestDevices:testDevices];
             NSLog(@"Added FB test Devices:%@",testDevices);
        }
        
    }
    adView.delegate = self;
    [adView disableAutoRefresh];
    
    [adView loadAd];
}

-(void)showAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
    }
}

-(void)invalidateAd
{
    self.delegate = nil;
    
    if (adView.superview) {
        [adView removeFromSuperview];
        adView = nil;
    }
}

-(void)dealloc
{
    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
}

#pragma mark-FBAdView Delegate

- (void)adView:(FBAdView *)adViewIn didFailWithError:(NSError *)error;
{
    
    
    NSLog(@"%@", error);
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
        
        NSLog(@"Failure Callback:%@", error);

    }
}

- (void)adViewDidLoad:(FBAdView *)inAdView;
{
       if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didLoadAdInView:)]) {

           CGRect frame = inAdView.frame;
           
            if ([selectedAdSize isEqualToString:kVMaxCustomAdSize_728x90]){
                frame.size.width = 728;
            }
           
           else if ([selectedAdSize isEqualToString:kVMaxCustomAdSize_300x250]){
               frame.size.width = 300;
           }
           [inAdView setFrame:frame];

               [self.delegate performSelector:@selector(VMaxCustomAd:didLoadAdInView:) withObject:self withObject:inAdView];
        NSLog(@"Success Callback:DidReceiveAd");
        

    }
}

- (void)adViewDidClick:(FBAdView *)adView
{
    NSLog(@"adViewDidClick");
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidInteractAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidInteractAd:) withObject:self];
    }
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    NSLog(@"The user finished to interact with the ad.");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidFinishInteractAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidFinishInteractAd:) withObject:self];
    }
}

- (void)adViewWillLogImpression:(nonnull FBAdView *)adView
{
    NSLog(@"adViewWillLogImpression");
}


@end
