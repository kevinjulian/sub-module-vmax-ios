//
//  VMaxFacebookAdInterface.m
//  VMaxClientSideMediationSample
//
//  Copyright (c) 2014 VMax.com. All rights reserved.
//

#import <FBAudienceNetwork/FBAudienceNetwork.h>

#import "VMaxFacebookInterstitialAdapter.h"

NSString* const kVMaxCustomAdAdapter_FacebookInterstetialPlacementId = @"placementid";

@interface VMaxFacebookInterstitialAdapter () <FBInterstitialAdDelegate>

@end

@implementation VMaxFacebookInterstitialAdapter
{
    FBInterstitialAd *interstitialAd;
}

#pragma mark-VMaxCustomAd Delegate

-(void)loadCustomAd:(NSDictionary*)inParams
       withDelegate:(id<VMaxCustomAdListener>)inDelegate
     viewController:(UIViewController*)parentViewController
         withExtras:(NSDictionary*)inExtraInfo
{
    self.parentViewController = parentViewController;
    
    self.delegate = inDelegate;
    
    NSString* facebookPlacementId = @"facebookTest";
    
    if ([inParams objectForKey: kVMaxCustomAdAdapter_FacebookInterstetialPlacementId]) {
        facebookPlacementId = [inParams objectForKey: kVMaxCustomAdAdapter_FacebookInterstetialPlacementId];
    }
    interstitialAd = [[FBInterstitialAd alloc] initWithPlacementID: facebookPlacementId];
    interstitialAd.delegate = self;
    
    if ([[inExtraInfo objectForKey:kVMaxCustomAdExtras_TestMode]isEqualToString:@"1"]) {
        
        NSArray *testDevices = [inExtraInfo objectForKey:kVMaxCustomAdExtras_TestDevices];
        if(testDevices && testDevices.count){
            [FBAdSettings addTestDevices:testDevices];
            NSLog(@"Added FB test Devices:%@",testDevices);
        }
        
    }
    [interstitialAd loadAd];
}

-(void)showAd
{
    if (interstitialAd) {
        [interstitialAd showAdFromRootViewController: self.parentViewController];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidShowAd:)]) {
            [self.delegate performSelector:@selector(VMaxCustomAdDidShowAd:) withObject:self];
        }
    }
}

-(void)invalidateAd
{
    self.delegate = nil;

    if (interstitialAd.parentViewController) {
        [interstitialAd dismissViewControllerAnimated:YES completion:^{
            interstitialAd = nil;
        }];
    }
}

//-(void)dealloc
//{
//    NSLog(@"Deallocated %@", NSStringFromClass([self class]));
//}

#pragma mark - FBInterstitialAd Delegate

- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAdIn
{
    NSLog(@"Success Callback:adViewDidLoad");

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidLoadAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidLoadAd:) withObject:self];
        NSLog(@"Success Callback:DidReceiveAd");
        
    }
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError %@", error);

    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAd:didFailWithError:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAd:didFailWithError:) withObject:self withObject:error];
        
        NSLog(@"Failure Callback:%@", error);

    }
}

- (void)interstitialAdDidClick:(FBInterstitialAd *)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdOnAdClicked:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdOnAdClicked:) withObject:self];
    }
}

- (void)interstitialAdWillClose:(FBInterstitialAd *)interstitialAd
{
}

- (void)interstitialAdDidClose:(FBInterstitialAd *)interstitialAd
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(VMaxCustomAdDidDismissAd:)]) {
        [self.delegate performSelector:@selector(VMaxCustomAdDidDismissAd:) withObject:self];
    }
}

@end
