//
//  VservCBridge.h
//  
//
//  Created by Jesudas Lobo on 10/12/14.
//
//

#import <Foundation/Foundation.h>

@interface VMaxCBridge : NSObject

@end

extern "C" {
    //Defining function pointers for callbacks
    //Function Pointer : For function with no return value and one parameter(string)
    typedef void (*VMaxAdCallbackWithString)(int ,const char *);
    //Function Pointer : For function with no return value and one parameter(string)
    typedef void (*VMaxAdCallbackWithInt)(int ,int);
    //Function Pointer : For function with no return value and no parameters
    typedef void (*VMaxAdCallback)(int);
    //Function Pointer : For VMax Wallet with additional int parameter
    typedef void (*VMaxWalletCallbackWithInt)(const char *,int);
    //Function Pointer : For VMax Wallet with additional string parameter
    typedef void (*VMaxWalletCallbackWithString)(const char *,const char *);
}
static VMaxAdCallbackWithString adViewDidCacheAd;
static VMaxAdCallback           adViewDidLoadAd;
static VMaxAdCallbackWithString adViewDidFailedToLoadAd;
static VMaxAdCallback           didInteractWithAd;
static VMaxAdCallback           willPresentOverlay;
static VMaxAdCallback           willDismissOverlay;
static VMaxAdCallback           willLeaveApp;
static VMaxAdCallback           adViewDidCollapse;
static VMaxAdCallback           adViewDidExpand;
static VMaxAdCallbackWithString	didInterruptRewardVideo;
static VMaxAdCallbackWithString	didFailedToPlaybackRewardVideo;
static VMaxAdCallbackWithInt	didCompleteRewardVideo;
static VMaxAdCallback 			willShowRewardedVideoPrepopUp;
static VMaxAdCallback 			willShowRewardedVideoPostpopUp;
static VMaxAdCallback 			willShowRewardedVideoNoFillpopUp;
static VMaxAdCallback 			willShowRewardedVideoImpressionCappopUp;
static VMaxAdCallback 			willShowRewardedVideoAdInterruptedpopUp;
static VMaxWalletCallbackWithInt       didUpdateWalletElement;
static VMaxWalletCallbackWithString    didFailtoUpdateWalletElement;


