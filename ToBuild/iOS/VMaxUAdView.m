//
//  VMaxUAdView.m
//  VMaxAdSDK
//
//  Created by RAHUL CK on 10/12/14.
//  Copyright (c) 2014 VMax.mobi. All rights reserved.
//
#import "VMaxUAdView.h"
#import "VMaxWallet.h"
#import "VMaxRewardedVideo.h"
#import "VMaxIconPopupView.h"

//User Params
NSString* const kVMaxUserParam_gender     = @"gender";
NSString* const kVMaxUserParam_age        = @"age";
NSString* const kVMaxUserParam_city       = @"city";
NSString* const kVMaxUserParam_email      = @"email";
NSString* const kVMaxUserParam_Name       = @"name";

void ThrowDidCacheAd(int instanceId);

void ThrowDidLoadAd(int instanceId);

void ThrowDidAdViewFailedToLoadAd(int instanceId, const char* inError);

void ThrowDidInteractWithAd(int instanceId);

void ThrowWillPresentAd(int instanceId);

void ThrowWillDismissAd(int instanceId);

void ThrowAdViewDidExpand(int instanceId);

void ThrowAdViewDidCollapse(int instanceId);

void ThrowWillLeaveApp(int instanceId);

void ThrowDidInterruptRewardVideo(int adInstance,const char * message);
void ThrowDidFailedToPlaybackRewardVideo(int adInstance,const char * errorMessage);
void ThrowDidCompleteRewardVideo(int adInstance, int rewardAmount);
void ThrowWillShowRewardedVideoPrepopUp(int adInstance);
void ThrowWillShowRewardedVideoPostpopUp(int adInstance);
void ThrowWillShowRewardedVideoNoFillpopUp (int adInstance);
void ThrowWillShowRewardedVideoImpressionCappopUp(int adInstance);
void ThrowWillShowRewardedVideoAdInterruptedpopUp(int adInstance);
void ThrowDidUpdateWalletElement(const char *  walletElement, int currencyValue);
void ThrowDidFailtoUpdateWalletElement(const char *  walletElement,const char *  errorMessage);

static CGRect GetAdViewRect(VMaxUBannerAdPlacement inPlacement)
{
    CGRect adRect = CGRectZero;
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    UIDevice* thisDevice = [UIDevice currentDevice];
    
    NSInteger adheight = (thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)? 90 : 50;
    
    adRect = CGRectMake(0, 0, screenSize.width, adheight);
    adRect.origin.y = (inPlacement == kVMaxUBannerAdPlacementBottom)? (screenSize.height - adheight) : 0;
    
    return adRect;
}

static CGRect GetAdViewRectForSBD(VMaxUBannerAdPlacement placement, NSString * sbdSize)
{
	CGRect frame = CGRectZero;
	CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
	if ([sbdSize isEqualToString:kVMaxAdSize_320x50]) {
	    CGFloat y = (placement == kVMaxUBannerAdPlacementBottom)? screenSize.height-50:0;
	    frame = CGRectMake((screenSize.width-320)/2, y, 320, 50);
	} else if ([sbdSize isEqualToString:kVMaxAdSize_728x90]) {
	    CGFloat y = (placement == kVMaxUBannerAdPlacementBottom)? screenSize.height-90:0;
	    frame = CGRectMake((screenSize.width-728)/2, y, 728, 90);
	} else if ([sbdSize isEqualToString:kVMaxAdSize_300x250]) {
	    CGFloat y = (placement == kVMaxUBannerAdPlacementBottom)? screenSize.height-250:0;
	    frame = CGRectMake((screenSize.width-300)/2, y, 300, 250);
	} else if ([sbdSize isEqualToString:kVMaxAdSize_480x320]) {
	    CGFloat y = (placement == kVMaxUBannerAdPlacementBottom)? screenSize.height-320:0;
	    frame = CGRectMake((screenSize.width-480)/2, y, 480, 320);
	} else if ([sbdSize isEqualToString:kVMaxAdSize_480x800]) {
	    CGFloat y = (placement == kVMaxUBannerAdPlacementBottom)? screenSize.height-800:0;
	    frame = CGRectMake((screenSize.width-480)/2, y, 480, 800);
	} else if ([sbdSize isEqualToString:kVMaxAdSize_1024x600]) {
	    CGFloat y = (placement == kVMaxUBannerAdPlacementBottom)? screenSize.height-600:0;
	    frame = CGRectMake((screenSize.width-1024)/2, y, 1024, 600);
	}
    return frame;
}

@interface VMaxUAdView () <VMaxAdDelegate,VMaxRewardedVideoDelegate,VMaxWalletElementDelegate,VMaxNativeAdDelegate,VMaxIconPopupViewDelegate>

@property (nonatomic, strong) VMaxAdView *vmaxAdView;

@property (nonatomic, strong) VMaxNativeAd *nativeAd;

@property (nonatomic, strong) NSString *vmaxAdSize;

@property(nonatomic, assign) VMaxAdUX AdUXType;

@property (nonatomic, assign) int instanceId;

@property (nonatomic, assign) BOOL isSendingRequest;

@property (nonatomic, assign) VMaxUBannerAdPlacement placement;

@property (nonatomic, strong) VMaxIconPopupView *popupView;

@property (nonatomic, assign) BOOL popupFullScreenWasPresented;


@end

static NSMutableDictionary *vmaxAdViewInstanceList;
static NSMutableDictionary *vmaxRewardedVideoInstanceList;
static NSMutableDictionary *vmaxNativeAdInstanceList;

static unsigned int vmaxInstanceCount;

@implementation VMaxUAdView

+(VMaxUAdView*) getVMaxAdViewInstance: (int)instanceId
{
    VMaxUAdView *vmaxUAdView = nil;
    
    vmaxUAdView = [vmaxAdViewInstanceList valueForKey:[VMaxUAdView getVMaxInstanceId:instanceId]];
    
    return vmaxUAdView;
}

+(int) createInterstitialAdForZoneID:(const char*)inZoneID
{
    VMaxUAdView *vmaxUadView = [[VMaxUAdView alloc]initWithZoneId:inZoneID adType:kVMaxAdUX_Interstitial];
#if !__has_feature(objc_arc)
    vmaxUadView = [vmaxUadView autorelease];
#endif
    if (!vmaxAdViewInstanceList)
        vmaxAdViewInstanceList = [[NSMutableDictionary alloc]init];
    
    if (!vmaxRewardedVideoInstanceList)
        vmaxRewardedVideoInstanceList = [[NSMutableDictionary alloc]init];
    
    vmaxInstanceCount++;
    
    vmaxUadView.adUx = kVMaxAdUX_Interstitial;
    
    vmaxUadView->_instanceId = vmaxInstanceCount;
    
    [vmaxAdViewInstanceList setObject:vmaxUadView forKey:[VMaxUAdView getVMaxInstanceId:vmaxInstanceCount]];
    
    
    return vmaxInstanceCount;
}

+(int) createBannerAdForZoneID:(const char*)inZoneID
                         forUX:(VMaxAdUX) adUX
                  forPlacement:(VMaxUBannerAdPlacement)inPlamenent
{
    VMaxUAdView *vmaxUadView = [[VMaxUAdView alloc]initWithZoneId:inZoneID adType:adUX];
#if !__has_feature(objc_arc)
    vmaxUadView = [vmaxUadView autorelease];
#endif
    vmaxUadView->_placement = inPlamenent;
    
    if (!vmaxAdViewInstanceList)
        vmaxAdViewInstanceList = [[NSMutableDictionary alloc]init];
    
    vmaxInstanceCount++;
    
    vmaxUadView->_instanceId = vmaxInstanceCount;
    vmaxUadView.adUx = adUX;

    
    [vmaxAdViewInstanceList setObject:vmaxUadView forKey:[VMaxUAdView getVMaxInstanceId:vmaxInstanceCount]];
    
    return vmaxInstanceCount;
}
+(int) createNativeAdAdForZoneID: (const char*)inZoneID
{
    VMaxUAdView *vmaxUadView = [[VMaxUAdView alloc]initWithZoneId:inZoneID adType:kVMaxAdUX_Native];
#if !__has_feature(objc_arc)
    vmaxUadView = [vmaxUadView autorelease];
#endif
    if (!vmaxAdViewInstanceList)
        vmaxAdViewInstanceList = [[NSMutableDictionary alloc]init];
    
    vmaxInstanceCount++;
    
    vmaxUadView->_instanceId = vmaxInstanceCount;
    vmaxUadView.adUx = kVMaxAdUX_Native;
    
    if (!vmaxNativeAdInstanceList)
        vmaxNativeAdInstanceList = [[NSMutableDictionary alloc]init];
    
    [vmaxAdViewInstanceList setObject:vmaxUadView forKey:[VMaxUAdView getVMaxInstanceId:vmaxInstanceCount]];
    
    
    return vmaxInstanceCount;
}


+(NSString*) getVMaxInstanceId:(unsigned int)instanceCount
{
    return [NSString stringWithFormat:@"VMaxInstance_%d",instanceCount];
}
+(NSString*) getVMaxRewardedVideoInstanceId:(unsigned int)instanceCount
{
    return [NSString stringWithFormat:@"VMaxRewardedVideoInstance_%d",instanceCount];
}
+(NSString*) getVMaxNativeInstanceId:(unsigned int)instanceCount
{
    return [NSString stringWithFormat:@"VMaxNativeInstance_%d",instanceCount];
}
+(void) releaseVMaxAdViewInstance: (int)instanceId
{

    [vmaxAdViewInstanceList removeObjectForKey:[VMaxUAdView getVMaxInstanceId:instanceId]];
    
    
    if (vmaxInstanceCount == 0 ) {
        vmaxAdViewInstanceList = nil;
    }
}

-(void) initRewardedVideo:(const char*)walletElement
{
    VMaxWallet *sharedWallet = [VMaxWallet sharedVMaxWallet];
    VMaxWalletElement *vmaxWalletElement = [sharedWallet walletElementWithCurrency:[NSString stringWithUTF8String:walletElement]];
    vmaxWalletElement.delegate = self;
    VMaxRewardedVideo *rewardedVideo = [[VMaxRewardedVideo alloc]initWithWalletElement:vmaxWalletElement];
    rewardedVideo.delegate = self;
    [vmaxRewardedVideoInstanceList setObject:rewardedVideo forKey:[VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    [self.vmaxAdView setRewardedVideo:rewardedVideo];
    
}
-(void) setShouldShowMaxImpressionCapPopup:(BOOL)shouldShowMaxImpressionCapPopup
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:[VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setShouldShowMaxImpressionCapPopup:shouldShowMaxImpressionCapPopup];
    }
}
-(void) setMaxImpressionCapPopUpWithTitle:(const char *)title message:(const char *)message
{
    
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setMaxImpressionCapPopUpWithTitle:[NSString stringWithUTF8String:title] message:[NSString stringWithUTF8String:message]];
    }
}
-(void) setShouldShowPrePopup:(BOOL)shouldShowPrePopup
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setShouldShowPrePopup:shouldShowPrePopup];
    }
}
-(void) setPrePopUpWithTitle:(const char*)title message:(const char*)message
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setPrePopUpWithTitle:[NSString stringWithUTF8String:title] message:[NSString stringWithUTF8String:message]];
    }
    
}
-(void) setShouldShowPostPopup:(BOOL)shouldShowPostPopup
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setShouldShowPostPopup:shouldShowPostPopup];
    }
}
-(void) setPostPopUpWithTitle:(const char*)title message:(const char*)message
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setPostPopUpWithTitle:[NSString stringWithUTF8String:title] message:[NSString stringWithUTF8String:message]];
    }
}
-(void) setShouldShowInterruptPopup:(BOOL)shouldShowInterruptPopup
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setShouldShowInterruptPopup:shouldShowInterruptPopup];
    }
}
-(void) setInterruptPopUpWithTitle:(const char*)title message:(const char*)message
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setInterruptPopUpWithTitle:[NSString stringWithUTF8String:title] message:[NSString stringWithUTF8String:message]];
    }
}
-(void) setShouldShowNoFillPopup:(BOOL)shouldShowNoFillPopup
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setShouldShowNoFillPopup:shouldShowNoFillPopup];
    }
}

-(void) setNoFillPopUpWithTitle:(const char*)title message:(const char*)message
{
    VMaxRewardedVideo *rewardedVideo = [vmaxRewardedVideoInstanceList objectForKey:
                                        [VMaxUAdView getVMaxRewardedVideoInstanceId:self.instanceId]];
    if (rewardedVideo) {
        [rewardedVideo setNoFillPopUpWithTitle:[NSString stringWithUTF8String:title] message:[NSString stringWithUTF8String:message]];
    }
}

-(void)updateBannerOrientation:(NSNotification*)inNote
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    CGRect adRect = self.vmaxAdView.frame;
    
    if(self.AdUXType == kVMaxAdUX_IN_Line_Display)
    {
        adRect = GetAdViewRectForSBD(self.placement, self.vmaxAdSize);
    }
    else
    {
        adRect.size.width = screenSize.width;
    }
    self.vmaxAdView.frame = adRect;
    
}

-(id)initWithZoneId:(const char*)inZoneId adType:(VMaxAdUX)inAdUX
{
    if (self = [super init]) {
        
        UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        
        self.AdUXType = inAdUX;
        
        self.isSendingRequest = NO;
        
        self.vmaxAdView = [[VMaxAdView alloc]initWithAdspotID: [NSString stringWithUTF8String:inZoneId]
                                              viewController : rootViewController
                                                withAdUXType : inAdUX];
        
        self.vmaxAdView.delegate = self;
        
        if (self.AdUXType == kVMaxAdUX_Banner || self.AdUXType == kVMaxAdUX_IN_Line_Display) {
            [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
            
            [[NSNotificationCenter defaultCenter] addObserver: self
                                                     selector: @selector(updateBannerOrientation:)
                                                         name: UIDeviceOrientationDidChangeNotification
                                                       object: nil];
        }
    }
    
    return self;
}

-(void)dealloc
{
    if (self.AdUXType == kVMaxAdUX_Banner || self.AdUXType == kVMaxAdUX_IN_Line_Display) {
        if (self.vmaxAdView.superview) {
            [self.vmaxAdView removeFromSuperview];
        }
        
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
        
    }
    
    self.vmaxAdView.delegate = nil;
    
    if (self.vmaxAdView) {
#if !__has_feature(objc_arc)
        [self.vmaxAdView release];
#endif
        self.vmaxAdView = nil;
    }
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

-(void)setRefresh:(BOOL)shouldEnableRefresh
{
    [self.vmaxAdView setRefresh:shouldEnableRefresh];
}


-(void)setRefreshRate:(UInt32)inRefreshIntervalInSeconds
{
    [self.vmaxAdView setRefreshRate:inRefreshIntervalInSeconds];
}

-(void)pauseRefresh
{
    [self.vmaxAdView pauseRefresh];
}

-(void)stopRefresh
{
    [self.vmaxAdView stopRefresh];
}

-(void)resumeRefresh
{
    [self.vmaxAdView resumeRefresh];
}

-(void)setTimeout:(UInt32)inTimeout
{
    [self.vmaxAdView setTimeout:inTimeout];
}

+ (UIColor *)UIColorWithHexString:(NSString *)hexString
{
    unsigned int rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0
                           green:((rgbValue & 0xFF00) >> 8)/255.0
                            blue:(rgbValue & 0xFF)/255.0
                           alpha:1.0];
}

-(void)setUXType:(VMaxAdUX)inAdUXType
      withConfig:(const char*)configString
{
    NSArray *configArray=[[NSString stringWithUTF8String:configString]componentsSeparatedByString:@","];
    
    NSMutableDictionary *inConfig = [NSMutableDictionary dictionary];
    
    NSString* keyPrefix = @"VMaxAdView_";
    
    for (NSString *config in configArray) {
        NSArray *configuration = [config componentsSeparatedByString:@":"];
        NSString *configKey = [keyPrefix stringByAppendingString:[configuration objectAtIndex:0]];
        NSString *configValue = [configuration objectAtIndex:1];
        [inConfig setObject:configValue forKey:configKey];
        
        if (   [configKey isEqualToString:VMaxAdView_BackgroundColor]
            || [configKey isEqualToString:VMaxAdView_FrameColor]) {
            
            UIColor* color = [[self class] UIColorWithHexString:configValue];
            [inConfig setObject:color forKey:configKey];
        }
    }
    
    [self.vmaxAdView setUXType:inAdUXType withConfig:inConfig];
}

-(void)cacheAd
{
    if (!self.isSendingRequest) {
        self.isSendingRequest = YES;
        [self.vmaxAdView cacheAd];
    }
}

-(void)cacheAdWithOrientation:(VMaxAdOrientation)inOrientation
{
    if (!self.isSendingRequest) {
        self.isSendingRequest = YES;
        [self.vmaxAdView cacheAdWithOrientation:inOrientation];
    }
}

-(void)loadAd
{
    if (!self.isSendingRequest) {
        self.isSendingRequest = YES;
        [self.vmaxAdView loadAd];
        [self addBannerToScreen];
    }
}

-(void)loadAdWithOrientation:(VMaxAdOrientation)inOrientation
{
    if (!self.isSendingRequest) {
        self.isSendingRequest = YES;
        [self.vmaxAdView loadAdWithOrientation:inOrientation];
        [self addBannerToScreen];
    }
}

-(void)addBannerToScreen
{
    if (self.AdUXType == kVMaxAdUX_Banner){
        if (!self.vmaxAdView.superview) {
            
            UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            
            CGRect adRect = GetAdViewRect(_placement);
            
            [self.vmaxAdView setFrame:adRect];
            
            [rootViewController.view addSubview:self.vmaxAdView];
        }
    }else if(self.AdUXType == kVMaxAdUX_IN_Line_Display){
        if (!self.vmaxAdView.superview) {
            
            UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            
            CGRect adRect = GetAdViewRectForSBD(_placement,self.vmaxAdSize);
            
            [self.vmaxAdView setFrame:adRect];
            
            [rootViewController.view addSubview:self.vmaxAdView];
        }
    }
}

/*!
 @function      setAdSettings
 @abstract      set settings to AdView
 */

-(void) setAdSettings:(const char*)adSettingsJson
{
    NSError *jsonError;
    NSDictionary *adSettings = nil;
    NSData *data = [[NSString stringWithUTF8String:adSettingsJson]dataUsingEncoding:NSUTF8StringEncoding];
    if (data) {
        adSettings = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (!jsonError) {
            [self.vmaxAdView setAdSettings:adSettings];
			self.vmaxAdSize = [adSettings objectForKey:VMaxAdView_AdSize];
        }
    }
}

 -(void)hideBannerAdView
{
    if (self.vmaxAdView) {
        self.vmaxAdView.hidden = YES;
    }
}

-(void)showBannerAdView
{
    if (self.vmaxAdView) {
    self.vmaxAdView.hidden = NO;
    }
}
-(void)showAd
{
    [self.vmaxAdView showAd];
    [self addBannerToScreen];
}

-(void)cancelAd
{
    [self.vmaxAdView cancelAd];
    self.isSendingRequest = NO;
}

-(VMaxAdState)getAdState
{
    return [self.vmaxAdView getAdState];
}

-(void)setTestDevice:(const char*)deviceIDFAListString
{
    
}

-(void)setUser:(const char *)userParams
{
    NSArray *userParamsArray = [[NSString stringWithUTF8String:userParams]componentsSeparatedByString:@","];
    
    VMaxAdUser * user = [[VMaxAdUser alloc]init];
    
    for (NSString *param in userParamsArray) {
        
        NSArray *userParam = [param componentsSeparatedByString:@":"];
        NSString *userParamKey = [userParam objectAtIndex:0];
        NSString *userParamValue = (userParam.count>1)?[userParam objectAtIndex:1]:@"";
        
        if ([userParamKey isEqualToString:kVMaxUserParam_gender])
            user.gender = userParamValue.integerValue;
        else if ([userParamKey isEqualToString:kVMaxUserParam_age])
            user.age = userParamValue;
        else if ([userParamKey isEqualToString:kVMaxUserParam_city])
            user.city = userParamValue;
        else if ([userParamKey isEqualToString:kVMaxUserParam_email])
            user.email = userParamValue;
    }
    
    
    [self.vmaxAdView setUser:user];
}

-(void)blockAds:(BOOL)inShouldBlockAds
{
    [self.vmaxAdView blockAds:inShouldBlockAds];
}

-(void)showAdsAfterSessions:(int)inNoOfSessions
{
    [self.vmaxAdView showAdsAfterSessions:inNoOfSessions];
}

-(void)blockCountries:(const char *)countryListString blockOrAllow:(VMaxCountryOption)inAllowOrBlock shouldRequestAdIfMCCUnawailable:(BOOL)inShouldRequest
{
    NSArray *inCountryList=[[NSString stringWithUTF8String:countryListString]componentsSeparatedByString:@","];
    
    [self.vmaxAdView blockCountries:inCountryList blockOrAllow:inAllowOrBlock shouldRequestAdIfMCCUnawailable:inShouldRequest];
}

-(void)disableOffline:(BOOL)inShouldDisableOffline
{
    [self.vmaxAdView disableOffline:inShouldDisableOffline];
}

-(void)userDidIAP:(BOOL)inDidIAP
{
    [self.vmaxAdView userDidIAP:inDidIAP];
}

-(void)userDidIncent:(BOOL)inDidIncent
{
    [self.vmaxAdView userDidIncent:inDidIncent];
}

-(void)setLanguageOfArticle:(const char*)inLanguageofArticle
{
    [self.vmaxAdView setLanguageOfArticle:[NSString stringWithUTF8String:inLanguageofArticle]];
}

#pragma mark - VMaxAdDelegate

/*!
 @function  adViewDidCacheAd:
 @abstract  This method will be called by SDK when the ad is cached and cacheAdWithorientation: in the background. The ad is not displayed yet. To display the ad call showAd method.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)adViewDidCacheAd:(VMaxAdView *)theAdView
{
    self.isSendingRequest = NO;
    if (self.adUx == kVMaxAdUX_Native) {
        self.nativeAd = [theAdView getNativeAd];
        self.nativeAd.autoLoading = YES;
        [vmaxNativeAdInstanceList setObject:self.nativeAd forKey:[VMaxUAdView getVMaxNativeInstanceId:vmaxInstanceCount]];
        self.nativeAd.nativeAdDelegate = self;
    }
    ThrowDidCacheAd(self.instanceId);
}

/*!
 @function  adViewDidLoadAd:
 @abstract  This method will be called by SDK when the ad is loaded to the ad spot and is visible to the user.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)adViewDidLoadAd:(VMaxAdView *)theAdView
{
    self.isSendingRequest = NO;
    ThrowDidLoadAd(self.instanceId);

   
}

/*!
 @function  adView: adViewDidFailedToLoadAd:
 @abstract  This method will be called by SDK when the adview failed to load the ad.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 @param     error, NSError. Description of error occured.
 */
- (void)adView:(VMaxAdView *)theAdView didFailedToLoadAd:(NSError *)error
{
    self.isSendingRequest = NO;
    ThrowDidAdViewFailedToLoadAd(self.instanceId, [error.localizedDescription UTF8String]);
}

/*!
 @function  didInteractWithAd:
 @abstract  This method will be called when used interacts with the ad.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)didInteractWithAd:(VMaxAdView *)theAdView
{
    ThrowDidInteractWithAd(self.instanceId);
}

/*!
 @function  willPresentOverlay:
 @abstract  This method will be called when the SDK is about to present the overlay on top of the user view.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)willPresentAd:(VMaxAdView *)theAdView
{
    ThrowWillPresentAd(self.instanceId);
}

/*!
 @function  willDismissOverlay:
 @abstract  This method will be called when the SDK is about to dismis the Overlay put by the SDK.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)willDismissAd:(VMaxAdView *)theAdView
{
    self.isSendingRequest = NO;
    ThrowWillDismissAd(self.instanceId);
}
- (void)adViewDidCollapse:(VMaxAdView *)theAdView
{
    self.isSendingRequest = NO;
    ThrowAdViewDidCollapse(self.instanceId);
}
-(void)adViewDidExpand:(VMaxAdView *)theAdView
{
    self.isSendingRequest = NO;
    ThrowAdViewDidExpand(self.instanceId);
}
#pragma mark-VMaxRewarded Video Delegate
- (void)adView:(VMaxAdView *)theAdView didInterruptRewardVideo:(NSString *)message
{
    ThrowDidInterruptRewardVideo(self.instanceId, message.UTF8String);
}

- (void)adView:(VMaxAdView *)theAdView didFailedToPlaybackRewardVideo:(NSError*)error
{
    ThrowDidFailedToPlaybackRewardVideo(self.instanceId, [[error localizedDescription]UTF8String]);
}

- (void)adView:(VMaxAdView *)theAdView didCompleteRewardVideo:(NSInteger)rewardAmount
{
    ThrowDidCompleteRewardVideo(self.instanceId, (int)rewardAmount);
}

-(void) willShowRewardedVideoPrepopUp
{
    ThrowWillShowRewardedVideoPrepopUp(self.instanceId);
}

-(void) willShowRewardedVideoPostpopUp
{
    ThrowWillShowRewardedVideoPostpopUp(self.instanceId);
}

-(void) willShowRewardedVideoNoFillpopUp
{
    ThrowWillShowRewardedVideoNoFillpopUp(self.instanceId);
}

-(void) willShowRewardedVideoImpressionCappopUp
{
    ThrowWillShowRewardedVideoImpressionCappopUp(self.instanceId);
}

-(void) willShowRewardedVideoAdInterruptedpopUp
{
    ThrowWillShowRewardedVideoAdInterruptedpopUp(self.instanceId);
}

#pragma mark-VMaxRewarded Video Delegate

-(void) didUpdateWalletElement:(VMaxWalletElement*)walletElement withCurrencyBalance:(NSInteger)currency
{
    
}

-(void) didFailtoUpdateWalletElement:(VMaxWalletElement*)walletElement withError:(NSError*)error
{
    
}
/*!
 @function      setTestMode:withDevices
 @abstract      Set the test mode.
 @param         testMode, VMaxTestMode
 @param         inDeviceIDFAList, NSArray, Test device array.
 */
-(void)setTestMode:(VMaxTestMode)testMode withDevices:(const char*)inDeviceIDFAList;
{
    NSArray *deviceIDFAList=[[NSString stringWithUTF8String:inDeviceIDFAList]componentsSeparatedByString:@","];
    [self.vmaxAdView setTestMode:testMode withDevices:deviceIDFAList];
}

/*!
 @function  willLeaveApp:
 @abstract  This method will be called when the SDK is taking the app to background due to action of the ad. For instance when clicking on ad will launch Safari which caused this app to go to background and safari to come front.
 @param     theAdView, VMaxAdView. The ad view which sent this event.
 */
- (void)willLeaveApp:(VMaxAdView *)theAdView
{
    ThrowWillLeaveApp(self.instanceId);
}

-(const char*)getMarkup
{
    const char * markupString = "";
    
    if([self.vmaxAdView getMarkup]){
        markupString=[[self.vmaxAdView getMarkup] UTF8String];
    }
    return markupString;
}
-(const char *) getNativeAdJson
{
    return [VMaxUAdView convertNativeAdToJson:[self.vmaxAdView getNativeAd]];
}
-(void) reportAndHandleNativeAdClick
{
    if (self.nativeAd) {
        [self.nativeAd reportAndHandleAdClick];
    }
}

-(void) registerNativeAdForImpression
{
//    if (self.nativeAd) {
//        [self.nativeAd registerViewForInteraction:nil view:nil listOfViews:nil];
//    }
    //As per change regarding impr
}
-(void)nativeIconClicked
{
    
    if (self.nativeAd) {
      [self prefetchNativeImages];
        self.popupFullScreenWasPresented = false;
        if ([self.nativeAd.nativeAdPartner isEqualToString:@"Facebook"]) {
            if (!self.popupView.view.superview) {
                [self presentNativeAdPopUp];
            }
        }
        else{
            [self presentNativeAdPopUp];
        }
    }
}
-(void) presentNativeAdPopUp
{
   
    _popupView = [[VMaxIconPopupView alloc] initWithFrame:[UIScreen mainScreen].bounds
                                             withNativeAd:self.nativeAd];
    _popupView.delegate = self;
    
    UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];

   [rootViewController.view  addSubview:_popupView.view];
     _popupView.view.center = rootViewController.view.center;

}
-(void) dismissNativeAdPopUp
{
    
        if (self.popupView.view.superview) {
            [self.popupView.view removeFromSuperview];
        }
    self.popupView = nil;

}
-(void) prefetchNativeImages
{
    // prefetch required images to speedup popup rendering
    if(self.nativeAd.imageMain) {
        
        [self.nativeAd.imageMain loadImageAsyncWithBlock:^(UIImage *image) {}];
        
    }
    else if(self.nativeAd.imageMedium) {
        
        [self.nativeAd.imageMedium loadImageAsyncWithBlock:^(UIImage *image) {}];
    }
}
#pragma mark -PopLib Delegate
- (void)popupDidShow
{
    if (self.popupView) {
        [self.nativeAd registerViewForInteraction:nil view:self.popupView.view listOfViews:@[self.popupView.actionButton]];
        ThrowAdViewDidExpand(self.instanceId);
    }
  
}
- (void)popupDidClose
{
    
    [self dismissNativeAdPopUp];
    
    ThrowAdViewDidCollapse(self.instanceId);
}
- (void)popupWillEnterFullscreen {

}

- (void)popupWillExitFullscreen {

    [self.popupView resumeMediaViewPlaybackIfNeeded];


}

#pragma mark NativeAdHandling -
- (void)nativeAdwillPresentOverlay:(VMaxNativeAd *)nativeAd; {
    
    [self.popupView resumeMediaViewPlaybackIfNeeded];
}

- (void)nativeAdwillDismissOverlay:(VMaxNativeAd *)nativeAd
{
    
}
#pragma Mark-UIView Touches Related Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesBegan");
    
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesMoved");
    
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesEnded");
    
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesCancelled");
    
}
+(const char*) convertNativeAdToJson:(VMaxNativeAd*)nativeAd
{
    NSMutableDictionary *nativeAdDic = [[NSMutableDictionary alloc]init];
    NSString *jsonString = nil;
            NSDictionary *imageIcon = @{@"imageurl":nativeAd.imageIcon.url.absoluteString,
                                        @"width":[NSNumber numberWithInteger:nativeAd.imageIcon.width],
                                        @"height":[NSNumber numberWithInteger:nativeAd.imageIcon.height]};
            nativeAdDic[@"imageIcon"] = imageIcon;
			NSError *error;
        if (nativeAdDic) {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:nativeAdDic
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            if (jsonData)
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
        }
    NSLog(@"%@",jsonString);
    return [jsonString UTF8String];
}
@end


