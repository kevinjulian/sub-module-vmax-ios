//
//  VMaxCBridge.m
//  
//
//  Created by Jesudas Lobo on 10/12/14.
//
//

#import "VMaxCBridge.h"
#import "VMaxUAdView.h"
#import "VMaxWallet.h"

@implementation VMaxCBridge

@end

extern "C"
{
    // Callbacks
    
    /// <summary>
    /// Throws the did cache ad.
    /// </summary>
    void ThrowDidCacheAd(int adInstance)
    {
        NSLog(@"Did Cache Ad");
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        if (AdView.adUx==kVMaxAdUX_Native) {
            char const * nativeAdJson = "";
            if(AdView)
            {
                nativeAdJson = [AdView getNativeAdJson];
                NSLog(@"*** %s",nativeAdJson);
            }
            adViewDidCacheAd(adInstance,nativeAdJson);
        }
        else{
           char const * markupString = "";
        if(AdView)
        {
            markupString = [AdView getMarkup];
        }
        adViewDidCacheAd(adInstance,markupString);
        }
    }
    /// <summary>
    /// Throws the did load ad.
    /// </summary>
    void ThrowDidLoadAd(int adInstance)
    {
        NSLog(@"Did Load Ad");
        adViewDidLoadAd(adInstance);
    }
    /// <summary>
    /// Throws the did ad view failed to load ad.
    /// </summary>
    /// <param name="message">Message.</param>
    void ThrowDidAdViewFailedToLoadAd(int adInstance, const char * message)
    {
        NSLog(@"Did Failed to Load Ad");
        adViewDidFailedToLoadAd(adInstance,message);
    }
    /// <summary>
    /// Throws the did interact with ad.
    /// </summary>
    void ThrowDidInteractWithAd(int adInstance)
    {
        NSLog(@"Did Interact with Ad");
        didInteractWithAd(adInstance);
    }
    
    /// <summary>
    /// Throws the will present overlay.
    /// </summary>
    void ThrowWillPresentAd(int adInstance)
    {
        NSLog(@"Will Present Ad");
        willPresentOverlay(adInstance);
    }
    
    /// <summary>
    /// Throws the will dismiss overlay.
    /// </summary>
    void ThrowWillDismissAd(int adInstance)
    {
        NSLog(@"Will Dismiss Ad");
        willDismissOverlay(adInstance);
    }
    /// <summary>
    /// Ad will expand
    /// </summary>
    void ThrowAdViewDidExpand(int instanceId)
    {
        NSLog(@"DidExpand");
        adViewDidExpand(instanceId);
    

        
    }
    /// <summary>
    /// Ad will collapse
    /// </summary>
    void ThrowAdViewDidCollapse(int instanceId)
    {
        NSLog(@"DidCollapse");
        adViewDidCollapse(instanceId);

    }
    /// <summary>
    /// Throws the will leave app.
    /// </summary>
    void ThrowWillLeaveApp(int adInstance)
    {
        NSLog(@"Will Leave App");
        willLeaveApp(adInstance);
    }
    
    /// <summary>
    /// Throws the did interrupt reward video.
    /// </summary>
    /// <param name="message">Message.</param>
    void ThrowDidInterruptRewardVideo(int adInstance,const char * message)
    {
        NSLog(@"Did Interrupt Rewarded Video Ad");
        didInterruptRewardVideo(adInstance,message);
    }
    /// <summary>
    /// Throws the did failed to playback reward video.
    /// </summary>
    /// <param name="errorMessage">Error message.</param>
    void ThrowDidFailedToPlaybackRewardVideo(int adInstance,const char * errorMessage)
    {
        NSLog(@"Did Failed to Play Rewarded Ad");
        didFailedToPlaybackRewardVideo(adInstance,errorMessage);
    }
    /// <summary>
    /// Throws the did complete reward video.
    /// </summary>
    /// <param name="rewardAmount">Reward amount.</param>
    void ThrowDidCompleteRewardVideo(int adInstance, int rewardAmount)
    {
        NSLog(@"Did Complete Rewarded Video Ad");
        didCompleteRewardVideo(adInstance,rewardAmount);
    }
    /// <summary>
    /// Throws the will show rewarded video prepop up.
    /// </summary>
    void ThrowWillShowRewardedVideoPrepopUp(int adInstance)
    {
        NSLog(@"Will Show Rewarded Video Pre Popup");
        willShowRewardedVideoPrepopUp(adInstance);
    }
    /// <summary>
    /// Throws the will show rewarded video postpop up.
    /// </summary>
    void ThrowWillShowRewardedVideoPostpopUp(int adInstance)
    {
        NSLog(@"Will Show Rewarded Video Post Popup");
        willShowRewardedVideoPostpopUp(adInstance);
    }
    /// <summary>
    /// Throws the will show rewarded video no fillpop up.
    /// </summary>
    void ThrowWillShowRewardedVideoNoFillpopUp (int adInstance)
    {
        NSLog(@"Will Show Rewarded Video No Fill Popup");
        willShowRewardedVideoNoFillpopUp(adInstance);
    }
    /// <summary>
    /// Throws the will show rewarded video impression cappop up.
    /// </summary>
    void ThrowWillShowRewardedVideoImpressionCappopUp(int adInstance)
    {
        NSLog(@"Will Show Rewarded Video Impression Popup");
        willShowRewardedVideoImpressionCappopUp(adInstance);
    }
    /// <summary>
    /// Throws the will show rewarded video ad interruptedpop up.
    /// </summary>
    void ThrowWillShowRewardedVideoAdInterruptedpopUp(int adInstance)
    {
        NSLog(@"Will Show Rewarded Video Interrupted Popup");
        willShowRewardedVideoAdInterruptedpopUp(adInstance);
    }
    void ThrowDidUpdateWalletElement(const char *  walletElement, int currencyValue)
    {
        NSLog(@"Did Update Wallet Element");
        didUpdateWalletElement(walletElement,currencyValue);
    }
    void ThrowDidFailtoUpdateWalletElement(const char *  walletElement,const char *  errorMessage)
    {
        NSLog(@"Did Failed to update Wallet Element");
        didFailtoUpdateWalletElement(walletElement, errorMessage);
    }
    //Native Functions
    
    void _initializeCallbacks( VMaxAdCallbackWithString     _adViewDidCacheAd,
                               VMaxAdCallback               _adViewDidLoadAd,
                               VMaxAdCallbackWithString     _adViewDidFailedToLoadAd,
                               VMaxAdCallback               _didInteractWithAd,
                               VMaxAdCallback               _willPresentOverlay,
                               VMaxAdCallback               _willDismissOverlay,
                               VMaxAdCallback               _adViewDidExpand,
                               VMaxAdCallback               _adViewDidCollapse,
                               VMaxAdCallback               _willLeaveApp,
                               VMaxAdCallbackWithString     _didInterruptRewardVideo,
                               VMaxAdCallbackWithString     _didFailedToPlaybackRewardVideo,
                               VMaxAdCallbackWithInt        _didCompleteRewardVideo,
                               VMaxAdCallback               _willShowRewardedVideoPrepopUp,
                               VMaxAdCallback               _willShowRewardedVideoPostpopUp,
                               VMaxAdCallback               _willShowRewardedVideoNoFillpopUp,
                               VMaxAdCallback               _willShowRewardedVideoImpressionCappopUp,
                               VMaxAdCallback               _willShowRewardedVideoAdInterruptedpopUp,
                               VMaxWalletCallbackWithInt	_didUpdateWalletElement,
                               VMaxWalletCallbackWithString _didFailtoUpdateWalletElement
                              )
    {
        NSLog(@"Registering Callbacks...");
        adViewDidCacheAd                        = _adViewDidCacheAd;
        adViewDidLoadAd                         = _adViewDidLoadAd;
        adViewDidFailedToLoadAd                 = _adViewDidFailedToLoadAd;
        didInteractWithAd                       = _didInteractWithAd;
        willPresentOverlay                      = _willPresentOverlay;
        willDismissOverlay                      = _willDismissOverlay;
        adViewDidExpand                         = _adViewDidExpand;
        adViewDidCollapse                       = _adViewDidCollapse;
        willLeaveApp                            = _willLeaveApp;
        didInterruptRewardVideo                 = _didInterruptRewardVideo;
        didFailedToPlaybackRewardVideo          = _didFailedToPlaybackRewardVideo;
        didCompleteRewardVideo                  = _didCompleteRewardVideo;
        willShowRewardedVideoPrepopUp           = _willShowRewardedVideoPrepopUp;
        willShowRewardedVideoPostpopUp          = _willShowRewardedVideoPostpopUp;
        willShowRewardedVideoNoFillpopUp        = _willShowRewardedVideoNoFillpopUp;
        willShowRewardedVideoImpressionCappopUp = _willShowRewardedVideoImpressionCappopUp;
        willShowRewardedVideoAdInterruptedpopUp = _willShowRewardedVideoAdInterruptedpopUp;
        didUpdateWalletElement                  = _didUpdateWalletElement;
        didFailtoUpdateWalletElement            = _didFailtoUpdateWalletElement;
    }
    int _createNewVMaxAd(const char * zoneId,GLuint adUXType, GLuint adPosition)
    {
        NSLog(@"Create Ad is Called %s %u %u",zoneId,adUXType,adPosition);
        int adInstance;
        if( adUXType == 0)
        {
            adInstance = [VMaxUAdView createInterstitialAdForZoneID:zoneId];
        }
        else if(adUXType == 4){
            adInstance = [VMaxUAdView createNativeAdAdForZoneID:zoneId];
        }
        else 
        {
            adInstance = [VMaxUAdView createBannerAdForZoneID:zoneId
                                                        forUX:(VMaxAdUX)adUXType
                                                  forPlacement:(VMaxUBannerAdPlacement)adPosition];
            
        }
        return adInstance;
    }
    void _releaseVMaxAdViewInstance(int adInstance)
    {
        [VMaxUAdView releaseVMaxAdViewInstance: adInstance];
    }
    
    void _showBannerAdView(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        if(AdView)
        {
            [AdView showBannerAdView];
        }
        NSLog(@"Showing BannerAd View");

    }
    void _hideBannerAdView(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        if(AdView)
        {
            [AdView hideBannerAdView];
        }
        NSLog(@"Hiding BannerAd View");

    }
	void _setAdSettings(int adInstance,const char * adSettings)
	{
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        if(AdView)
        {
            [AdView setAdSettings:adSettings];
        }
        NSLog(@"Setting Ad settings");
	}
    void _setRefresh(int adInstance, bool shouldEnableRefresh)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setRefresh:shouldEnableRefresh];
        }
        NSLog(@"Enabling Refresh Rate...");

    }
    void _setRefreshRate(int adInstance, int refreshIntervalInseconds)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setRefreshRate:refreshIntervalInseconds];
        }
        NSLog(@"Setting Refresh Rate...");
    }
    void _pauseRefresh(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView pauseRefresh];
        }
        NSLog(@"Pausing Refresh...");
    }
    void _stopRefresh(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView stopRefresh];
        }
        NSLog(@"Stopping Refresh...");
    }
    void _resumeRefresh(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView resumeRefresh];
        }
        NSLog(@"Resuming Refresh...");
    }
    void _setTimeOut(int adInstance, int timeOut)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setTimeout:timeOut];
        }
        NSLog(@"Setting Timeout...");
    }
    void _setUXTypeWithConfig(int adInstance,int adUXType, const char * configString)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setUXType:(VMaxAdUX)adUXType
                   withConfig:configString];
        }
        NSLog(@"Setting UXType... %s",configString);
    }
    void _cacheAd(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView cacheAd];
        }
        NSLog(@"Caching Ad...");
    }
    char const * _getMarkup(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        char const * markupString = "";
        if(AdView)
        {
            markupString = [AdView getMarkup];
        }
        NSLog(@"Returning Markup...");
        return markupString;
    }
    void _cacheAdWithOriention(int adInstance, int orientation)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView cacheAdWithOrientation:(VMaxAdOrientation)orientation];
        }
        NSLog(@"Caching Ad with orientation...");
    }
    void _loadAd(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView loadAd];
        }
        NSLog(@"Loading Ad...");
    }
    void _loadAdWithOriention(int adInstance, int orientation)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView loadAdWithOrientation:(VMaxAdOrientation)orientation];
        }
        NSLog(@"Loading Ad with orientation...");
    }
    void _showAd(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView showAd];
        }
        NSLog(@"Showing Ad...");
    }
    void _cancelAd(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView cancelAd];
        }
        NSLog(@"Cancelling Ad...");
    }
    int _getAdState(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        int currentAdState;
        if(AdView)
        {
            currentAdState = [AdView getAdState];
        }
        NSLog(@"Getiing Current State of the Ad...");
        return currentAdState;
    }
    void _setTestDevice(int adInstance, const char * deviceIDFAListString)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setTestDevice:deviceIDFAListString];
        }
        NSLog(@"Setting Test device...");
    }
    void _setTestMode(int adInstance,int testMode, const char * deviceIDFAListString)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setTestMode:(VMaxTestMode)testMode withDevices:deviceIDFAListString];
        }
        NSLog(@"Setting Test device...%d, %s",testMode,deviceIDFAListString);
    }
    void _setUser(int adInstance, const char * userString)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setUser:userString];
        }
        NSLog(@"Assigning User...");
    }
    void _blockAds(int adInstance, bool shouldBlockAds)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView blockAds:shouldBlockAds];
        }
        NSLog(@"Blocking Ads %s...",shouldBlockAds?"Yes":"No");
    }
    void _showAdsAfterSessions(int adInstance, int noOfSessions)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView showAdsAfterSessions:noOfSessions];
        }
        NSLog(@"Show Ads after %d number of sessions...",noOfSessions);
    }
    void _blockCountries(int adInstance, const char * countryListString, int allowOrBlock, bool shouldRequest)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
                    [AdView blockCountries:countryListString
                              blockOrAllow:(VMaxCountryOption)allowOrBlock
           shouldRequestAdIfMCCUnawailable:shouldRequest];
        }
        NSLog(@"Blocking Contries %d, List %s %d...",allowOrBlock,countryListString,shouldRequest);
    }
    
    void _disableOffline(int adInstance, bool shouldDisableOffline)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView disableOffline:shouldDisableOffline];
        }
        NSLog(@"Disable Offline %d...", shouldDisableOffline);
    }
    void _userDidIAP(int adInstance, bool didIAP)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView userDidIAP:didIAP];
        }
        NSLog(@"User did IAP %d...",didIAP);
    }
    void _userDidIncent(int adInstance, bool didIncent)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView userDidIncent:didIncent];
        }
        NSLog(@"User did incent %d...",didIncent);
    }
    void _setLanguageOfArticle(int adInstance, const char * languageOfArticle)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setLanguageOfArticle:languageOfArticle];
        }
        NSLog(@"Setting Language Of Article %s...",languageOfArticle);
    }
    //Native Ad
    void _reportAndHandleNativeAdClick(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        if (AdView) {
            [AdView reportAndHandleNativeAdClick];
        }
    }
    void _registerNativeAdForImpression(int adInstance)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        if (AdView) {
            [AdView registerNativeAdForImpression];
        }
    }

    // Rewarded Video
    int _getTotalCurrency(const char * walletElementString)
    {
        VMaxWalletElement * walletElement = [[VMaxWallet sharedVMaxWallet]walletElementWithCurrency:[NSString stringWithUTF8String:walletElementString]];
        return (int)[walletElement getTotalCurrency];
        
    }
    
    void _setWalletElementImage(const char * walletNameString,const char * imageName)
    {
        VMaxWalletElement * walletElement = [[VMaxWallet sharedVMaxWallet]walletElementWithCurrency:[NSString stringWithUTF8String:walletNameString]];
        [walletElement setCurrencyImage:[UIImage imageNamed:[NSString stringWithUTF8String:imageName]]];
    }
    
    void _spendVirtualCurrency(const char * walletElementString,int currencyValue)
    {
         VMaxWalletElement * walletElement = [[VMaxWallet sharedVMaxWallet]walletElementWithCurrency:[NSString stringWithUTF8String:walletElementString]];
        [walletElement spendVirtualCurrency:currencyValue];
    }
    
    void _awardVirtualCurrency(const char * walletElementString, int currencyValue)
    {
        VMaxWalletElement * walletElement = [[VMaxWallet sharedVMaxWallet]walletElementWithCurrency:[NSString stringWithUTF8String:walletElementString]];
        [walletElement awardVirtualCurrency:currencyValue withCompletionBlock:^(BOOL isSuccess, NSInteger currentBalance) {
            if(isSuccess){
                ThrowDidUpdateWalletElement(walletElementString, (int)currentBalance);
            }
        }];
    }
    
    void _getWalletElementWithCurrency(const char * walletElementString)
    {
        [[VMaxWallet sharedVMaxWallet]walletElementWithCurrency:[NSString stringWithUTF8String:walletElementString]];
    }
    
    void _synchronizeWallet()
    {
        [[VMaxWallet sharedVMaxWallet]synchronizeWallet];
    }
    void _initializeWithRewardedVideo(int adInstance,const char * walletElement)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView initRewardedVideo:walletElement];
        }
    }
    
    char * _getInitialWalletElements()
    {
        NSString *walletElementsString = [[NSString alloc]init];
        for (VMaxWalletElement *element in [[VMaxWallet sharedVMaxWallet]allWalletElemets]) {
            if ([walletElementsString length]==0) {
                walletElementsString = element.currency;
            }else{
                walletElementsString = [NSString stringWithFormat:@"%@,%@",walletElementsString,element.currency];
            }
        }
        char * str = (char *)malloc(strlen([walletElementsString UTF8String])*sizeof(char));
        strcpy(str,[walletElementsString UTF8String]);
        return str;
    }
    void _setShouldShowMaxImpressionCapPopup(int adInstance, bool value)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setShouldShowMaxImpressionCapPopup:value];
        }
    }
    void _setShouldShowPrePopup(int adInstance, bool value)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setShouldShowPrePopup:value];
        }
    }
    void _setShouldShowPostPopup(int adInstance, bool value)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setShouldShowPostPopup:value];
        }
    }
    void _setShouldShowInterruptPopup(int adInstance, bool value)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setShouldShowInterruptPopup:value];
        }
    }
    void _setShouldShowNoFillPopup(int adInstance, bool value)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setShouldShowNoFillPopup:value];
        }
    }
    void _setMaxImpressionCapPopUpWithTitle(int adInstance, char * title , char * message)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setMaxImpressionCapPopUpWithTitle:title message:message];
        }
    }
    void _setPrePopUpWithTitle(int adInstance, char * title , char * message)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setPrePopUpWithTitle:title message:message];
        }
    }
    void _setPostPopUpWithTitle(int adInstance, char * title , char * message)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setPostPopUpWithTitle:title message:message];
        }
    }
    void _setInterruptPopUpWithTitle(int adInstance, char * title , char * message)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setInterruptPopUpWithTitle:title message:message];
        }
    }
    void _setNoFillPopUpWithTitle(int adInstance, char * title , char * message)
    {
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView setNoFillPopUpWithTitle:title message:message];
        }
    }
    
    void _nativeIconClicked(int adInstance){
        VMaxUAdView* AdView = [VMaxUAdView getVMaxAdViewInstance:adInstance];
        
        if(AdView)
        {
            [AdView nativeIconClicked];
        }
    }
}